﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaterialSkin.Controls
{
	public partial class MaterialToolTip : ToolTip
	{
		[Browsable(false)]
		public MaterialSkinManager SkinManager => MaterialSkinManager.Instance;

		[Browsable(false)]
		[Obsolete]
		public new Color BackColor;

		[Browsable(false)]
		[Obsolete]
		public new Color ForeColor;

		public MaterialToolTip()
		{
			InitializeComponent();
			this.OwnerDraw = true;
			this.UseFading = false;
			this.Draw += MaterialToolTip_Draw;
		}

		public MaterialToolTip(IContainer container)
		{
			container.Add(this);

			InitializeComponent();
			this.OwnerDraw = true;
			this.UseFading = false;
			this.Draw += MaterialToolTip_Draw;
			this.Popup += MaterialToolTip_Popup;
		}

		private void MaterialToolTip_Popup(object sender, PopupEventArgs e)
		{
			e.ToolTipSize = new Size((int)TextRenderer.MeasureText(this.GetToolTip(e.AssociatedControl), SkinManager.ROBOTO_MEDIUM_10).Width + 16, 24);
			var hwnd = (IntPtr)typeof(ToolTip).GetProperty("Handle",
				BindingFlags.NonPublic | BindingFlags.Instance).GetValue(this);
			var cs = GetClassLong(hwnd, GCL_STYLE);
			if ((cs & CS_DROPSHADOW) == CS_DROPSHADOW)
			{
				cs = cs & ~CS_DROPSHADOW;
				SetClassLong(hwnd, GCL_STYLE, cs);
			}
		}

		private void MaterialToolTip_Draw(object sender, DrawToolTipEventArgs e)
		{
			//e.Graphics.Clear(Color.Black);
			var windowRect = GetWindowRect();
			e.Graphics.CopyFromScreen(windowRect.Left, windowRect.Top, 0, 0, new Size(e.Bounds.Width, e.Bounds.Height));
			e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
			//e.Graphics.Clear(Color.White);
			var stringSize = e.Graphics.MeasureString(e.ToolTipText, SkinManager.ROBOTO_MEDIUM_10);
			//e.Bounds.X -= e.Bounds.Width / 2;
			var rect = new Rectangle(0, 0, (int)stringSize.Width + 16, 24);
			var bgPath = DrawHelper.CreateRoundRect(rect, 4);
			//e.Graphics.FillPath(new SolidBrush(Color.FromArgb(230, Color.Black)), bgPath);
			e.Graphics.DrawString(e.ToolTipText, SkinManager.ROBOTO_MEDIUM_10, Brushes.White, rect, new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center });
		}

		//https://stackoverflow.com/questions/1381911/transparency-around-non-rectangular-owner-draw-tooltip-control
		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool GetWindowRect(IntPtr hWnd, ref Rectangle lpRect);

		private Rectangle GetWindowRect()
		{
			Rectangle rect = new Rectangle();
			var window = typeof(ToolTip).GetField("window", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(this) as NativeWindow;
			GetWindowRect(window.Handle, ref rect);
			return rect;
		}

		//https://stackoverflow.com/questions/50879056/how-to-draw-my-own-tooltip-without-shadow
		public const int GCL_STYLE = -26;

		public const int CS_DROPSHADOW = 0x20000;

		[DllImport("user32.dll", EntryPoint = "GetClassLong")]
		public static extern int GetClassLong(IntPtr hWnd, int nIndex);

		[DllImport("user32.dll", EntryPoint = "SetClassLong")]
		public static extern int SetClassLong(IntPtr hWnd, int nIndex, int dwNewLong);
	}
}