﻿using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;
using MaterialSkin.Animations;
using System;

namespace MaterialSkin.Controls
{
	public class MaterialFloatingButton : Button, IMaterialControl
	{
		[Browsable(false)]
		public int Depth { get; set; }

		[Browsable(false)]
		public MaterialSkinManager SkinManager => MaterialSkinManager.Instance;

		[Browsable(false)]
		public MouseState MouseState { get; set; }

		public bool Mini { get; set; }

		private readonly AnimationManager _animationManager;

		private readonly AnimationManager _upAnimationManager;

		private SizeF _textSize;

		private Image _icon;

		private bool _justIcon => string.IsNullOrEmpty(Text);

		public Image Icon
		{
			get { return _icon; }
			set
			{
				_icon = value;
				if (AutoSize)
					Size = GetPreferredSize();
				Invalidate();
			}
		}

		public MaterialFloatingButton()
		{
			_animationManager = new AnimationManager(false)
			{
				Increment = 0.03,
				AnimationType = AnimationType.EaseOut
			};
			_animationManager.OnAnimationProgress += sender => Invalidate();
			_upAnimationManager = new AnimationManager(false)
			{
				Increment = 0.1,
				AnimationType = AnimationType.EaseOut,
				InterruptAnimation = true,
			};
			_upAnimationManager.OnAnimationProgress += sender => Invalidate();

			AutoSizeMode = AutoSizeMode.GrowAndShrink;
			AutoSize = true;
		}

		public override string Text
		{
			get { return base.Text; }
			set
			{
				base.Text = value;
				_textSize = CreateGraphics().MeasureString(value.ToUpper(), SkinManager.ROBOTO_MEDIUM_10);
				if (AutoSize)
					Size = GetPreferredSize();
				Invalidate();
			}
		}

		protected override void OnPaint(PaintEventArgs pevent)
		{
			var g = pevent.Graphics;
			g.SmoothingMode = SmoothingMode.AntiAlias;
			g.TextRenderingHint = TextRenderingHint.AntiAlias;

			g.Clear(Parent.BackColor);

			var backgroundPath = DrawHelper.CreateRoundRect(ClientRectangle.X,
				ClientRectangle.Y,
				ClientRectangle.Width - 1,
				ClientRectangle.Height - 1,
				 _justIcon ? (18f * (Mini ? 1f : 1.5f)) : 24f);

			g.FillPath(SkinManager.ColorScheme.AccentBrush, backgroundPath);

			if (ContainsFocus)
				g.FillPath(new SolidBrush(Color.FromArgb(32, SkinManager.ColorScheme.AccentColor)), backgroundPath);

			if (MouseState == MouseState.HOVER && !_upAnimationManager.IsAnimating())
				g.FillPath(new SolidBrush(Color.FromArgb(16, SkinManager.ColorScheme.AccentColor)), backgroundPath);
			else if (MouseState == MouseState.DOWN && !_animationManager.IsAnimating())
				g.FillPath(new SolidBrush(Color.FromArgb(70, SkinManager.ColorScheme.AccentColor)), backgroundPath);
			if (_upAnimationManager.IsAnimating())
			{
				for (int i = 0; i < _upAnimationManager.GetAnimationCount(); i++)
				{
					var animationValue = _upAnimationManager.GetProgress(i);
					var trans = 70 - animationValue * 48;
					g.FillPath(new SolidBrush(Color.FromArgb((int)trans, Color.White)), backgroundPath);
				}
			}
			g.SetClip(backgroundPath);
			if (_animationManager.IsAnimating())
			{
				for (int i = 0; i < _animationManager.GetAnimationCount(); i++)
				{
					var animationValue = _animationManager.GetProgress(i);
					var animationSource = _animationManager.GetSource(i);
					var rippleBrush = new SolidBrush(Color.FromArgb(
						(int)(151 - (animationValue * 100)),
						 Color.White));
					var rippleSize = (int)(animationValue * Width * 2);
					g.FillEllipse(rippleBrush, new Rectangle(animationSource.X - rippleSize / 2, animationSource.Y - rippleSize / 2, rippleSize, rippleSize));
				}
			}
			g.ResetClip();
			//Icon
			Rectangle iconRect;

			if (_justIcon)
			{
				if (Mini)
				{
					iconRect = new Rectangle(8, 8, 24, 24);
				}
				else
				{
					iconRect = new Rectangle(16, 16, 24, 24);
				}
			}
			else
			{
				iconRect = new Rectangle(16, 12, 24, 24);
			}

			if (Icon != null)
				g.DrawImage(Icon, iconRect);

			//Text
			var textRect = ClientRectangle;

			if (Icon != null)
			{
				//
				// Resize and move Text container
				//

				// First 8: left padding
				// 24: icon width
				// Second 4: space between Icon and Text
				// Third 8: right padding
				textRect.Width -= 8 + 24 + 4 + 8;

				// First 8: left padding
				// 24: icon width
				// Second 4: space between Icon and Text
				textRect.X += 8 + 24 + 4;
			}

			g.DrawString(
				Text.ToUpper(),
				SkinManager.ROBOTO_MEDIUM_10,
				SkinManager.GetRaisedButtonTextBrush(true),
				textRect,
				new StringFormat { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center });
		}

		protected override void OnCreateControl()
		{
			base.OnCreateControl();
			if (DesignMode) return;

			MouseState = MouseState.OUT;
			MouseEnter += (sender, args) =>
			{
				MouseState = MouseState.HOVER;
				Invalidate();
			};
			MouseLeave += (sender, args) =>
			{
				MouseState = MouseState.OUT;
				Invalidate();
			};
			MouseDown += (sender, args) =>
			{
				if (args.Button == MouseButtons.Left)
				{
					MouseState = MouseState.DOWN;
					_animationManager.InterruptAnimation = true;
					_animationManager.StartNewAnimation(AnimationDirection.In, args.Location);
					Invalidate();
				}
			};
			MouseUp += (sender, args) =>
			{
				MouseState = MouseState.HOVER;
				if (!_animationManager.IsAnimating()) _upAnimationManager.StartNewAnimation(AnimationDirection.In);
				Invalidate();
			};
		}

		private Size GetPreferredSize()
		{
			return GetPreferredSize(new Size(0, 0));
		}

		public override Size GetPreferredSize(Size proposedSize)
		{
			if (_justIcon)
			{
				var s = Mini ? 40 : 56;
				return new Size(s, s);
			}
			else
			{
				//Left Padding + Icon & Text Spacing + Right Padding
				var extra = 16 + 24 + 8 + 20;
				return new Size((int)Math.Ceiling(_textSize.Width) + extra, 48);
			}
		}
	}
}