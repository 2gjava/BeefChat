﻿using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;
using MaterialSkin.Animations;
using System;

namespace MaterialSkin.Controls
{
	public class MaterialButton : Button, IMaterialControl
	{
		[Browsable(false)]
		public int Depth { get; set; }

		[Browsable(false)]
		public MaterialSkinManager SkinManager => MaterialSkinManager.Instance;

		[Browsable(false)]
		public MouseState MouseState { get; set; }

		public ButtonType Type { get; set; }

		private readonly AnimationManager _animationManager;

		private readonly AnimationManager _upAnimationManager;

		private SizeF _textSize;

		private Image _icon;

		public Image Icon
		{
			get { return _icon; }
			set
			{
				_icon = value;
				if (AutoSize)
					Size = GetPreferredSize();
				Invalidate();
			}
		}

		public MaterialButton()
		{
			_animationManager = new AnimationManager(false)
			{
				Increment = 0.03,
				AnimationType = AnimationType.EaseOut,
				InterruptAnimation = true,
			};
			_animationManager.OnAnimationProgress += sender => Invalidate();
			_upAnimationManager = new AnimationManager(false)
			{
				Increment = 0.1,
				AnimationType = AnimationType.EaseOut,
				InterruptAnimation = true,
			};
			_upAnimationManager.OnAnimationProgress += sender => Invalidate();
			AutoSizeMode = AutoSizeMode.GrowAndShrink;
			AutoSize = true;
		}

		public override string Text
		{
			get { return base.Text; }
			set
			{
				base.Text = value;
				_textSize = CreateGraphics().MeasureString(value.ToUpper(), SkinManager.ROBOTO_MEDIUM_10);
				if (AutoSize)
					Size = GetPreferredSize();
				Invalidate();
			}
		}

		protected override void OnPaint(PaintEventArgs pevent)
		{
			var g = pevent.Graphics;
			g.SmoothingMode = SmoothingMode.AntiAlias;
			g.TextRenderingHint = TextRenderingHint.AntiAlias;
			g.TextContrast = 1;
			g.InterpolationMode = InterpolationMode.High;
			g.Clear(Parent.BackColor);

			using (var backgroundPath = DrawHelper.CreateRoundRect(ClientRectangle.X + 1,
				ClientRectangle.Y + 1,
				ClientRectangle.Width - 2,
				ClientRectangle.Height - 2,
				2f))
			{
				var color = (Type == ButtonType.Contained) ? Color.White : SkinManager.ColorScheme.PrimaryColor;

				if (Type == ButtonType.Contained)
					g.FillPath(SkinManager.ColorScheme.PrimaryBrush, backgroundPath);
				else if (Type == ButtonType.Outlined)
					g.DrawPath(new Pen(Color.FromArgb(32, SkinManager.Theme == MaterialSkinManager.Themes.DARK ? Color.White : Color.Black), 1), backgroundPath);

				if (ContainsFocus)
					g.FillPath(new SolidBrush(Color.FromArgb(32, color)), backgroundPath);

				if (MouseState == MouseState.HOVER && !_upAnimationManager.IsAnimating())
					g.FillPath(new SolidBrush(Color.FromArgb(16, color)), backgroundPath);
				else if (MouseState == MouseState.DOWN && !_animationManager.IsAnimating())
					g.FillPath(new SolidBrush(Color.FromArgb(70, color)), backgroundPath);
				if (_upAnimationManager.IsAnimating())
				{
					for (int i = 0; i < _upAnimationManager.GetAnimationCount(); i++)
					{
						var animationValue = _upAnimationManager.GetProgress(i);
						var trans = 80 - animationValue * 48;
						g.FillPath(new SolidBrush(Color.FromArgb((int)trans, color)), backgroundPath);
					}
				}
				g.SetClip(backgroundPath);
			}

			if (_animationManager.IsAnimating())
			{
				for (int i = 0; i < _animationManager.GetAnimationCount(); i++)
				{
					var animationValue = _animationManager.GetProgress(i);
					var animationSource = _animationManager.GetSource(i);
					var rippleBrush = new SolidBrush(Color.FromArgb(
						(int)(151 - (animationValue * 100)),
						(Type == ButtonType.Contained) ? Color.White : SkinManager.ColorScheme.PrimaryColor));
					var rippleSize = (int)(animationValue * Width * 2);
					g.FillEllipse(rippleBrush, new Rectangle(animationSource.X - rippleSize / 2, animationSource.Y - rippleSize / 2, rippleSize, rippleSize));
				}
			}
			g.ResetClip();
			//Icon
			var iconRect = new Rectangle(9, 9, 18, 18);

			if (string.IsNullOrEmpty(Text))
				// Center Icon
				iconRect.X += 2;

			if (Icon != null)
				g.DrawImage(Icon, iconRect);

			//Text
			var textRect = ClientRectangle;

			if (Icon != null)
			{
				//
				// Resize and move Text container
				//

				// First 8: left padding
				// 24: icon width
				// Second 4: space between Icon and Text
				// Third 8: right padding
				textRect.Width -= 8 + 24 + 4 + 8;

				// First 8: left padding
				// 24: icon width
				// Second 4: space between Icon and Text
				textRect.X += 8 + 24 + 4;
			}
			Brush textBrush;

			if (Type == ButtonType.Contained)
				textBrush = SkinManager.GetRaisedButtonTextBrush(true);
			else
				textBrush = SkinManager.ColorScheme.PrimaryBrush;

			g.DrawString(
				Text.ToUpper(),
				SkinManager.ROBOTO_MEDIUM_10,
				textBrush,
				textRect,
				new StringFormat { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center });
		}

		protected override void OnCreateControl()
		{
			base.OnCreateControl();
			if (DesignMode) return;

			MouseState = MouseState.OUT;
			MouseEnter += (sender, args) =>
			{
				MouseState = MouseState.HOVER;
				Invalidate();
			};
			MouseLeave += (sender, args) =>
			{
				MouseState = MouseState.OUT;
				Invalidate();
			};
			MouseDown += (sender, args) =>
			{
				if (args.Button == MouseButtons.Left)
				{
					MouseState = MouseState.DOWN;
					_animationManager.InterruptAnimation = true;
					_animationManager.StartNewAnimation(AnimationDirection.In, args.Location);
					Invalidate();
				}
			};
			MouseUp += (sender, args) =>
			{
				MouseState = MouseState.HOVER;
				if (!_animationManager.IsAnimating()) _upAnimationManager.StartNewAnimation(AnimationDirection.In);
				Invalidate();
			};
		}

		private Size GetPreferredSize()
		{
			return GetPreferredSize(new Size(0, 0));
		}

		public override Size GetPreferredSize(Size proposedSize)
		{
			// Provides extra space for proper padding for content
			var extra = string.IsNullOrWhiteSpace(Text) ? 4 : 32;

            if (Icon != null && !string.IsNullOrWhiteSpace(Text))
            {
                extra += 4;
            }
            

			return new Size(Math.Max(64, (int)Math.Ceiling(_textSize.Width)) + extra, 36);
		}
	}

	public enum ButtonType
	{
		Text,
		Outlined,
		Contained
	}
}