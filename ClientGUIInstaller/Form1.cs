﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientGUIInstaller
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
		}

		private void CancelButton_Click(object sender, EventArgs e) => this.Close();

		private void tabControl1_DrawItem(object sender, DrawItemEventArgs e)
		{
			e.Graphics.Clear(SystemColors.Control);
		}
	}
}