﻿namespace ClientGUIInstaller
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.NextButton = new System.Windows.Forms.Button();
			this.BackButton = new System.Windows.Forms.Button();
			this.CancelButton = new System.Windows.Forms.Button();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.InstallPathLabel = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.ResultsTabPage = new System.Windows.Forms.TabPage();
			this.label2 = new System.Windows.Forms.Label();
			this.LaunchCheckBox = new System.Windows.Forms.CheckBox();
			this.ProtocolCheckBox = new System.Windows.Forms.CheckBox();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.ResultsTabPage.SuspendLayout();
			this.SuspendLayout();
			// 
			// NextButton
			// 
			this.NextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.NextButton.Location = new System.Drawing.Point(431, 328);
			this.NextButton.Name = "NextButton";
			this.NextButton.Size = new System.Drawing.Size(75, 23);
			this.NextButton.TabIndex = 0;
			this.NextButton.Text = "&Next";
			this.NextButton.UseVisualStyleBackColor = true;
			// 
			// BackButton
			// 
			this.BackButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.BackButton.Location = new System.Drawing.Point(350, 328);
			this.BackButton.Name = "BackButton";
			this.BackButton.Size = new System.Drawing.Size(75, 23);
			this.BackButton.TabIndex = 1;
			this.BackButton.Text = "&Back";
			this.BackButton.UseVisualStyleBackColor = true;
			// 
			// CancelButton
			// 
			this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.CancelButton.Location = new System.Drawing.Point(269, 328);
			this.CancelButton.Name = "CancelButton";
			this.CancelButton.Size = new System.Drawing.Size(75, 23);
			this.CancelButton.TabIndex = 2;
			this.CancelButton.Text = "&Cancel";
			this.CancelButton.UseVisualStyleBackColor = true;
			this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
			// 
			// tabControl1
			// 
			this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Controls.Add(this.ResultsTabPage);
			this.tabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
			this.tabControl1.ItemSize = new System.Drawing.Size(0, 1);
			this.tabControl1.Location = new System.Drawing.Point(12, 12);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(494, 310);
			this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
			this.tabControl1.TabIndex = 3;
			this.tabControl1.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tabControl1_DrawItem);
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.ProtocolCheckBox);
			this.tabPage1.Controls.Add(this.textBox1);
			this.tabPage1.Controls.Add(this.InstallPathLabel);
			this.tabPage1.Location = new System.Drawing.Point(4, 5);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(486, 301);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "tabPage1";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// tabPage2
			// 
			this.tabPage2.Location = new System.Drawing.Point(4, 5);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(486, 301);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "tabPage2";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// InstallPathLabel
			// 
			this.InstallPathLabel.AutoSize = true;
			this.InstallPathLabel.Dock = System.Windows.Forms.DockStyle.Top;
			this.InstallPathLabel.Location = new System.Drawing.Point(3, 3);
			this.InstallPathLabel.Name = "InstallPathLabel";
			this.InstallPathLabel.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
			this.InstallPathLabel.Size = new System.Drawing.Size(85, 18);
			this.InstallPathLabel.TabIndex = 0;
			this.InstallPathLabel.Text = "Installation Path:";
			// 
			// textBox1
			// 
			this.textBox1.Dock = System.Windows.Forms.DockStyle.Top;
			this.textBox1.Location = new System.Drawing.Point(3, 21);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(480, 20);
			this.textBox1.TabIndex = 1;
			// 
			// tabPage3
			// 
			this.tabPage3.Location = new System.Drawing.Point(4, 5);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage3.Size = new System.Drawing.Size(486, 301);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "tabPage3";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// ResultsTabPage
			// 
			this.ResultsTabPage.Controls.Add(this.LaunchCheckBox);
			this.ResultsTabPage.Controls.Add(this.label2);
			this.ResultsTabPage.Location = new System.Drawing.Point(4, 5);
			this.ResultsTabPage.Name = "ResultsTabPage";
			this.ResultsTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.ResultsTabPage.Size = new System.Drawing.Size(486, 301);
			this.ResultsTabPage.TabIndex = 3;
			this.ResultsTabPage.Text = "tabPage4";
			this.ResultsTabPage.UseVisualStyleBackColor = true;
			// 
			// label2
			// 
			this.label2.Dock = System.Windows.Forms.DockStyle.Top;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(3, 3);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(480, 20);
			this.label2.TabIndex = 0;
			this.label2.Text = "BeefChat has been installed";
			// 
			// LaunchCheckBox
			// 
			this.LaunchCheckBox.AutoSize = true;
			this.LaunchCheckBox.Dock = System.Windows.Forms.DockStyle.Top;
			this.LaunchCheckBox.Location = new System.Drawing.Point(3, 23);
			this.LaunchCheckBox.Name = "LaunchCheckBox";
			this.LaunchCheckBox.Padding = new System.Windows.Forms.Padding(5);
			this.LaunchCheckBox.Size = new System.Drawing.Size(480, 27);
			this.LaunchCheckBox.TabIndex = 1;
			this.LaunchCheckBox.Text = "Launch BeefChat after setup";
			this.LaunchCheckBox.UseVisualStyleBackColor = true;
			// 
			// ProtocolCheckBox
			// 
			this.ProtocolCheckBox.AutoSize = true;
			this.ProtocolCheckBox.Dock = System.Windows.Forms.DockStyle.Top;
			this.ProtocolCheckBox.Location = new System.Drawing.Point(3, 41);
			this.ProtocolCheckBox.Name = "ProtocolCheckBox";
			this.ProtocolCheckBox.Padding = new System.Windows.Forms.Padding(2, 16, 2, 2);
			this.ProtocolCheckBox.Size = new System.Drawing.Size(480, 35);
			this.ProtocolCheckBox.TabIndex = 2;
			this.ProtocolCheckBox.Text = "Register BeefChat protocols";
			this.ProtocolCheckBox.UseVisualStyleBackColor = true;
			// 
			// MainForm
			// 
			this.AcceptButton = this.NextButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.CancelButton;
			this.ClientSize = new System.Drawing.Size(518, 363);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.CancelButton);
			this.Controls.Add(this.BackButton);
			this.Controls.Add(this.NextButton);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.Text = "BeefChat Installer";
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.ResultsTabPage.ResumeLayout(false);
			this.ResultsTabPage.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button NextButton;
		private System.Windows.Forms.Button BackButton;
		private System.Windows.Forms.Button CancelButton;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.CheckBox ProtocolCheckBox;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label InstallPathLabel;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.TabPage ResultsTabPage;
		private System.Windows.Forms.CheckBox LaunchCheckBox;
		private System.Windows.Forms.Label label2;
	}
}

