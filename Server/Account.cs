﻿using System.Collections.Generic;
using BeefChat.Shared.Objects;

namespace BeefChat.Server
{
	public class Account
	{
		public string Username { get; set; }
		public string Password { get; set; }
		public User Profile { get; set; }
		public List<string> Friends { get; set; } = new List<string>();
	}
}