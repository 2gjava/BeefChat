﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Threading;

using BeefChat.Shared;
using BeefChat.Shared.Logging;
using BeefChat.Shared.Objects;
using BeefChat.Shared.Packets;

using Newtonsoft.Json;

using static BeefChat.Shared.Enums;

namespace BeefChat.Server
{
	public class Server
	{
		public bool Started { get; private set; }

		#region Private Fields

		private TcpListener _tcpListener;
		private Thread _acceptThread;
		private readonly List<Client> _clients = new List<Client>();
		private readonly List<Thread> _clientThreads = new List<Thread>();
		private Dictionary<string, Account> _accounts;

		private readonly List<Channel> _channels = new List<Channel>
		{
			 new Channel("#general"),
			 new Channel("#developer"),
			 new Channel("#otaku"),
		};

		#endregion Private Fields

		#region Starting, Shutting down, restarting

		/// <summary>
		/// Starts the server
		/// </summary>
		/// <param name="port">The port the server should listen to</param>
		public void Start(int? port)
		{
			if (Started) throw new Exception("Server is already started, no need to do that.");
			if (_tcpListener == null) _tcpListener = new TcpListener(port ?? Constants.PortNumber);
			_tcpListener.Start();
			_acceptThread = new Thread(AcceptNewClients)
			{
				Name = "Accept Thread",
				Priority = ThreadPriority.Normal
			};
			_acceptThread.Start();
			Started = true;
		}

		/// <summary>
		/// Stops the server
		/// </summary>
		public void Stop()
		{
			if (_tcpListener == null) throw new NullReferenceException("Inner tcpListener is null, please use Start() to create a object.");
			_tcpListener.Stop();
			foreach (Thread t in _clientThreads)
			{
				try
				{
					t.Abort();
				}
				catch
				{
					// ignored
				}
			}
			Started = false;
		}

		/// <summary>
		/// Restarts the server
		/// </summary>
		/// <param name="port">The port the server should listen to</param>
		public void Restart(int? port)
		{
			Stop();
			Start(port);
		}

		#endregion Starting, Shutting down, restarting

		#region Loading & Saving data

		public void LoadData(string dataPath)
		{
			Logger.Add(LogEntryType.Verbose, "Loading data");
			var accpath = Path.Combine(dataPath, "accounts.json");
			if (!Directory.Exists(dataPath)) Directory.CreateDirectory(dataPath);
			if (System.IO.File.Exists(accpath))
			{
				System.IO.File.WriteAllText(accpath, JsonConvert.SerializeObject(new Dictionary<string, Account>()));
				_accounts = JsonConvert.DeserializeObject<Dictionary<string, Account>>(System.IO.File.ReadAllText(accpath));
				Logger.Add(LogEntryType.Verbose, "Validating data");
				if (_accounts.Any(acc => acc.Key.Any(c => char.IsUpper(c))))
					Logger.Add(LogEntryType.Warning, "Data seems invalid, there's an username with a uppercase letter.");
				Logger.Add(LogEntryType.Verbose, "Data loaded");
			}
			else
			{
				_accounts = new Dictionary<string, Account>();
			}
		}

		public void WriteData(string dataPath)
		{
			Logger.Add(LogEntryType.Verbose, "Writing data");
			var accpath = Path.Combine(dataPath, "accounts.json");
			if (!Directory.Exists(dataPath)) throw new DirectoryNotFoundException("Entered path couldn't be found");
			System.IO.File.WriteAllText(accpath, JsonConvert.SerializeObject(_accounts));
			Logger.Add(LogEntryType.Verbose, "Data written");
		}

		#endregion Loading & Saving data

		#region Account Queries

		internal bool AccountExists(string username) => _accounts.ContainsKey(username.ToLower());

		internal Account GetAccount(string username)
		{
			if (username != null && _accounts.ContainsKey(username.ToLower()))
			{
				return _accounts[username.ToLower()];
			}
			return null;
		}

		internal void SetAccount(string username, Account account)
		{
			if (username != null && account != null)
			{
				_accounts[username.ToLower()] = account;
			}
		}

		#endregion Account Queries

		private void AcceptNewClients()
		{
			if (_tcpListener == null)
			{
				throw new NullReferenceException("Inner tcpListener is null, please use Start() to create an object.");
			}
			try
			{
				while (true)
				{
					var client = _tcpListener.AcceptTcpClient();
					var thread = new Thread(HandleClient)
					{
						Priority = ThreadPriority.AboveNormal,
						Name = "Client Thread"
					};
					thread.Start(client);
					_clientThreads.Add(thread);
				}
			}
			catch
			{
				Logger.Add(LogEntryType.Warning, "AcceptClient thread has been ended because of an exception");
			}
		}

		private void SaveException(Exception exception)
		{
			if (!Directory.Exists("errors")) Directory.CreateDirectory("errors");
			var fileName = Path.GetTempFileName() + ".txt";
			Logger.Add(LogEntryType.Warning, "Saving exception as " + Path.GetFileName(fileName));
			System.IO.File.WriteAllText(fileName, exception.ToString());
		}

		private void HandleClient(object tcpClient)
		{
			if (tcpClient.GetType() != typeof(TcpClient))
			{
				throw new ArgumentException("Argument tcpClient is not a TcpClient.", nameof(tcpClient));
			}
			var client = new Client((TcpClient)tcpClient);
			_clients.Add(client);
			Logger.Add(LogEntryType.Debug, "Client connected");
			try
			{
				while (client.TcpClient.Connected)
				{
					var p = client.Receive();
					if (p == null) break;
					HandlePacket(ref client, p);
				}
			}
			catch (IOException)
			{
				client.Disconnect("Disconnected by user");
				return;
			}
			catch (Exception ex)
			{
				//Crashes the program if a debugger is there instead it will ignore the exception
				if (Debugger.IsAttached)
				{
					throw;
				}
				else
				{
					client.Disconnect("Internal server error");
					SaveException(ex);
					return;
				}
			}
			client.Disconnect(null);
		}

		private void HandlePacket(ref Client client, Packet packet)
		{
			if (client == null) throw new ArgumentNullException(nameof(client));
			if (packet == null) throw new ArgumentNullException(nameof(packet));

			if (client.IsAuthenticated)
			{
				switch (packet)
				{
					case MessagePacket p:
						if (string.IsNullOrWhiteSpace(p.Message.Content) && p.Message.Attachments.Count == 0) return;
						if (p.Channel.Type == Channel.ChannelType.Public)
						{
							foreach (Client receiver in _clients)
								receiver.SendMessage(new Message(p.Message)
								{
									Author = client.Username
								});
						}
						else if (p.Channel.Type == Channel.ChannelType.User)
						{
							var u = _clients.FirstOrDefault(cl => cl.Username == p.Channel.Name);
							if (u != null)
							{
								var msg = new Message(p.Message)
								{
									Channel = new Channel(client.Account.Username, Channel.ChannelType.User),
									Author = client.Account.Username
								};
								u.SendMessage(msg);
							}
							client.SendMessage(new Message(p.Channel, client.Account.Username, p.Message.Content));
						}
						break;

					case RequestUserPacket p:
						User result = null;
						if (AccountExists(p.Username))
						{
							var account = GetAccount(p.Username);
							if (account?.Profile != null)
							{
								result = new User(account.Profile) { Friend = client.Account.Friends.Contains(p.Username) };
							}
						}
						client.SendUser(result);
						break;

					case ManageFriendPacket p:
						if (p.Add && !client.Account.Friends.Contains(p.Username))
							client.Account.Friends.Add(p.Username);
						else if (!p.Add && client.Account.Friends.Contains(p.Username))
							client.Account.Friends.Remove(p.Username);
						client.SaveAccount();
						client.SendFriends(client.Account.Friends);
						break;

					case ChangeProfilePacket p:
						if (p.Key == 1 && p.Value.GetType() == typeof(User.UserStatus))
							client.Account.Profile.Status = (User.UserStatus)p.Value;
						if (p.Key == 2 && p.Value.GetType() == typeof(string))
							client.Account.Profile.StatusText = (string)p.Value;
						if (p.Key == 3 && p.Value.GetType() == typeof(string))
							client.Account.Profile.Nickname = (string)p.Value;
						if (p.Key == 4 && p.Value.GetType() == typeof(byte[]))
							client.Account.Profile.Picture = (byte[])p.Value;
						if (p.Key == 5 && p.Value.GetType() == typeof(string))
							client.Account.Profile.Color = (string)p.Value;
						client.SaveAccount();
						client.SendUser(client.Account.Profile);
						break;

					case UserDeleteAccountPacket p:
						if (client.Account.Password == p.Password)
						{
							Logger.Add(LogEntryType.Normal, "Deleting account...");
							client.Disconnect("Account is being deleted.", false, false);
							_accounts.Remove(client.Username);
						}
						break;

					default:
						Logger.Add(LogEntryType.Warning, "Client sent an unknown request. (" + packet.ToString() + ")");
						throw new Exception($"Unknown request ({packet.ToString()})");
				}
			}
			else if (packet.GetType() == typeof(AuthenticationRequestPacket))
			{
				var p = (AuthenticationRequestPacket)packet;
				AuthenticationResponsePacket responsePacket = null;
				string username = p.Username.ToLower();
				if (Regex.IsMatch(username, @"^[a-zA-Z0-9]+$"))
				{
					//Is valid username
					if (!p.Register)
					{
						if (AccountExists(username) && GetAccount(username).Password == p.Password)
						{
							client.Username = username;
							client.IsAuthenticated = true;
							client.SendChannels(_channels);
							client.SendUser(client.Account.Profile);
							responsePacket = new AuthenticationResponsePacket(AuthResponse.Success, packet.Id);
						}
						else
						{
							responsePacket = new AuthenticationResponsePacket(AuthResponse.WrongCredentials, p.Id);
						}
					}
					else
					{
						if (!AccountExists(username))
						{
							_accounts[username] = new Account
							{
								Username = username,
								Password = p.Password,
								Profile = new User
								{
									//Using "p.Username" because "username" has been lowercased
									Nickname = p.Username,
									Username = username,
									Status = User.UserStatus.Online,
									Picture = null,
								}
							};
							client.Username = username;
							client.IsAuthenticated = true;
							client.SendChannels(_channels);
							client.SendUser(client.Account.Profile);
							responsePacket = new AuthenticationResponsePacket(AuthResponse.Success, packet.Id);
						}
						else
						{
							responsePacket = new AuthenticationResponsePacket(AuthResponse.AccountAlreadyExists, packet.Id);
						}
					}
				}
				else
				{
					responsePacket = new AuthenticationResponsePacket(AuthResponse.InvalidCredentials, packet.Id);
				}

				client.Send(responsePacket);
			}
		}

		public void RemoveClientFromClientList(Client client) => _clients.Remove(client);
	}
}