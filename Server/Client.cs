using System.Collections.Generic;
using System.Net.Sockets;

using BeefChat.Shared;
using BeefChat.Shared.Logging;
using BeefChat.Shared.Objects;
using BeefChat.Shared.Packets;

namespace BeefChat.Server
{
	public class Client
	{
		public TcpClient TcpClient { get; set; }
		public NetworkStream Stream => TcpClient.GetStream();
		public bool IsAuthenticated { get; set; } = false;
		public string Username { get; set; }
		public Account Account => Program.Server.GetAccount(Username);

		public Client(TcpClient client)
		{
			TcpClient = client;
		}

		public Packet Receive() => Packet.Read(Stream);

		public void Disconnect(string reason, bool banned = false, bool saveAccount = true)
		{
			Logger.Add(LogEntryType.Normal, "Disconnecting " + (banned ? "and banning " : "") + "a client for " + reason);
			try
			{
				var disconnectPacket = new DisconnectPacket(reason, banned);
				disconnectPacket.Send(Stream);
			}
			catch
			{
				//Logger.Add(LogEntryType.Verbose, "Couldn't send disconnect packet to client");
			}
			if (saveAccount && Account != null)
			{
				SaveAccount();
				//Logger.Add(LogEntryType.Verbose, "Account saved");
			}
			Program.Server.RemoveClientFromClientList(this);
			TcpClient.Close();
			//Logger.Add(LogEntryType.Verbose, "Client disconnected");
		}

		public void ChangePicture(byte[] picture) => Account.Profile.Picture = picture;

		public void ChangeStatus(User.UserStatus status) => Account.Profile.Status = status;

		public void SendMessage(Message message) => Send(new MessagePacket(message));

		public void SendChannels(List<Channel> channels) => Send(new ServerChannelsPacket(channels));

		public void SendFriends(List<string> friends) => Send(new UserFriendsPacket(friends));

		public void SendUser(User user) => Send(new UserInformationPacket(user));

		public void Send(Packet packet) => Packet.Send(Stream, packet);

		public void SaveAccount()
		{
			if (Account == null) return;
			Program.Server.SetAccount(Username, Account);
			Logger.Add(LogEntryType.Verbose, "Account saved");
		}
	}
}