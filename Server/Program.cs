﻿using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

using BeefChat.Shared;
using BeefChat.Shared.Logging;

namespace BeefChat.Server
{
	public static class Program
	{
		public static Server Server { get; private set; }

		private static void Main()
		{
			Console.Title = "BeefChat - Server";
			Console.CancelKeyPress += ConsoleCancel;
			Logger.Add(LogEntryType.Normal, $"BeefChat Server running protocol version {Constants.ProtocolVersion} on {RuntimeInformation.OSDescription}", "Server");
			if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
			{
				//Intercept all closing events on Windows
				SetConsoleCtrlHandler(new HandlerRoutine(ConsoleCtrlCheck), true);

				//Disable quick edit mode to prevent server freeze when admin clicks the console.
				int mode;
				bool successful = true;
				var handle = GetStdHandle(-10);
				if (!GetConsoleMode(handle, out mode))
				{
					successful = false;
				}
				//Disable QuickEdit Mode
				mode &= ~0x0040;
				//Disable Mouse Input
				mode &= ~0x0010;
				if (!SetConsoleMode(handle, mode))
				{
					successful = false;
				}
				if (!successful)
				{
					Logger.Add(LogEntryType.Warning, "BeefChat Server couldn't retrieve/set the QuickEdit mode automatically, the server might freeze when the console is clicked");
				}
			}
			if (Shared.Constants.DebugMode)
			{
				Logger.Add(LogEntryType.Warning, "Debug Mode in the shared library is enabled, this will expose every packet type for debugging");
			}

			Logger.Add(LogEntryType.Normal, "Starting", "Server");
			Server = new Server();
			Server.LoadData("data");
			Server.Start(null);
			Logger.Add(LogEntryType.Normal, "Started", "Server");
			Task.Delay(-1).GetAwaiter().GetResult();
		}

		private static void ConsoleCancel(object sender, ConsoleCancelEventArgs args)
		{
			args.Cancel = true;
			Shutdown();
		}

		private static void Shutdown()
		{
			Logger.Add(LogEntryType.Normal, "Shutting down...", "Server");
			Server.Stop();
			Server.WriteData("data");
			Environment.Exit(-1);
		}

		#region Windows Console DLL Calls

		[DllImport("kernel32.dll", SetLastError = true)]
		private static extern IntPtr GetStdHandle(int nStdHandle);

		#region Windows Console Canceling

		// Declare the SetConsoleCtrlHandler function
		// as external and receiving a delegate.
		[DllImport("Kernel32")]
		public static extern bool SetConsoleCtrlHandler(HandlerRoutine Handler, bool Add);

		// A delegate type to be used as the handler routine
		// for SetConsoleCtrlHandler.
		public delegate bool HandlerRoutine(CtrlTypes CtrlType);

		// An enumerated type for the control messages
		// sent to the handler routine.
		public enum CtrlTypes
		{
			CTRL_C_EVENT = 0,
			CTRL_BREAK_EVENT,
			CTRL_CLOSE_EVENT,
			CTRL_LOGOFF_EVENT = 5,
			CTRL_SHUTDOWN_EVENT
		}

		private static bool ConsoleCtrlCheck(CtrlTypes ctrlType)
		{
			Shutdown();
			return true;
		}

		#endregion Windows Console Canceling

		#region Windows Console Modes

		[DllImport("kernel32.dll")]
		private static extern bool GetConsoleMode(IntPtr hConsoleHandle, out int lpMode);

		[DllImport("kernel32.dll")]
		public static extern bool SetConsoleMode(IntPtr hConsoleHandle, int dwMode);

		#endregion Windows Console Modes

		#endregion Windows Console DLL Calls
	}
}