﻿using System;

namespace BeefChat.Shared.Exceptions
{
	public class InvalidCredentialsException : Exception
	{
		public InvalidCredentialsException()
		{
		}

		public InvalidCredentialsException(string message) : base(message)
		{
		}
	}
}