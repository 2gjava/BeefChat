﻿using System;

namespace BeefChat.Shared.Exceptions
{
	/// <summary>
	/// It's being used to throw an exception if entered credentials are reported to be wrong by the server.
	/// </summary>
	public class WrongCredentialsException : Exception
	{
	}
}