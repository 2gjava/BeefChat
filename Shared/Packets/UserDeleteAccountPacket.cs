﻿using MessagePack;

namespace BeefChat.Shared.Packets
{
	[MessagePackObject(true)]
	public class UserDeleteAccountPacket : Packet
	{
		[SerializationConstructor]
		public UserDeleteAccountPacket(string password) => this.Password = password;

		public string Password;
	}
}