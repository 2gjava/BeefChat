﻿using System;
using System.Net.Sockets;

using MessagePack;

namespace BeefChat.Shared.Packets
{
	[Union(0, typeof(AuthenticationRequestPacket))]
	[Union(1, typeof(AuthenticationResponsePacket))]
	[Union(2, typeof(ChangeProfilePacket))]
	[Union(3, typeof(DisconnectPacket))]
	[Union(4, typeof(ManageFriendPacket))]
	[Union(5, typeof(RequestUserPacket))]
	[Union(6, typeof(ServerChannelsPacket))]
	[Union(7, typeof(UserFriendsPacket))]
	[Union(8, typeof(UserInformationPacket))]
	[Union(10, typeof(MessagePacket))]
	[Union(11, typeof(UserDeleteAccountPacket))]
	public abstract class Packet
	{
		[Key("id")]
		public Guid Id = Guid.NewGuid();

		[Key("responseId")]
		public Guid? ResponseId = null;

		public static Packet Read(NetworkStream stream)
		{
			//Read how long the packet will be
			var lengthBuffer = new byte[4];
			stream.Read(lengthBuffer, 0, 4);
			var length = Conversion.ToInteger(lengthBuffer);
			if (length == 0)
			{
				return null;
			}
			//Get Data
			var packetBuffer = new byte[length];
			stream.Read(packetBuffer, 0, length);
			if (packetBuffer.Length == 0)
			{
				return null;
			}
			//Serialize to Packet and return it
			Packet p = LZ4MessagePackSerializer.Deserialize<Packet>(packetBuffer);
			if (Shared.Constants.DebugMode)
			{
				Logging.Logger.Add(Logging.LogEntryType.Debug, "Receiving packet: " + p.GetType().ToString());
			}
			return p;
		}

		public static void Send(NetworkStream stream, Packet p)
		{
			if (Shared.Constants.DebugMode)
			{
				Logging.Logger.Add(Logging.LogEntryType.Debug, "Sending packet: " + p.GetType().ToString());
			}
			if (p == null) throw new ArgumentNullException(nameof(p));
			var packet = LZ4MessagePackSerializer.Serialize(p);
			var length = Conversion.ToByteArray(packet.Length);
			stream.Write(length, 0, 4);
			stream.Write(packet, 0, packet.Length);
			stream.Flush();
		}

		public void Send(NetworkStream stream) => Send(stream, this);
	}
}