﻿using MessagePack;

namespace BeefChat.Shared.Packets
{
	[MessagePackObject(true)]
	public class ManageFriendPacket : Packet
	{
		[SerializationConstructor]
		public ManageFriendPacket(bool add, string username)
		{
			this.Add = add;
			this.Username = username;
		}

		public bool Add;

		public string Username;
	}
}