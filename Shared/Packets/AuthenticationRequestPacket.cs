﻿using System.Net;

using MessagePack;

namespace BeefChat.Shared.Packets
{
	[MessagePackObject(true)]
	public class AuthenticationRequestPacket : Packet
	{
		public AuthenticationRequestPacket(NetworkCredential credential, bool register)
		{
			this.Username = credential.UserName;
			this.Password = credential.Password;
			this.Register = register;
		}

		[SerializationConstructor]
		public AuthenticationRequestPacket(string username, string password, bool register)
		{
			this.Username = username;
			this.Password = password;
			this.Register = register;
		}

		public string Username;

		public string Password;

		public bool Register;
	}
}