﻿using System.Collections.Generic;
using BeefChat.Shared.Objects;
using MessagePack;

namespace BeefChat.Shared.Packets
{
	[MessagePackObject(true)]
	public class ServerChannelsPacket : Packet
	{
		[SerializationConstructor]
		public ServerChannelsPacket(List<Channel> channels) => this.Channels = channels;

		public List<Channel> Channels;
	}
}