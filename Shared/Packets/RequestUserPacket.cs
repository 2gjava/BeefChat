﻿using MessagePack;

namespace BeefChat.Shared.Packets
{
	[MessagePackObject(true)]
	public class RequestUserPacket : Packet
	{
		[SerializationConstructor]
		public RequestUserPacket(string username) => this.Username = username;

		public string Username;
	}
}