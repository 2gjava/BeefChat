﻿using System.Collections.Generic;

using MessagePack;

namespace BeefChat.Shared.Packets
{
	[MessagePackObject(true)]
	public class UserFriendsPacket : Packet
	{
		[SerializationConstructor]
		public UserFriendsPacket(List<string> friends) => this.Friends = friends;

		public List<string> Friends;
	}
}