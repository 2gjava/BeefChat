﻿using BeefChat.Shared.Objects;
using MessagePack;

namespace BeefChat.Shared.Packets
{
	[MessagePackObject(true)]
	public class UserInformationPacket : Packet
	{
		[SerializationConstructor]
		public UserInformationPacket(User profile) => this.Profile = profile;

		public User Profile;
	}
}