﻿using System;

using MessagePack;

using static BeefChat.Shared.Enums;

namespace BeefChat.Shared.Packets
{
	[MessagePackObject(true)]
	public class AuthenticationResponsePacket : Packet
	{
		[SerializationConstructor]
		public AuthenticationResponsePacket(AuthResponse response) => this.Response = response;

		public AuthenticationResponsePacket(AuthResponse response, Guid responseId)
		{
			this.Response = response;
			this.ResponseId = responseId;
		}

		[IgnoreMember]
		public bool Successful => Response == AuthResponse.Success;

		public AuthResponse Response;
	}
}