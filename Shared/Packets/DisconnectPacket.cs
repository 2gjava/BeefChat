﻿using MessagePack;

namespace BeefChat.Shared.Packets
{
	[MessagePackObject(true)]
	public class DisconnectPacket : Packet
	{
		[SerializationConstructor]
		public DisconnectPacket(string reason, bool banned = false)
		{
			this.Reason = reason;
			this.Banned = banned;
		}

		public string Reason;

		public bool Banned;
	}
}