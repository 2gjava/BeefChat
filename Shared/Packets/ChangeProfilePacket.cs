﻿using MessagePack;

namespace BeefChat.Shared.Packets
{
	[MessagePackObject(true)]
	public class ChangeProfilePacket : Packet
	{
		/// <summary>
		/// Sent when the user wants to change something at their profile
		/// </summary>
		/// <param name="key">1 = Status
		/// 2 = Status Text
		/// 3 = Nickname
		/// 4 = Picture
		/// 5 = Color</param>
		/// <param name="value"></param>
		/// [SerializationConstructor]
		public ChangeProfilePacket(int key, object value)
		{
			this.Key = key;
			this.Value = value;
		}

		public int Key;

		public object Value;
	}
}