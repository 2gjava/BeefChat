﻿using BeefChat.Shared.Objects;
using MessagePack;

namespace BeefChat.Shared.Packets
{
	[MessagePackObject(true)]
	public class MessagePacket : Packet
	{
		[SerializationConstructor]
		public MessagePacket(Message message) => this.Message = message;

		[IgnoreMember]
		public Channel Channel => Message.Channel;

		public Message Message;
	}
}