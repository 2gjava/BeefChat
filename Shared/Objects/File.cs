﻿using System.IO;
using MessagePack;

namespace BeefChat.Shared.Objects
{
	[MessagePackObject(true)]
	public class File : Attachment
	{
		/// <summary>
		/// The file name
		/// </summary>
		/// <example>Image.png</example>
		public string Name { get; set; }

		/// <summary>
		/// The file content
		/// </summary>
		public byte[] Content { get; set; }

		/// <summary>
		/// Creates an <see cref="File"/> <see cref="Attachment"/> from an actual file.
		/// </summary>
		/// <param name="filePath">The path to the file</param>
		public File(string filePath)
		{
			this.Name = Path.GetFileName(Name);
			this.Content = System.IO.File.ReadAllBytes(filePath);
		}

		[SerializationConstructor]
		public File(string name, byte[] content)
		{
			this.Name = name;
			this.Content = content;
		}
	}
}