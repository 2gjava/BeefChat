﻿using System;
using MessagePack;

namespace BeefChat.Shared.Objects
{
	/// <summary>
	/// Class for creating message attachments.
	/// </summary>
	[Union(1, typeof(Contact))]
	[Union(0, typeof(File))]
	public abstract class Attachment
	{
		/// <summary>
		/// Unique Id for every <seealso cref="Attachment"/>.
		/// </summary>
		public Guid Id;

		[SerializationConstructor]
		public Attachment() => Id = Guid.NewGuid();
	}
}