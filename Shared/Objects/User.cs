﻿using MessagePack;

namespace BeefChat.Shared.Objects
{
	[MessagePackObject(true)]
	public class User
	{
		public static User ExampleUser { get; } = new User()
		{
			Admin = true,
			Friend = true,
			Color = "#FF9999",
			Status = UserStatus.Online,
			Nickname = "Example User",
			Username = "exampleuser",
			StatusText = "Working on BeefChat",
			Picture = null
		};

		[SerializationConstructor]
		public User()
		{
		}

		public User(User user)
		{
			this.Username = user.Username;
			this.Nickname = user.Nickname;
			this.Status = user.Status;
			this.StatusText = user.StatusText;
			this.Picture = user.Picture;
			this.Color = user.Color;
			this.Admin = user.Admin;
			this.Friend = user.Friend;
		}

		public string Username { get; set; }

		private string nickname { get; set; } = null;

		[IgnoreMember]
		public string Nickname
		{
			set => nickname = value;
			get => string.IsNullOrWhiteSpace(nickname) ? Username : nickname;
		}

		private string statusText { get; set; } = null;

		[IgnoreMember]
		public string StatusText
		{
			get => string.IsNullOrWhiteSpace(statusText) ? Status.ToString() : statusText;
			set => statusText = value;
		}

		public UserStatus Status { get; set; } = UserStatus.Online;
		public byte[] Picture { get; set; }
		public string Color { get; set; } = "#999999";

		public bool Admin { get; set; }
		public bool Friend { get; set; }

		public enum UserStatus : int
		{
			Offline = 0,
			Online = 1,
			Away = 2,
			Busy = 3,
		}
	}
}