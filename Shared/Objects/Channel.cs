﻿using MessagePack;

namespace BeefChat.Shared.Objects
{
	[MessagePackObject(true)]
	public class Channel
	{
		public string Name { get; set; }
		public string Topic { get; set; } = "";
		public ChannelType Type { get; set; }

		public Channel()
		{
		}

		public Channel(string name)
		{
			Name = name.Remove(0, 1);
			var type = ToType(name[0]);
			if (type != null) Type = type.Value;
		}

		public Channel(string name, ChannelType type)
		{
			Name = name;
			Type = type;
		}

		public Channel(string name, string topic)
		{
			Name = name.Remove(0);
			var type = ToType(name[0]);
			if (type != null) Type = type.Value;
			Topic = topic;
		}

		[SerializationConstructor]
		public Channel(string name, string topic, ChannelType type)
		{
			Name = name;
			Type = type;
			Topic = topic;
		}

		/// <summary>
		/// Returns a compacted channel identifier (e.g. #general)
		/// </summary>
		/// <returns></returns>
		public override string ToString() => ToChar(Type) + Name;

		#region Channel Type

		public enum ChannelType
		{
			Public,
			Group,
			User
		}

		public static ChannelType? ToType(char type)
		{
			switch (type)
			{
				case '#': return ChannelType.Public;
				case '!': return ChannelType.Group;
				case '@': return ChannelType.User;
				default: return null;
			}
		}

		public static char? ToChar(ChannelType type)
		{
			switch (type)
			{
				case ChannelType.Public: return '#';
				case ChannelType.Group: return '!';
				case ChannelType.User: return '@';
				default: return null;
			}
		}

		#endregion Channel Type
	}
}