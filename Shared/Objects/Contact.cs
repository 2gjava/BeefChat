﻿using System;
using MessagePack;

namespace BeefChat.Shared.Objects
{
	[MessagePackObject(true)]
	public class Contact : Attachment
	{
		public ContactPlatform Platform { get; set; } = ContactPlatform.BeefChat;
		public string Username { get; set; }

		[SerializationConstructor]
		public Contact(ContactPlatform platform, string username)
		{
			switch (platform)
			{
				case ContactPlatform.Email:
					if (!username.Contains("@")) throw new ArgumentException("Entered \"username\" isn't an email address. @ missing", nameof(username));
					break;

				case ContactPlatform.Discord:
					if (!username.Contains("#")) throw new ArgumentException("Entered \"username\" isn't an discord tag. # missing", nameof(username));
					break;
			}
			this.Platform = platform;
			this.Username = username;
		}
	}

	public enum ContactPlatform : int
	{
		Default,
		Email,
		BeefChat,
		Discord,
		Skype,
	}
}