﻿using System;
using System.Collections.Generic;
using MessagePack;

namespace BeefChat.Shared.Objects
{
	[MessagePackObject(true)]
	public class Message
	{
		public Channel Channel { get; set; }
		public string Author { get; set; }
		public string Content { get; set; } = "";
		public Guid Id { get; set; }

		public List<Attachment> Attachments { get; set; } = new List<Attachment>();

		public Message(Message message)
		{
			this.Channel = message.Channel;
			this.Author = message.Author;
			this.Content = message.Content;
			this.Id = message.Id;
			this.Attachments = message.Attachments;
		}

		public Message(Channel channel, string author, string content)
		{
			Id = Guid.NewGuid();
			Channel = channel;
			Author = author;
			Content = content;
		}

		public Message(Channel channel, string author, List<Attachment> attachments)
		{
			this.Id = Guid.NewGuid();
			this.Channel = channel;
			this.Author = author;
			this.Attachments.AddRange(attachments);
		}

		public Message(Channel channel, string author, Attachment attachment)
		{
			this.Id = Guid.NewGuid();
			this.Channel = channel;
			this.Author = author;
			this.Attachments.Add(attachment);
		}

		[SerializationConstructor]
		public Message(Guid id, Channel channel, string author, string content, List<Attachment> attachments)
		{
			this.Id = id;
			this.Channel = channel;
			this.Author = author;
			this.Content = content;
			this.Attachments = attachments;
		}
	}
}