﻿using System;
using System.Text;

namespace BeefChat.Shared
{
	public static class Conversion
	{
		#region Bytes <-> Int32

		//Taken from https://stackoverflow.com/a/1318948/7972419
		/// <summary>
		/// Converts a <see cref="int"/> to an <see cref="byte[]"/>
		/// </summary>
		/// <param name="integer">The <see cref="int"/> to be converted to a <see cref="byte[]"/></param>
		public static byte[] ToByteArray(int integer)
		{
			byte[] intBytes = BitConverter.GetBytes(integer);
			if (BitConverter.IsLittleEndian)
				Array.Reverse(intBytes);
			return intBytes;
		}

		//Taken from https://stackoverflow.com/a/1318948/7972419 (reversed by myself)
		/// <summary>
		/// Converts an <see cref="byte[]"/> to an <see cref="int"/>
		/// </summary>
		/// <param name="bytes">The <see cref="byte[]"/> to be converted to an <see cref="int"/></param>
		public static int ToInteger(byte[] bytes)
		{
			if (BitConverter.IsLittleEndian)
				Array.Reverse(bytes);
			int integer = BitConverter.ToInt32(bytes, 0);
			return integer;
		}

		#endregion Bytes <-> Int32

		#region Bytes <-> String

		public static string ToString(this byte[] bytes) => Encoding.Default.GetString(bytes);

		public static byte[] ToBytes(this string String) => Encoding.Default.GetBytes(String);

		#endregion Bytes <-> String
	}
}