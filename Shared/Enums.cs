﻿namespace BeefChat.Shared
{
	public static class Enums
	{
		public enum AuthResponse
		{
			Success,
			AccountAlreadyExists,
			InvalidCredentials,
			WrongCredentials,
			Banned,
		}
	}
}