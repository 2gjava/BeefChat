﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace BeefChat.Shared.Logging
{
	public class LogEntry
	{
		public LogEntryType Type;
		public string Message;
		public string Application;

		public LogEntry(LogEntryType type, string message, string application)
		{
			this.Type = type;
			this.Message = message;
			this.Application = application;
		}
	}

	public enum LogEntryType
	{
		Normal,
		Warning,
		Error,
		Verbose,
		Debug
	}

	public static class Logger
	{
		private const string Format = "[{0}] {2}";
		public static bool Verbose = Environment.GetCommandLineArgs().Contains("--verbose");
		public static readonly List<LogEntry> LogEntries = new List<LogEntry>();

		public delegate void EntryAddedEvent(LogEntry entry);

		public static event EntryAddedEvent EntryAdded;

		public static void Add(LogEntryType type, string message, string application = null)
		{
			if (application == null)
				application = new StackFrame(1, true).GetMethod().Name;
			Add(new LogEntry(type, message, application));
		}

		public static void Add(LogEntry entry)
		{
#if !DEBUG
            if (logType == LogEntryType.Debug) return;
#endif
			LogEntries.Add(entry);
			if (Verbose && entry.Type == LogEntryType.Verbose) return;
			EntryAdded?.Invoke(entry);
			Console.ForegroundColor = entry.Type.GetConsoleColor();
			Console.WriteLine(Format, entry.Application, GetName(entry.Type), entry.Message);
			Console.ResetColor();
		}

		private static ConsoleColor GetConsoleColor(this LogEntryType logType)
		{
			switch (logType)
			{
				default:
				case LogEntryType.Normal: return ConsoleColor.Gray;
				case LogEntryType.Warning: return ConsoleColor.Yellow;
				case LogEntryType.Error: return ConsoleColor.Red;
				case LogEntryType.Debug: return ConsoleColor.Green;
				case LogEntryType.Verbose: return ConsoleColor.DarkGray;
			}
		}

		private static string GetName(this LogEntryType logType)
		{
			switch (logType)
			{
				case LogEntryType.Normal: return "Normal";
				case LogEntryType.Warning: return "Warning";
				case LogEntryType.Error: return "Error";
				case LogEntryType.Debug: return "Debug";
				case LogEntryType.Verbose: return "Verbose";
				default: return string.Empty;
			}
		}
	}
}