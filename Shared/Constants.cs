﻿namespace BeefChat.Shared
{
	public static class Constants
	{
		public const int ProtocolVersion = 2;
        public const bool DebugMode = true;
		public const int PortNumber = 42210;
	}
}