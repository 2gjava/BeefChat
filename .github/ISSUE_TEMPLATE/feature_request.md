---
name: Feature request
about: Suggest an idea for this project

---

**Which application edition**
ClientGUI and/or Server

**Is your feature request related to a problem? Please describe.**
A clear and concise description of what the problem is. Ex. I'm always frustrated when [...]

**The design of feature**
Sketches or description of how the feature will look like in the UI.

**Describe the solution you'd like**
A clear and concise description of what you want to happen.

**Describe alternatives you've considered**
A clear and concise description of any alternative solutions or features you've considered.

**Resources that could be used**
Add libraries you might have thought they could help the developers implement that feature.

**Additional context**
Add any other context or screenshots about the feature request here.
