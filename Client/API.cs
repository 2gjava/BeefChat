﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Net.Sockets;
using System.Threading;

using BeefChat.Shared;
using BeefChat.Shared.Exceptions;
using BeefChat.Shared.Logging;
using BeefChat.Shared.Objects;
using BeefChat.Shared.Packets;

using static BeefChat.Shared.Enums;

namespace BeefChat.Client
{
	/// <summary>
	/// Contains functions and properties to interact to a server.
	/// </summary>
	public sealed partial class Client : IDisposable
	{
		public string Username { get; private set; }

		public List<Channel> Channels { get; } = new List<Channel>();

		public List<string> Friends { get; } = new List<string>();

		#region Events

		#region Message Received

		public delegate void MessageReceived(Message eventArgs);

		public event MessageReceived MessageReceivedEventHandler;

		#endregion Message Received

		#region Disconnected

		public delegate void Disconnected(string reason);

		public event Disconnected DisconnectedEventHandler;

		#endregion Disconnected

		#region Channels Received

		public delegate void ChannelsReceived();

		public event ChannelsReceived ChannelsReceivedEventHandler;

		#endregion Channels Received

		#region Friends Received

		public delegate void FriendsReceived();

		public event FriendsReceived FriendsReceivedEventHandler;

		#endregion Friends Received

		#region User Info Received

		public delegate void UserInfoReceived(User user);

		public event UserInfoReceived UserInfoReceivedEventHandler;

		#endregion User Info Received

		#endregion Events

		#region Functions

		#region Connecting & Disconnecting

		/// <summary>
		/// Discards the current connection and connects to the specified server.
		/// </summary>
		public void Connect(string ipAddress, int port = Constants.PortNumber)
		{
			if (ipAddress == null) throw new ArgumentNullException(nameof(ipAddress));
			if (port > 65535 || port < 0) throw new ArgumentOutOfRangeException(nameof(port), "Port is not in valid range. 0-65535");
			Disconnect();
			Logger.Add(LogEntryType.Debug, $"Connecting to '{ipAddress}:{port}'", "Client");
			_tcpClient.Connect(ipAddress, port);

			_serverThread = new Thread(HandleServer) { Name = "Server Thread", Priority = ThreadPriority.Highest };
			_serverThread.Start();

			Logger.Add(LogEntryType.Debug, "Connected", "Client");
		}

		/// <summary>
		/// Disconnects from the current server.
		/// </summary>
		public void Disconnect()
		{
			_tcpClient?.Close();
			_tcpClient = new TcpClient();
			_serverThread?.Abort();
			Logger.Add(LogEntryType.Verbose, "Disconnected", "Client");
		}

		public bool IsConnected()
		{
			if (_tcpClient == null) return false;
			if (_serverThread == null) return false;

			return _tcpClient.Connected;
		}

		#endregion Connecting & Disconnecting

		#region Authentication

		/// <summary>
		/// Logs into an account at the currently connected server.
		/// </summary>
		/// <remarks>The username parameter will be always lowercased.</remarks>
		/// <param name="username">Username to login with</param>
		/// <param name="password">Password to login with</param>
		public void Login(string username, string password) => Authenticate(false, username, password);

		/// <summary>
		/// Registers an new account at the currently connected server.
		/// </summary>
		/// <remarks>The username parameter will be always lowercased.</remarks>
		/// <param name="username">Username to login with</param>
		/// <param name="password">Password to login with</param>
		public void Register(string username, string password) => Authenticate(true, username, password);

		private void Authenticate(bool register, string username, string password)
		{
			//Check if any of the needed resources are null.
			if (username == null) throw new ArgumentNullException(nameof(username));
			if (password == null) throw new ArgumentNullException(nameof(password));
			if (Stream == null) throw new NullReferenceException("Client is disposed or not connected.");
			var requestPacket = new AuthenticationRequestPacket(username.ToLower(), password, register);
			requestPacket.Send(Stream);
			var responsePacket = (AuthenticationResponsePacket)WaitForPacket(requestPacket.Id);
			switch (responsePacket.Response)
			{
				case AuthResponse.Success: Username = username.ToLower(); return;
				case AuthResponse.WrongCredentials: throw new WrongCredentialsException();
				case AuthResponse.InvalidCredentials: throw new InvalidCredentialsException();
				case AuthResponse.AccountAlreadyExists: throw new AccountAlreadyExistsException();
				case AuthResponse.Banned: throw new BannedException();
			}
		}

		#endregion Authentication

		#region Sending

		/// <summary>
		/// Sends a <paramref name="message"/>
		/// </summary>
		public void SendMessage(Message message)
		{
			if (message == null) throw new ArgumentNullException(nameof(message));
			new MessagePacket(message).Send(Stream);
		}

		/// <summary>
		/// Sends the <paramref name="message"/> to the <paramref name="channel"/>
		/// </summary>
		/// <param name="channel">Where the <paramref name="message"/> should be sent to.</param>
		/// <param name="message">Content of the <paramref name="message"/></param>
		public void SendMessage(Channel channel, string message) => SendMessage(new Message(channel, "", message));

		/// <summary>
		/// Sends a <paramref name="file"/>
		/// </summary>
		public void SendFile(Channel channel, byte[] file, string name, bool compress)
		{
			if (Stream == null) throw new NullReferenceException("Client is disposed or not connected.");
			if (channel == null) throw new ArgumentNullException(nameof(channel));
			if (file == null) throw new ArgumentNullException(nameof(file));
			if (name == null) throw new ArgumentNullException(nameof(name));
			if (compress)
			{
				var oldLength = file.Length;
				file = Compression.Compress(file);
				Logger.Add(LogEntryType.Debug, "Compression saved " + (oldLength - file.Length).ToString() + " bytes");
			}
			new MessagePacket(new Message(channel, name, new File(name, file))).Send(Stream);
		}

		#endregion Sending

		#region Change Profile Information

		/// <summary>
		/// Changes the nickname of the current user
		/// </summary>
		/// <param name="nickname">The new nickname</param>
		public void ChangeNickname(string nickname) => new ChangeProfilePacket(3, nickname).Send(Stream);

		/// <summary>
		/// Changes the profile picture of the current user
		/// </summary>
		/// <param name="picture">The new profile picture</param>
		public void ChangePicture(byte[] picture) => new ChangeProfilePacket(4, picture).Send(Stream);

		/// <summary>
		/// Changes the profile color of the current user
		/// </summary>
		/// <param name="color">The new profile color. </param>
		public void ChangeColor(string color) => new ChangeProfilePacket(5, color).Send(Stream);

		#endregion Change Profile Information

		/// <summary>
		/// Requests the profile from an user
		/// </summary>
		public void RequestProfile(string username) => new RequestUserPacket(username.ToLower()).Send(Stream);

		/// <summary>
		/// Gets the profile from an user
		/// </summary>
		/// <returns>The profile of the requested user.</returns>
		public User GetProfile(string username)
		{
			if (username == null) throw new ArgumentNullException(nameof(username));
			username = username.ToLower();
			if (UserCache.ContainsKey(username))
			{
				return UserCache[username];
			}
			//Profile couldn't be found, we are asking the server then if it can give us information.
			RequestProfile(username);
			return null;
		}

		#region Friend List

		/// <summary>
		/// Adds an user from the client's friend-list.
		/// </summary>
		/// <param name="username"></param>
		public void AddFriend(string username)
		{
			if (username == null) throw new ArgumentNullException(nameof(username));
			new ManageFriendPacket(true, username).Send(Stream);
		}

		/// <summary>
		/// Removes an user from the client's friend-list.
		/// </summary>
		/// <param name="username"></param>
		public void RemoveFriend(string username)
		{
			if (username == null) throw new ArgumentNullException(nameof(username));
			new ManageFriendPacket(false, username).Send(Stream);
		}

		#endregion Friend List

		public void DeleteAccount(string password)
		{
			var requestPacket = new UserDeleteAccountPacket(password);
			requestPacket.Send(Stream);
		}

		#endregion Functions
	}
}