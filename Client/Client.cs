﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Threading;

using BeefChat.Shared;
using BeefChat.Shared.Objects;
using BeefChat.Shared.Packets;

using BeefChat.Shared.Exceptions;
using BeefChat.Shared.Logging;

namespace BeefChat.Client
{
	public sealed partial class Client : IDisposable
	{
		public void ClearUserCache()
		{
			Logger.Add(LogEntryType.Debug, "Clearing User Cache...");
			UserCache.Clear();
			Logger.Add(LogEntryType.Debug, "User Cache cleared");
		}

		public void Dispose() => _tcpClient?.Close();

		#region Properties

		private TcpClient _tcpClient;
		private Thread _serverThread;
		private List<Packet> PendingPackets { get; } = new List<Packet>();
		private Dictionary<string, User> UserCache { get; } = new Dictionary<string, User>();

		private NetworkStream Stream => _tcpClient.GetStream();

		#endregion Properties

		#region Functions

		/// <summary>
		/// Function for handling the responses of the server.
		/// </summary>
		private void HandleServer()
		{
			while (_tcpClient.Connected)
			{
				var packet = Packet.Read(Stream);
				if (packet == null)
				{
					break;
				}
				switch (packet)
				{
					case UserInformationPacket p:
						if (p.Profile != null)
						{
							UserCache[p.Profile.Username] = p.Profile;
							UserInfoReceivedEventHandler?.Invoke(UserCache[p.Profile.Username]);
						}
						break;

					case MessagePacket p:
						foreach (Attachment a in p.Message.Attachments)
						{
							if (a.GetType() == typeof(File))
							{
								(a as File).Content = Compression.Decompress((a as File).Content);
							}
						}
						MessageReceivedEventHandler?.Invoke(p.Message);
						break;

					case ServerChannelsPacket p:
						Channels.Clear();
						foreach (Channel channel in p.Channels) Channels.Add(channel);
						ChannelsReceivedEventHandler?.Invoke();
						break;

					case UserFriendsPacket p:
						Friends.Clear();
						foreach (string friend in p.Friends) Friends.Add(friend);
						FriendsReceivedEventHandler?.Invoke();
						break;

					case DisconnectPacket p:
						Logger.Add(LogEntryType.Verbose, p.Reason);
						DisconnectedEventHandler?.Invoke(p.Reason);
						Disconnect();
						break;

					default: PendingPackets.Add(packet); break;
				}
			}
			Logger.Add(LogEntryType.Verbose, "Lost connection to server");
			DisconnectedEventHandler?.Invoke("Lost connection to the server.");
		}

		private Packet WaitForPacket(Guid responseId)
		{
			//Logger.Add(LogEntryType.Debug, $"Waiting for packet with the response id {responseId.ToString()}");
			while (PendingPackets.All(p => p.ResponseId != responseId))
			{
				Thread.Sleep(1);
			}
			//Logger.Add(LogEntryType.Debug, $"Got packet with response id {responseId.ToString()}");
			var packet = PendingPackets.First(p => p.ResponseId == responseId);
			lock (PendingPackets)
			{
				PendingPackets.Remove(packet);
			}
			return packet;
		}

		#endregion Functions
	}
}