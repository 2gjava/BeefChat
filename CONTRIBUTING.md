# Contributing Guidelines
## Making good code changes
Please try to follow the style of the code
- Name all Windows Forms components like Text + Control Name = e.g. TestButton.
- Try to comment difficult code sections (not necessary but might help other developers)
  - XML documentation would be also nice
- Code used by both Server and Client belong into Shared which is referenced by all three Server, Client and ClientGUI.
  - If a class is used by other code and is used to define parts of the chat application (like Messages, Users etc.) put it into the Objects namespace.
  - If a exception is used by other code put it into the Exceptions namespace of Shared.
- Try to always use brackets that we have consistent style
> If you have any other question about the code styling contact us at craftplacer@outlook.de, and missing information will be added.

## Making good issues
Your issues should have all necessary information for the developers. 

### If it is a suggestion or pull request then include the following information
- Type of suggestion
- Explaination of that suggestion (what does it do, how it should be)
- Why we should take it
### If it isn't a pull request and just a suggestion/issue
- Design (UI components, layout etc.)
- Resources that could be used (other libraries that developers could use to implement that suggestion)

> Note: People may critic the suggestion/pull request and discuss certain points to either don't make into the application or to make it better.

### If it is a bug report then include the following information
- Where it occured
- What you did/wanted to do (Which window, button etc. was clicked)
- Expected result
- What happened instead (e.g. Crash Dialog showed up)
- Any details given by the application (Exception details etc.)

## Behaviour in our community
Use common sense and behave nicely to see a complete document [visit this link](.github/CODE_OF_CONDUCT.md).
