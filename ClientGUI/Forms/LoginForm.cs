﻿using System;
using System.Windows.Forms;

using MaterialSkin.Controls;

namespace BeefChat.ClientGUI.Forms
{
	public partial class LoginForm : MaterialForm
	{
		public string IpAddress => IpAddressTextBox.Text;
		public string Username => UsernameTextBox.Text;
		public string Password => PasswordTextBox.Text;

		public LoginForm()
		{
			InitializeComponent();

			Program.Msm.AddFormToManage(this);
		}

		private void ButtonClick(object sender, EventArgs e) => Close();

		private void SaveCredentials()
		{
			if (RememberCheckBox.Checked) CredentialUtil.SetCredentials(IpAddress, Username, Password, CredentialManagement.PersistanceType.LocalComputer);
		}

		private void LoadCredentials()
		{
			var creden = CredentialUtil.GetCredential(IpAddress);
			if (creden == null) return;
			UsernameTextBox.Text = creden.Username;
			PasswordTextBox.Text = creden.Password;
			RememberCheckBox.Checked = true;
		}

		private void SetText()
		{
			CloseButton.Text = Localization.GetString("Button_Close");
			LoginButton.Text = Localization.GetString("Button_Login");
			PasswordTextBox.Hint = Localization.GetString("Password");
			RegisterButton.Text = Localization.GetString("Button_Register");
			RememberCheckBox.Text = Localization.GetString("RememberMe");
			this.Text = Localization.GetString("LoginForm_Text");
			UsernameTextBox.Hint = Localization.GetString("Username");
		}

		private void LoginForm_Load(object sender, EventArgs e)
		{
			LoadCredentials();
			SetText();

			ButtonPanel.BackColor = Program.Msm.GetApplicationBackgroundColor();
			ContentPanel.BackColor = Program.Msm.GetApplicationBackgroundColor();
			BannerPanel.BackColor = Program.Msm.ColorScheme.PrimaryColor;
			BannerPanel.BackgroundImage = Program.GetIcon("forum", 64, Program.Msm.ColorScheme.LightPrimaryColor);
			var args = Environment.GetCommandLineArgs();
			if (args.Length > 1)
			{
				if (Uri.TryCreate(args[1], UriKind.Absolute, out var uri))
				{
					switch (uri.Scheme.ToLower())
					{
						case Program.UriScheme:
							break;

						case Program.FriendUriScheme:
							break;
					}
					IpAddressTextBox.Text = uri.Host;
					if (!String.IsNullOrWhiteSpace(uri.UserInfo))
					{
						UsernameTextBox.Text = uri.UserInfo.Split(':')[0];
						if (uri.UserInfo.Contains(":"))
						{
							PasswordTextBox.Text = uri.UserInfo.Split(':')[1];
						}
					}
				}
			}
			foreach (Control ctl in ContentPanel.Controls) ctl.BackColor = Program.Msm.GetApplicationBackgroundColor();
		}

		private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			SaveCredentials();
			if (DialogResult == DialogResult.Cancel) Program.Shutdown();
		}
	}
}