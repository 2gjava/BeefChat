﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin.Animations;

namespace BeefChat.ClientGUI.Forms
{
    public partial class NotificationForm : Form
    {
        public new Image Icon { get; set; } = null;
        public new string Text { get; set; }
        public string Title { get; set; }
        public string ReplyText => ReplyTextBox.Text;
        private AnimationManager fadeAnimation = new AnimationManager()
        {
             AnimationType = AnimationType.EaseInOut,
             Increment = 0.1,
        };
        private bool isClosing = false;
        public bool Reply
        {
            get => ReplyButton.Visible;
            set => ReplyButton.Visible = value;
        }

        public NotificationForm()
        {
            InitializeComponent();
            fadeAnimation.OnAnimationProgress += FadeAnimation_OnAnimationProgress;
        }
        public NotificationForm(string title, string text, Image icon = null, bool reply = false)
        {
            InitializeComponent();
            fadeAnimation.OnAnimationProgress += FadeAnimation_OnAnimationProgress;
            this.Title = title;
            this.Text = text;
            this.Icon = icon;
            this.Reply = reply;
        }

        private void NotificationForm_Load(object sender, EventArgs e)
        {
            var area = Screen.PrimaryScreen.WorkingArea;
            this.Location = new Point(
                area.Width - this.Width - 8,
                area.Height - this.Height - 8);
            SendReplyButton.Image = Program.GetIcon("send", 24, Color.White);
            CloseButton.Image = Program.GetIcon("close", 24, Color.FromArgb(128, Program.Msm.GetPrimaryTextColor()));
            
        }

        private void FadeAnimation_OnAnimationProgress(object sender) => this.Opacity = fadeAnimation.GetProgress();

        private void NotificationForm_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(Program.Msm.GetApplicationBackgroundColor());
            
            //Application Icon
            var beefchatIcon = Program.GetIcon("forum", 16, Program.Msm.ColorScheme.PrimaryColor);
            e.Graphics.DrawImage(beefchatIcon, 16, 16);
            //Secondary Text
            var secondaryText = Program.ApplicationName;
            e.Graphics.DrawString(secondaryText, Program.Msm.GetFont(false,10f), Program.Msm.GetPrimaryTextBrush(), 40,16);
            //Icon
            if (Icon != null)
            {
                e.Graphics.DrawImage(Drawing.ClipToCircle(Icon, new Size(36, 36)), 16, 48);
            }
            var textX = Icon != null ? 65 : 16;
            //Title
            if (!string.IsNullOrWhiteSpace(Title))
            {
                e.Graphics.DrawString(Title, Program.Msm.ROBOTO_MEDIUM_11, Program.Msm.GetPrimaryTextBrush(), textX, 50);
            }
            //Text
            if (!string.IsNullOrWhiteSpace(Text))
            {
                e.Graphics.DrawString(Text, Program.Msm.ROBOTO_REGULAR_11, Program.Msm.GetPrimaryTextBrush(), textX, 70);
            }
        }

        private void CloseButton_Click(object sender, EventArgs e) => this.Close();

        private void SendReplyButton_Click(object sender, EventArgs e) => this.Close();

        private void ButtonsPanel_Paint(object sender, PaintEventArgs e)
        {
            ButtonsPanel.BackColor = Program.Msm.GetDividersColor();
            ReplyButton.BackColor = Program.Msm.GetDividersColor();
        }

        private void ReplyButton_Click(object sender, EventArgs e) => ShowReplyBox();

        void ShowReplyBox()
        {
            ButtonsPanel.Hide();
            ReplyPanel.Show();
        }

        void HideReplyBox()
        {
            ButtonsPanel.Show();
            ReplyPanel.Hide();
        }

        private void ReplyPanel_Paint(object sender, PaintEventArgs e)
        {
            ReplyPanel.BackColor = Program.Msm.ColorScheme.PrimaryColor;
            ReplyTextBox.BackColor = Program.Msm.ColorScheme.PrimaryColor;
        }

        private void ReplyTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode== Keys.Enter)
            {
                SendReplyButton.PerformClick();
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        

        private void ReplyTextBox_TextChanged(object sender, EventArgs e) => SendReplyButton.Enabled = !string.IsNullOrWhiteSpace(ReplyTextBox.Text);
        private void NotificationForm_Shown(object sender, EventArgs e) => fadeAnimation.StartNewAnimation(AnimationDirection.In);
        private void NotificationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!isClosing)
            {
                e.Cancel = true;
                isClosing = true;
                fadeAnimation.OnAnimationFinished += (s) => { this.Close(); };
                fadeAnimation.StartNewAnimation(AnimationDirection.Out);
            }
        }

        
    }
}
