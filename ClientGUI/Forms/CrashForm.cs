﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace BeefChat.ClientGUI.Forms
{
	public partial class CrashForm : Form
	{
		private Exception Exception;

		public CrashForm(Exception exception)
		{
			InitializeComponent();
			this.Exception = exception;
			HelpButton.Enabled = (Exception.HelpLink != null);
			ExceptionTextBox.Text = Exception.ToString();
		}

		private void CopyButton_Click(object sender, EventArgs e) => Clipboard.SetText(ExceptionTextBox.Text);

		private void RestartButton_Click(object sender, EventArgs e) => Application.Restart();

		private void HelpButton_Click(object sender, EventArgs e) => Process.Start(Exception.HelpLink);
	}
}