﻿namespace BeefChat.ClientGUI.Forms
{
    partial class NotificationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonsPanel = new System.Windows.Forms.Panel();
            this.CloseButton = new System.Windows.Forms.Button();
            this.ReplyPanel = new System.Windows.Forms.Panel();
            this.SendReplyButton = new System.Windows.Forms.Button();
            this.ReplyTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.ReplyButton = new MaterialSkin.Controls.MaterialButton();
            this.ButtonsPanel.SuspendLayout();
            this.ReplyPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ButtonsPanel
            // 
            this.ButtonsPanel.BackColor = System.Drawing.Color.White;
            this.ButtonsPanel.Controls.Add(this.ReplyButton);
            this.ButtonsPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ButtonsPanel.Location = new System.Drawing.Point(0, 102);
            this.ButtonsPanel.Name = "ButtonsPanel";
            this.ButtonsPanel.Size = new System.Drawing.Size(529, 49);
            this.ButtonsPanel.TabIndex = 1;
            this.ButtonsPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.ButtonsPanel_Paint);
            // 
            // CloseButton
            // 
            this.CloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseButton.BackColor = System.Drawing.Color.Transparent;
            this.CloseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CloseButton.FlatAppearance.BorderSize = 0;
            this.CloseButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.CloseButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.CloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseButton.Location = new System.Drawing.Point(500, 13);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(16, 16);
            this.CloseButton.TabIndex = 2;
            this.CloseButton.UseVisualStyleBackColor = false;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // ReplyPanel
            // 
            this.ReplyPanel.Controls.Add(this.ReplyTextBox);
            this.ReplyPanel.Controls.Add(this.SendReplyButton);
            this.ReplyPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ReplyPanel.Location = new System.Drawing.Point(0, 53);
            this.ReplyPanel.Name = "ReplyPanel";
            this.ReplyPanel.Padding = new System.Windows.Forms.Padding(13);
            this.ReplyPanel.Size = new System.Drawing.Size(529, 49);
            this.ReplyPanel.TabIndex = 3;
            this.ReplyPanel.Visible = false;
            this.ReplyPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.ReplyPanel_Paint);
            // 
            // SendReplyButton
            // 
            this.SendReplyButton.BackColor = System.Drawing.Color.Transparent;
            this.SendReplyButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.SendReplyButton.Enabled = false;
            this.SendReplyButton.FlatAppearance.BorderSize = 0;
            this.SendReplyButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.SendReplyButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.SendReplyButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SendReplyButton.Location = new System.Drawing.Point(493, 13);
            this.SendReplyButton.Name = "SendReplyButton";
            this.SendReplyButton.Size = new System.Drawing.Size(23, 23);
            this.SendReplyButton.TabIndex = 4;
            this.SendReplyButton.UseVisualStyleBackColor = false;
            this.SendReplyButton.Click += new System.EventHandler(this.SendReplyButton_Click);
            // 
            // ReplyTextBox
            // 
            this.ReplyTextBox.Depth = 0;
            this.ReplyTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReplyTextBox.Hint = "Reply";
            this.ReplyTextBox.Location = new System.Drawing.Point(13, 13);
            this.ReplyTextBox.MaxLength = 32767;
            this.ReplyTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.ReplyTextBox.Name = "ReplyTextBox";
            this.ReplyTextBox.PasswordChar = '\0';
            this.ReplyTextBox.SelectedText = "";
            this.ReplyTextBox.SelectionLength = 0;
            this.ReplyTextBox.SelectionStart = 0;
            this.ReplyTextBox.Size = new System.Drawing.Size(480, 23);
            this.ReplyTextBox.TabIndex = 3;
            this.ReplyTextBox.TabStop = false;
            this.ReplyTextBox.UseSystemPasswordChar = false;
            this.ReplyTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReplyTextBox_KeyDown);
            this.ReplyTextBox.TextChanged += new System.EventHandler(this.ReplyTextBox_TextChanged);
            // 
            // ReplyButton
            // 
            this.ReplyButton.AutoSize = true;
            this.ReplyButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ReplyButton.BackColor = System.Drawing.Color.LightGray;
            this.ReplyButton.Depth = 0;
            this.ReplyButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.ReplyButton.Icon = null;
            this.ReplyButton.Location = new System.Drawing.Point(0, 0);
            this.ReplyButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.ReplyButton.Name = "ReplyButton";
            this.ReplyButton.Size = new System.Drawing.Size(96, 49);
            this.ReplyButton.TabIndex = 0;
            this.ReplyButton.Text = "Reply";
            this.ReplyButton.Type = MaterialSkin.Controls.ButtonType.Text;
            this.ReplyButton.UseVisualStyleBackColor = false;
            this.ReplyButton.Click += new System.EventHandler(this.ReplyButton_Click);
            // 
            // NotificationForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CancelButton = this.CloseButton;
            this.ClientSize = new System.Drawing.Size(529, 151);
            this.ControlBox = false;
            this.Controls.Add(this.ReplyPanel);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.ButtonsPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NotificationForm";
            this.Opacity = 0D;
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Notification";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NotificationForm_FormClosing);
            this.Load += new System.EventHandler(this.NotificationForm_Load);
            this.Shown += new System.EventHandler(this.NotificationForm_Shown);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.NotificationForm_Paint);
            this.ButtonsPanel.ResumeLayout(false);
            this.ButtonsPanel.PerformLayout();
            this.ReplyPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel ButtonsPanel;
        private MaterialSkin.Controls.MaterialButton ReplyButton;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Panel ReplyPanel;
        private MaterialSkin.Controls.MaterialSingleLineTextField ReplyTextBox;
        private System.Windows.Forms.Button SendReplyButton;
    }
}