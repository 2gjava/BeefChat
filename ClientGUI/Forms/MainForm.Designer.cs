﻿namespace BeefChat.ClientGUI.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.DeveloperTabPage = new System.Windows.Forms.TabPage();
			this.NotificationTestButton = new MaterialSkin.Controls.MaterialButton();
			this.TestCrachDialogButton = new MaterialSkin.Controls.MaterialButton();
			this.ClearUserCacheButton = new MaterialSkin.Controls.MaterialButton();
			this.LogListBox = new BeefChat.ClientGUI.Controls.LogListBox();
			this.userPanel1 = new BeefChat.ClientGUI.Controls.UserPanel();
			this.SettingsTabPage = new System.Windows.Forms.TabPage();
			this.SettingsPropertyGrid = new System.Windows.Forms.PropertyGrid();
			this.ChannelsTabPage = new System.Windows.Forms.TabPage();
			this.FriendsTabPage = new System.Windows.Forms.TabPage();
			this.AddFriendButton = new MaterialSkin.Controls.MaterialFloatingButton();
			this.FriendsPanel = new System.Windows.Forms.Panel();
			this.UserTabPage = new System.Windows.Forms.TabPage();
			this.ChangePasswordButton = new MaterialSkin.Controls.MaterialButton();
			this.DeleteAccountButton = new MaterialSkin.Controls.MaterialButton();
			this.ManageAccountDivider = new BeefChat.ClientGUI.Controls.Divider();
			this.PictureListItem = new BeefChat.ClientGUI.Controls.ListItem();
			this.ColorListItem = new BeefChat.ClientGUI.Controls.ListItem();
			this.PersonalizationDivider = new BeefChat.ClientGUI.Controls.Divider();
			this.StatusListItem = new BeefChat.ClientGUI.Controls.ListItem();
			this.NicknameListItem = new BeefChat.ClientGUI.Controls.ListItem();
			this.InformationDivider = new BeefChat.ClientGUI.Controls.Divider();
			this.TabControl = new MaterialSkin.Controls.MaterialTabControl();
			this.SideBar = new BeefChat.ClientGUI.Controls.SideBar();
			this.ToolTip = new MaterialSkin.Controls.MaterialToolTip(this.components);
			this.DeveloperTabPage.SuspendLayout();
			this.SettingsTabPage.SuspendLayout();
			this.FriendsTabPage.SuspendLayout();
			this.UserTabPage.SuspendLayout();
			this.TabControl.SuspendLayout();
			this.SuspendLayout();
			// 
			// DeveloperTabPage
			// 
			this.DeveloperTabPage.Controls.Add(this.NotificationTestButton);
			this.DeveloperTabPage.Controls.Add(this.TestCrachDialogButton);
			this.DeveloperTabPage.Controls.Add(this.ClearUserCacheButton);
			this.DeveloperTabPage.Controls.Add(this.LogListBox);
			this.DeveloperTabPage.Controls.Add(this.userPanel1);
			this.DeveloperTabPage.Location = new System.Drawing.Point(4, 22);
			this.DeveloperTabPage.Name = "DeveloperTabPage";
			this.DeveloperTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.DeveloperTabPage.Size = new System.Drawing.Size(543, 410);
			this.DeveloperTabPage.TabIndex = 4;
			this.DeveloperTabPage.Text = "Developer";
			this.DeveloperTabPage.UseVisualStyleBackColor = true;
			// 
			// NotificationTestButton
			// 
			this.NotificationTestButton.AutoSize = true;
			this.NotificationTestButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.NotificationTestButton.Depth = 0;
			this.NotificationTestButton.Icon = null;
			this.NotificationTestButton.Location = new System.Drawing.Point(351, 6);
			this.NotificationTestButton.MouseState = MaterialSkin.MouseState.HOVER;
			this.NotificationTestButton.Name = "NotificationTestButton";
			this.NotificationTestButton.Size = new System.Drawing.Size(165, 36);
			this.NotificationTestButton.TabIndex = 14;
			this.NotificationTestButton.Text = "Test Notification";
			this.NotificationTestButton.Type = MaterialSkin.Controls.ButtonType.Text;
			this.NotificationTestButton.UseVisualStyleBackColor = true;
			this.NotificationTestButton.Click += new System.EventHandler(this.NotificationTestButton_Click);
			// 
			// TestCrachDialogButton
			// 
			this.TestCrachDialogButton.AutoSize = true;
			this.TestCrachDialogButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.TestCrachDialogButton.Depth = 0;
			this.TestCrachDialogButton.Icon = null;
			this.TestCrachDialogButton.Location = new System.Drawing.Point(176, 6);
			this.TestCrachDialogButton.MouseState = MaterialSkin.MouseState.HOVER;
			this.TestCrachDialogButton.Name = "TestCrachDialogButton";
			this.TestCrachDialogButton.Size = new System.Drawing.Size(169, 36);
			this.TestCrachDialogButton.TabIndex = 13;
			this.TestCrachDialogButton.Text = "Test Crash Dialog";
			this.TestCrachDialogButton.Type = MaterialSkin.Controls.ButtonType.Text;
			this.TestCrachDialogButton.UseVisualStyleBackColor = true;
			this.TestCrachDialogButton.Click += new System.EventHandler(this.TestCrashDialogButton_Click);
			// 
			// ClearUserCacheButton
			// 
			this.ClearUserCacheButton.AutoSize = true;
			this.ClearUserCacheButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.ClearUserCacheButton.Depth = 0;
			this.ClearUserCacheButton.Icon = null;
			this.ClearUserCacheButton.Location = new System.Drawing.Point(6, 6);
			this.ClearUserCacheButton.MouseState = MaterialSkin.MouseState.HOVER;
			this.ClearUserCacheButton.Name = "ClearUserCacheButton";
			this.ClearUserCacheButton.Size = new System.Drawing.Size(164, 36);
			this.ClearUserCacheButton.TabIndex = 12;
			this.ClearUserCacheButton.Text = "Clear User Cache";
			this.ClearUserCacheButton.Type = MaterialSkin.Controls.ButtonType.Text;
			this.ClearUserCacheButton.UseVisualStyleBackColor = true;
			// 
			// LogListBox
			// 
			this.LogListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.LogListBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
			this.LogListBox.FormattingEnabled = true;
			this.LogListBox.ItemHeight = 18;
			this.LogListBox.Location = new System.Drawing.Point(6, 48);
			this.LogListBox.Name = "LogListBox";
			this.LogListBox.Size = new System.Drawing.Size(531, 346);
			this.LogListBox.TabIndex = 8;
			// 
			// userPanel1
			// 
			this.userPanel1.InnerPadding = 5;
			this.userPanel1.Location = new System.Drawing.Point(388, 130);
			this.userPanel1.Name = "userPanel1";
			this.userPanel1.Size = new System.Drawing.Size(8, 8);
			this.userPanel1.TabIndex = 7;
			this.userPanel1.Text = "userPanel1";
			this.userPanel1.User = "";
			// 
			// SettingsTabPage
			// 
			this.SettingsTabPage.Controls.Add(this.SettingsPropertyGrid);
			this.SettingsTabPage.Location = new System.Drawing.Point(4, 22);
			this.SettingsTabPage.Margin = new System.Windows.Forms.Padding(0);
			this.SettingsTabPage.Name = "SettingsTabPage";
			this.SettingsTabPage.Size = new System.Drawing.Size(543, 410);
			this.SettingsTabPage.TabIndex = 3;
			this.SettingsTabPage.Text = "Settings";
			this.SettingsTabPage.UseVisualStyleBackColor = true;
			// 
			// SettingsPropertyGrid
			// 
			this.SettingsPropertyGrid.CommandsVisibleIfAvailable = false;
			this.SettingsPropertyGrid.Cursor = System.Windows.Forms.Cursors.Default;
			this.SettingsPropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.SettingsPropertyGrid.Location = new System.Drawing.Point(0, 0);
			this.SettingsPropertyGrid.Name = "SettingsPropertyGrid";
			this.SettingsPropertyGrid.Size = new System.Drawing.Size(543, 410);
			this.SettingsPropertyGrid.TabIndex = 1;
			this.SettingsPropertyGrid.ToolbarVisible = false;
			// 
			// ChannelsTabPage
			// 
			this.ChannelsTabPage.AutoScroll = true;
			this.ChannelsTabPage.Location = new System.Drawing.Point(4, 22);
			this.ChannelsTabPage.Margin = new System.Windows.Forms.Padding(0);
			this.ChannelsTabPage.Name = "ChannelsTabPage";
			this.ChannelsTabPage.Padding = new System.Windows.Forms.Padding(5);
			this.ChannelsTabPage.Size = new System.Drawing.Size(543, 410);
			this.ChannelsTabPage.TabIndex = 1;
			this.ChannelsTabPage.Text = "Channels";
			this.ChannelsTabPage.UseVisualStyleBackColor = true;
			// 
			// FriendsTabPage
			// 
			this.FriendsTabPage.Controls.Add(this.AddFriendButton);
			this.FriendsTabPage.Controls.Add(this.FriendsPanel);
			this.FriendsTabPage.Location = new System.Drawing.Point(4, 22);
			this.FriendsTabPage.Margin = new System.Windows.Forms.Padding(0);
			this.FriendsTabPage.Name = "FriendsTabPage";
			this.FriendsTabPage.Padding = new System.Windows.Forms.Padding(5);
			this.FriendsTabPage.Size = new System.Drawing.Size(543, 410);
			this.FriendsTabPage.TabIndex = 0;
			this.FriendsTabPage.Text = "Friends";
			this.FriendsTabPage.UseVisualStyleBackColor = true;
			// 
			// AddFriendButton
			// 
			this.AddFriendButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.AddFriendButton.AutoSize = true;
			this.AddFriendButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.AddFriendButton.Depth = 0;
			this.AddFriendButton.Icon = null;
			this.AddFriendButton.Location = new System.Drawing.Point(477, 344);
			this.AddFriendButton.Mini = false;
			this.AddFriendButton.MouseState = MaterialSkin.MouseState.HOVER;
			this.AddFriendButton.Name = "AddFriendButton";
			this.AddFriendButton.Size = new System.Drawing.Size(56, 56);
			this.AddFriendButton.TabIndex = 2;
			this.AddFriendButton.UseVisualStyleBackColor = true;
			this.AddFriendButton.Click += new System.EventHandler(this.AddFriendButton_Click);
			// 
			// FriendsPanel
			// 
			this.FriendsPanel.AutoScroll = true;
			this.FriendsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.FriendsPanel.Location = new System.Drawing.Point(5, 5);
			this.FriendsPanel.Name = "FriendsPanel";
			this.FriendsPanel.Size = new System.Drawing.Size(533, 400);
			this.FriendsPanel.TabIndex = 1;
			this.FriendsPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.FriendsPanel_Paint);
			// 
			// UserTabPage
			// 
			this.UserTabPage.AutoScroll = true;
			this.UserTabPage.Controls.Add(this.ChangePasswordButton);
			this.UserTabPage.Controls.Add(this.DeleteAccountButton);
			this.UserTabPage.Controls.Add(this.ManageAccountDivider);
			this.UserTabPage.Controls.Add(this.PictureListItem);
			this.UserTabPage.Controls.Add(this.ColorListItem);
			this.UserTabPage.Controls.Add(this.PersonalizationDivider);
			this.UserTabPage.Controls.Add(this.StatusListItem);
			this.UserTabPage.Controls.Add(this.NicknameListItem);
			this.UserTabPage.Controls.Add(this.InformationDivider);
			this.UserTabPage.Location = new System.Drawing.Point(4, 22);
			this.UserTabPage.Margin = new System.Windows.Forms.Padding(0);
			this.UserTabPage.Name = "UserTabPage";
			this.UserTabPage.Padding = new System.Windows.Forms.Padding(5);
			this.UserTabPage.Size = new System.Drawing.Size(543, 410);
			this.UserTabPage.TabIndex = 2;
			this.UserTabPage.Text = "User Profile";
			this.UserTabPage.UseVisualStyleBackColor = true;
			// 
			// ChangePasswordButton
			// 
			this.ChangePasswordButton.AutoSize = true;
			this.ChangePasswordButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.ChangePasswordButton.Depth = 0;
			this.ChangePasswordButton.Dock = System.Windows.Forms.DockStyle.Top;
			this.ChangePasswordButton.Enabled = false;
			this.ChangePasswordButton.Icon = null;
			this.ChangePasswordButton.Location = new System.Drawing.Point(5, 353);
			this.ChangePasswordButton.MouseState = MaterialSkin.MouseState.HOVER;
			this.ChangePasswordButton.Name = "ChangePasswordButton";
			this.ChangePasswordButton.Size = new System.Drawing.Size(533, 36);
			this.ChangePasswordButton.TabIndex = 31;
			this.ChangePasswordButton.Text = "Change Password";
			this.ChangePasswordButton.Type = MaterialSkin.Controls.ButtonType.Outlined;
			this.ChangePasswordButton.UseVisualStyleBackColor = true;
			// 
			// DeleteAccountButton
			// 
			this.DeleteAccountButton.AutoSize = true;
			this.DeleteAccountButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.DeleteAccountButton.Depth = 0;
			this.DeleteAccountButton.Dock = System.Windows.Forms.DockStyle.Top;
			this.DeleteAccountButton.Icon = null;
			this.DeleteAccountButton.Location = new System.Drawing.Point(5, 317);
			this.DeleteAccountButton.MouseState = MaterialSkin.MouseState.HOVER;
			this.DeleteAccountButton.Name = "DeleteAccountButton";
			this.DeleteAccountButton.Size = new System.Drawing.Size(533, 36);
			this.DeleteAccountButton.TabIndex = 30;
			this.DeleteAccountButton.Text = "Delete Account";
			this.DeleteAccountButton.Type = MaterialSkin.Controls.ButtonType.Outlined;
			this.DeleteAccountButton.UseVisualStyleBackColor = true;
			this.DeleteAccountButton.Click += new System.EventHandler(this.DeleteAccountButton_Click);
			// 
			// ManageAccountDivider
			// 
			this.ManageAccountDivider.BackColor = System.Drawing.Color.Transparent;
			this.ManageAccountDivider.DividerHeight = 1;
			this.ManageAccountDivider.Dock = System.Windows.Forms.DockStyle.Top;
			this.ManageAccountDivider.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ManageAccountDivider.Location = new System.Drawing.Point(5, 293);
			this.ManageAccountDivider.Name = "ManageAccountDivider";
			this.ManageAccountDivider.Padding = new System.Windows.Forms.Padding(16, 0, 32, 0);
			this.ManageAccountDivider.Size = new System.Drawing.Size(533, 24);
			this.ManageAccountDivider.TabIndex = 27;
			this.ManageAccountDivider.Text = "Manage Account";
			this.ManageAccountDivider.TextSpacing = 6;
			// 
			// PictureListItem
			// 
			this.PictureListItem.BackColor = System.Drawing.Color.Transparent;
			this.PictureListItem.Dock = System.Windows.Forms.DockStyle.Top;
			this.PictureListItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PictureListItem.Icon = null;
			this.PictureListItem.Location = new System.Drawing.Point(5, 245);
			this.PictureListItem.Name = "PictureListItem";
			this.PictureListItem.PrimaryFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PictureListItem.PrimaryText = "Profile Picture";
			this.PictureListItem.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PictureListItem.SecondaryText = "";
			this.PictureListItem.Size = new System.Drawing.Size(533, 48);
			this.PictureListItem.TabIndex = 24;
			this.PictureListItem.Text = "listItem4";
			this.PictureListItem.Click += new System.EventHandler(this.PictureListItem_Click);
			// 
			// ColorListItem
			// 
			this.ColorListItem.BackColor = System.Drawing.Color.Transparent;
			this.ColorListItem.Dock = System.Windows.Forms.DockStyle.Top;
			this.ColorListItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ColorListItem.Icon = null;
			this.ColorListItem.Location = new System.Drawing.Point(5, 181);
			this.ColorListItem.Name = "ColorListItem";
			this.ColorListItem.PrimaryFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ColorListItem.PrimaryText = "Profile Color";
			this.ColorListItem.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ColorListItem.SecondaryText = "(Insert Hex Color Here)";
			this.ColorListItem.Size = new System.Drawing.Size(533, 64);
			this.ColorListItem.TabIndex = 23;
			this.ColorListItem.Text = "listItem3";
			this.ColorListItem.Click += new System.EventHandler(this.ColorListItem_Click);
			// 
			// PersonalizationDivider
			// 
			this.PersonalizationDivider.BackColor = System.Drawing.Color.Transparent;
			this.PersonalizationDivider.DividerHeight = 1;
			this.PersonalizationDivider.Dock = System.Windows.Forms.DockStyle.Top;
			this.PersonalizationDivider.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PersonalizationDivider.Location = new System.Drawing.Point(5, 157);
			this.PersonalizationDivider.Name = "PersonalizationDivider";
			this.PersonalizationDivider.Padding = new System.Windows.Forms.Padding(16, 0, 32, 0);
			this.PersonalizationDivider.Size = new System.Drawing.Size(533, 24);
			this.PersonalizationDivider.TabIndex = 26;
			this.PersonalizationDivider.Text = "Personalization";
			this.PersonalizationDivider.TextSpacing = 6;
			// 
			// StatusListItem
			// 
			this.StatusListItem.BackColor = System.Drawing.Color.Transparent;
			this.StatusListItem.Dock = System.Windows.Forms.DockStyle.Top;
			this.StatusListItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.StatusListItem.Icon = null;
			this.StatusListItem.Location = new System.Drawing.Point(5, 93);
			this.StatusListItem.Name = "StatusListItem";
			this.StatusListItem.PrimaryFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.StatusListItem.PrimaryText = "Status";
			this.StatusListItem.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.StatusListItem.SecondaryText = "(Insert Status Here)";
			this.StatusListItem.Size = new System.Drawing.Size(533, 64);
			this.StatusListItem.TabIndex = 22;
			this.StatusListItem.Text = "listItem2";
			this.StatusListItem.Click += new System.EventHandler(this.StatusListItem_Click);
			// 
			// NicknameListItem
			// 
			this.NicknameListItem.BackColor = System.Drawing.Color.Transparent;
			this.NicknameListItem.Dock = System.Windows.Forms.DockStyle.Top;
			this.NicknameListItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.NicknameListItem.Icon = null;
			this.NicknameListItem.Location = new System.Drawing.Point(5, 29);
			this.NicknameListItem.Name = "NicknameListItem";
			this.NicknameListItem.PrimaryFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.NicknameListItem.PrimaryText = "Nickname";
			this.NicknameListItem.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.NicknameListItem.SecondaryText = "(Insert Nickname Here)";
			this.NicknameListItem.Size = new System.Drawing.Size(533, 64);
			this.NicknameListItem.TabIndex = 21;
			this.NicknameListItem.Click += new System.EventHandler(this.NicknameListItem_Click);
			// 
			// InformationDivider
			// 
			this.InformationDivider.BackColor = System.Drawing.Color.Transparent;
			this.InformationDivider.DividerHeight = 0;
			this.InformationDivider.Dock = System.Windows.Forms.DockStyle.Top;
			this.InformationDivider.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.InformationDivider.Location = new System.Drawing.Point(5, 5);
			this.InformationDivider.Name = "InformationDivider";
			this.InformationDivider.Padding = new System.Windows.Forms.Padding(16, 0, 32, 0);
			this.InformationDivider.Size = new System.Drawing.Size(533, 24);
			this.InformationDivider.TabIndex = 25;
			this.InformationDivider.Text = "Basic Information";
			this.InformationDivider.TextSpacing = 5;
			// 
			// TabControl
			// 
			this.TabControl.Controls.Add(this.UserTabPage);
			this.TabControl.Controls.Add(this.FriendsTabPage);
			this.TabControl.Controls.Add(this.ChannelsTabPage);
			this.TabControl.Controls.Add(this.DeveloperTabPage);
			this.TabControl.Controls.Add(this.SettingsTabPage);
			this.TabControl.Depth = 0;
			this.TabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TabControl.Location = new System.Drawing.Point(40, 68);
			this.TabControl.MouseState = MaterialSkin.MouseState.HOVER;
			this.TabControl.Name = "TabControl";
			this.TabControl.SelectedIndex = 0;
			this.TabControl.Size = new System.Drawing.Size(551, 436);
			this.TabControl.TabIndex = 1;
			this.TabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
			// 
			// SideBar
			// 
			this.SideBar.Dock = System.Windows.Forms.DockStyle.Left;
			this.SideBar.Location = new System.Drawing.Point(4, 68);
			this.SideBar.MinimumSize = new System.Drawing.Size(36, 164);
			this.SideBar.Name = "SideBar";
			this.SideBar.Padding = new System.Windows.Forms.Padding(2);
			this.SideBar.Size = new System.Drawing.Size(36, 436);
			this.SideBar.TabIndex = 2;
			// 
			// ToolTip
			// 
			this.ToolTip.OwnerDraw = true;
			this.ToolTip.UseFading = false;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(595, 508);
			this.Controls.Add(this.TabControl);
			this.Controls.Add(this.SideBar);
			this.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.KeyPreview = true;
			this.MinimumSize = new System.Drawing.Size(595, 395);
			this.Name = "MainForm";
			this.Padding = new System.Windows.Forms.Padding(4, 68, 4, 4);
			this.Text = "BeefChat";
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
			this.DeveloperTabPage.ResumeLayout(false);
			this.DeveloperTabPage.PerformLayout();
			this.SettingsTabPage.ResumeLayout(false);
			this.FriendsTabPage.ResumeLayout(false);
			this.FriendsTabPage.PerformLayout();
			this.UserTabPage.ResumeLayout(false);
			this.UserTabPage.PerformLayout();
			this.TabControl.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabPage DeveloperTabPage;
        private System.Windows.Forms.TabPage SettingsTabPage;
        private System.Windows.Forms.PropertyGrid SettingsPropertyGrid;
        private System.Windows.Forms.TabPage ChannelsTabPage;
        private System.Windows.Forms.TabPage FriendsTabPage;
        private System.Windows.Forms.TabPage UserTabPage;
        private MaterialSkin.Controls.MaterialTabControl TabControl;
        private System.Windows.Forms.Panel FriendsPanel;
        private Controls.ListItem NicknameListItem;
        private Controls.ListItem PictureListItem;
        private Controls.ListItem ColorListItem;
        private Controls.Divider PersonalizationDivider;
        private Controls.ListItem StatusListItem;
        private Controls.Divider InformationDivider;
		private Controls.Divider ManageAccountDivider;
		private Controls.LogListBox LogListBox;
		private Controls.UserPanel userPanel1;
        private MaterialSkin.Controls.MaterialFloatingButton AddFriendButton;
        private MaterialSkin.Controls.MaterialButton DeleteAccountButton;
        private MaterialSkin.Controls.MaterialButton NotificationTestButton;
        private MaterialSkin.Controls.MaterialButton TestCrachDialogButton;
        private MaterialSkin.Controls.MaterialButton ClearUserCacheButton;
        private Controls.SideBar SideBar;
        private MaterialSkin.Controls.MaterialButton ChangePasswordButton;
		private MaterialSkin.Controls.MaterialToolTip ToolTip;
	}
}