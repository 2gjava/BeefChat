﻿namespace BeefChat.ClientGUI.Forms
{
    partial class ColorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.RedButton = new BeefChat.ClientGUI.Controls.RoundButton();
			this.PinkButton = new BeefChat.ClientGUI.Controls.RoundButton();
			this.DeepPurpleButton = new BeefChat.ClientGUI.Controls.RoundButton();
			this.PurpleButton = new BeefChat.ClientGUI.Controls.RoundButton();
			this.LightBlueButton = new BeefChat.ClientGUI.Controls.RoundButton();
			this.BlueButton = new BeefChat.ClientGUI.Controls.RoundButton();
			this.IndigoButton = new BeefChat.ClientGUI.Controls.RoundButton();
			this.OrangeButton = new BeefChat.ClientGUI.Controls.RoundButton();
			this.AmberButton = new BeefChat.ClientGUI.Controls.RoundButton();
			this.YellowButton = new BeefChat.ClientGUI.Controls.RoundButton();
			this.LimeButton = new BeefChat.ClientGUI.Controls.RoundButton();
			this.LightGreenButton = new BeefChat.ClientGUI.Controls.RoundButton();
			this.GreenButton = new BeefChat.ClientGUI.Controls.RoundButton();
			this.TealButton = new BeefChat.ClientGUI.Controls.RoundButton();
			this.ButtonGray = new BeefChat.ClientGUI.Controls.RoundButton();
			this.ButtonBrown = new BeefChat.ClientGUI.Controls.RoundButton();
			this.DeepOrangeButton = new BeefChat.ClientGUI.Controls.RoundButton();
			this.ButtonWhite = new BeefChat.ClientGUI.Controls.RoundButton();
			this.ButtonBlueGray = new BeefChat.ClientGUI.Controls.RoundButton();
			this.BlackButton = new BeefChat.ClientGUI.Controls.RoundButton();
			this.CustomColorButton = new BeefChat.ClientGUI.Controls.RoundButton();
			this.btn_ok = new MaterialSkin.Controls.MaterialFlatButton();
			this.btn_cancel = new MaterialSkin.Controls.MaterialFlatButton();
			this.SuspendLayout();
			// 
			// RedButton
			// 
			this.RedButton.BackColor = System.Drawing.Color.Transparent;
			this.RedButton.CustomColor = true;
			this.RedButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
			this.RedButton.Icon = null;
			this.RedButton.Location = new System.Drawing.Point(12, 82);
			this.RedButton.Name = "RedButton";
			this.RedButton.Raised = false;
			this.RedButton.Size = new System.Drawing.Size(40, 40);
			this.RedButton.TabIndex = 0;
			this.RedButton.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// PinkButton
			// 
			this.PinkButton.BackColor = System.Drawing.Color.Transparent;
			this.PinkButton.CustomColor = true;
			this.PinkButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(30)))), ((int)(((byte)(99)))));
			this.PinkButton.Icon = null;
			this.PinkButton.Location = new System.Drawing.Point(58, 82);
			this.PinkButton.Name = "PinkButton";
			this.PinkButton.Raised = false;
			this.PinkButton.Size = new System.Drawing.Size(40, 40);
			this.PinkButton.TabIndex = 1;
			this.PinkButton.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// DeepPurpleButton
			// 
			this.DeepPurpleButton.BackColor = System.Drawing.Color.Transparent;
			this.DeepPurpleButton.CustomColor = true;
			this.DeepPurpleButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(58)))), ((int)(((byte)(183)))));
			this.DeepPurpleButton.Icon = null;
			this.DeepPurpleButton.Location = new System.Drawing.Point(150, 82);
			this.DeepPurpleButton.Name = "DeepPurpleButton";
			this.DeepPurpleButton.Raised = false;
			this.DeepPurpleButton.Size = new System.Drawing.Size(40, 40);
			this.DeepPurpleButton.TabIndex = 3;
			this.DeepPurpleButton.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// PurpleButton
			// 
			this.PurpleButton.BackColor = System.Drawing.Color.Transparent;
			this.PurpleButton.CustomColor = true;
			this.PurpleButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(39)))), ((int)(((byte)(176)))));
			this.PurpleButton.Icon = null;
			this.PurpleButton.Location = new System.Drawing.Point(104, 82);
			this.PurpleButton.Name = "PurpleButton";
			this.PurpleButton.Raised = false;
			this.PurpleButton.Size = new System.Drawing.Size(40, 40);
			this.PurpleButton.TabIndex = 2;
			this.PurpleButton.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// LightBlueButton
			// 
			this.LightBlueButton.BackColor = System.Drawing.Color.Transparent;
			this.LightBlueButton.CustomColor = true;
			this.LightBlueButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(169)))), ((int)(((byte)(244)))));
			this.LightBlueButton.Icon = null;
			this.LightBlueButton.Location = new System.Drawing.Point(288, 82);
			this.LightBlueButton.Name = "LightBlueButton";
			this.LightBlueButton.Raised = false;
			this.LightBlueButton.Size = new System.Drawing.Size(40, 40);
			this.LightBlueButton.TabIndex = 6;
			this.LightBlueButton.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// BlueButton
			// 
			this.BlueButton.BackColor = System.Drawing.Color.Transparent;
			this.BlueButton.CustomColor = true;
			this.BlueButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
			this.BlueButton.Icon = null;
			this.BlueButton.Location = new System.Drawing.Point(242, 82);
			this.BlueButton.Name = "BlueButton";
			this.BlueButton.Raised = false;
			this.BlueButton.Size = new System.Drawing.Size(40, 40);
			this.BlueButton.TabIndex = 5;
			this.BlueButton.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// IndigoButton
			// 
			this.IndigoButton.BackColor = System.Drawing.Color.Transparent;
			this.IndigoButton.CustomColor = true;
			this.IndigoButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(81)))), ((int)(((byte)(181)))));
			this.IndigoButton.Icon = null;
			this.IndigoButton.Location = new System.Drawing.Point(196, 82);
			this.IndigoButton.Name = "IndigoButton";
			this.IndigoButton.Raised = false;
			this.IndigoButton.Size = new System.Drawing.Size(40, 40);
			this.IndigoButton.TabIndex = 4;
			this.IndigoButton.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// OrangeButton
			// 
			this.OrangeButton.BackColor = System.Drawing.Color.Transparent;
			this.OrangeButton.CustomColor = true;
			this.OrangeButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(152)))), ((int)(((byte)(0)))));
			this.OrangeButton.Icon = null;
			this.OrangeButton.Location = new System.Drawing.Point(288, 128);
			this.OrangeButton.Name = "OrangeButton";
			this.OrangeButton.Raised = false;
			this.OrangeButton.Size = new System.Drawing.Size(40, 40);
			this.OrangeButton.TabIndex = 13;
			this.OrangeButton.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// AmberButton
			// 
			this.AmberButton.BackColor = System.Drawing.Color.Transparent;
			this.AmberButton.CustomColor = true;
			this.AmberButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(7)))));
			this.AmberButton.Icon = null;
			this.AmberButton.Location = new System.Drawing.Point(242, 128);
			this.AmberButton.Name = "AmberButton";
			this.AmberButton.Raised = false;
			this.AmberButton.Size = new System.Drawing.Size(40, 40);
			this.AmberButton.TabIndex = 12;
			this.AmberButton.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// YellowButton
			// 
			this.YellowButton.BackColor = System.Drawing.Color.Transparent;
			this.YellowButton.CustomColor = true;
			this.YellowButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(59)))));
			this.YellowButton.Icon = null;
			this.YellowButton.Location = new System.Drawing.Point(196, 128);
			this.YellowButton.Name = "YellowButton";
			this.YellowButton.Raised = false;
			this.YellowButton.Size = new System.Drawing.Size(40, 40);
			this.YellowButton.TabIndex = 11;
			this.YellowButton.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// LimeButton
			// 
			this.LimeButton.BackColor = System.Drawing.Color.Transparent;
			this.LimeButton.CustomColor = true;
			this.LimeButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(220)))), ((int)(((byte)(57)))));
			this.LimeButton.Icon = null;
			this.LimeButton.Location = new System.Drawing.Point(150, 128);
			this.LimeButton.Name = "LimeButton";
			this.LimeButton.Raised = false;
			this.LimeButton.Size = new System.Drawing.Size(40, 40);
			this.LimeButton.TabIndex = 10;
			this.LimeButton.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// LightGreenButton
			// 
			this.LightGreenButton.BackColor = System.Drawing.Color.Transparent;
			this.LightGreenButton.CustomColor = true;
			this.LightGreenButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(195)))), ((int)(((byte)(74)))));
			this.LightGreenButton.Icon = null;
			this.LightGreenButton.Location = new System.Drawing.Point(104, 128);
			this.LightGreenButton.Name = "LightGreenButton";
			this.LightGreenButton.Raised = false;
			this.LightGreenButton.Size = new System.Drawing.Size(40, 40);
			this.LightGreenButton.TabIndex = 9;
			this.LightGreenButton.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// GreenButton
			// 
			this.GreenButton.BackColor = System.Drawing.Color.Transparent;
			this.GreenButton.CustomColor = true;
			this.GreenButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(175)))), ((int)(((byte)(80)))));
			this.GreenButton.Icon = null;
			this.GreenButton.Location = new System.Drawing.Point(58, 128);
			this.GreenButton.Name = "GreenButton";
			this.GreenButton.Raised = false;
			this.GreenButton.Size = new System.Drawing.Size(40, 40);
			this.GreenButton.TabIndex = 8;
			this.GreenButton.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// TealButton
			// 
			this.TealButton.BackColor = System.Drawing.Color.Transparent;
			this.TealButton.CustomColor = true;
			this.TealButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(150)))), ((int)(((byte)(136)))));
			this.TealButton.Icon = null;
			this.TealButton.Location = new System.Drawing.Point(12, 128);
			this.TealButton.Name = "TealButton";
			this.TealButton.Raised = false;
			this.TealButton.Size = new System.Drawing.Size(40, 40);
			this.TealButton.TabIndex = 7;
			this.TealButton.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// ButtonGray
			// 
			this.ButtonGray.BackColor = System.Drawing.Color.Transparent;
			this.ButtonGray.CustomColor = true;
			this.ButtonGray.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(158)))), ((int)(((byte)(158)))));
			this.ButtonGray.Icon = null;
			this.ButtonGray.Location = new System.Drawing.Point(150, 174);
			this.ButtonGray.Name = "ButtonGray";
			this.ButtonGray.Raised = false;
			this.ButtonGray.Size = new System.Drawing.Size(40, 40);
			this.ButtonGray.TabIndex = 16;
			this.ButtonGray.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// ButtonBrown
			// 
			this.ButtonBrown.BackColor = System.Drawing.Color.Transparent;
			this.ButtonBrown.CustomColor = true;
			this.ButtonBrown.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(85)))), ((int)(((byte)(72)))));
			this.ButtonBrown.Icon = null;
			this.ButtonBrown.Location = new System.Drawing.Point(58, 174);
			this.ButtonBrown.Name = "ButtonBrown";
			this.ButtonBrown.Raised = false;
			this.ButtonBrown.Size = new System.Drawing.Size(40, 40);
			this.ButtonBrown.TabIndex = 15;
			this.ButtonBrown.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// DeepOrangeButton
			// 
			this.DeepOrangeButton.BackColor = System.Drawing.Color.Transparent;
			this.DeepOrangeButton.CustomColor = true;
			this.DeepOrangeButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(87)))), ((int)(((byte)(34)))));
			this.DeepOrangeButton.Icon = null;
			this.DeepOrangeButton.Location = new System.Drawing.Point(12, 174);
			this.DeepOrangeButton.Name = "DeepOrangeButton";
			this.DeepOrangeButton.Raised = false;
			this.DeepOrangeButton.Size = new System.Drawing.Size(40, 40);
			this.DeepOrangeButton.TabIndex = 14;
			this.DeepOrangeButton.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// ButtonWhite
			// 
			this.ButtonWhite.BackColor = System.Drawing.Color.Transparent;
			this.ButtonWhite.CustomColor = true;
			this.ButtonWhite.ForeColor = System.Drawing.Color.WhiteSmoke;
			this.ButtonWhite.Icon = null;
			this.ButtonWhite.Location = new System.Drawing.Point(196, 174);
			this.ButtonWhite.Name = "ButtonWhite";
			this.ButtonWhite.Raised = false;
			this.ButtonWhite.Size = new System.Drawing.Size(40, 40);
			this.ButtonWhite.TabIndex = 18;
			this.ButtonWhite.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// ButtonBlueGray
			// 
			this.ButtonBlueGray.BackColor = System.Drawing.Color.Transparent;
			this.ButtonBlueGray.CustomColor = true;
			this.ButtonBlueGray.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(125)))), ((int)(((byte)(139)))));
			this.ButtonBlueGray.Icon = null;
			this.ButtonBlueGray.Location = new System.Drawing.Point(104, 174);
			this.ButtonBlueGray.Name = "ButtonBlueGray";
			this.ButtonBlueGray.Raised = false;
			this.ButtonBlueGray.Size = new System.Drawing.Size(40, 40);
			this.ButtonBlueGray.TabIndex = 17;
			this.ButtonBlueGray.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// BlackButton
			// 
			this.BlackButton.BackColor = System.Drawing.Color.Transparent;
			this.BlackButton.CustomColor = true;
			this.BlackButton.ForeColor = System.Drawing.Color.Black;
			this.BlackButton.Icon = null;
			this.BlackButton.Location = new System.Drawing.Point(242, 174);
			this.BlackButton.Name = "BlackButton";
			this.BlackButton.Raised = false;
			this.BlackButton.Size = new System.Drawing.Size(40, 40);
			this.BlackButton.TabIndex = 19;
			this.BlackButton.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// CustomColorButton
			// 
			this.CustomColorButton.BackColor = System.Drawing.Color.Transparent;
			this.CustomColorButton.CustomColor = true;
			this.CustomColorButton.ForeColor = System.Drawing.Color.Transparent;
			this.CustomColorButton.Icon = null;
			this.CustomColorButton.Location = new System.Drawing.Point(288, 174);
			this.CustomColorButton.Name = "CustomColorButton";
			this.CustomColorButton.Raised = false;
			this.CustomColorButton.Size = new System.Drawing.Size(40, 40);
			this.CustomColorButton.TabIndex = 20;
			this.CustomColorButton.Click += new System.EventHandler(this.btn_color_Click);
			// 
			// btn_ok
			// 
			this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_ok.AutoSize = true;
			this.btn_ok.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.btn_ok.Depth = 0;
			this.btn_ok.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btn_ok.Icon = null;
			this.btn_ok.Location = new System.Drawing.Point(292, 227);
			this.btn_ok.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
			this.btn_ok.MouseState = MaterialSkin.MouseState.HOVER;
			this.btn_ok.Name = "btn_ok";
			this.btn_ok.Primary = true;
			this.btn_ok.Size = new System.Drawing.Size(39, 36);
			this.btn_ok.TabIndex = 21;
			this.btn_ok.Text = "Ok";
			this.btn_ok.UseVisualStyleBackColor = true;
			this.btn_ok.Click += new System.EventHandler(this.btn_close_Click);
			// 
			// btn_cancel
			// 
			this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_cancel.AutoSize = true;
			this.btn_cancel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.btn_cancel.Depth = 0;
			this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btn_cancel.Icon = null;
			this.btn_cancel.Location = new System.Drawing.Point(211, 227);
			this.btn_cancel.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
			this.btn_cancel.MouseState = MaterialSkin.MouseState.HOVER;
			this.btn_cancel.Name = "btn_cancel";
			this.btn_cancel.Primary = false;
			this.btn_cancel.Size = new System.Drawing.Size(73, 36);
			this.btn_cancel.TabIndex = 22;
			this.btn_cancel.Text = "Cancel";
			this.btn_cancel.UseVisualStyleBackColor = true;
			this.btn_cancel.Click += new System.EventHandler(this.btn_close_Click);
			// 
			// ColorForm
			// 
			this.AcceptButton = this.btn_ok;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btn_cancel;
			this.ClientSize = new System.Drawing.Size(344, 278);
			this.ControlBox = false;
			this.Controls.Add(this.btn_cancel);
			this.Controls.Add(this.btn_ok);
			this.Controls.Add(this.CustomColorButton);
			this.Controls.Add(this.BlackButton);
			this.Controls.Add(this.ButtonWhite);
			this.Controls.Add(this.ButtonBlueGray);
			this.Controls.Add(this.ButtonGray);
			this.Controls.Add(this.ButtonBrown);
			this.Controls.Add(this.DeepOrangeButton);
			this.Controls.Add(this.OrangeButton);
			this.Controls.Add(this.AmberButton);
			this.Controls.Add(this.YellowButton);
			this.Controls.Add(this.LimeButton);
			this.Controls.Add(this.LightGreenButton);
			this.Controls.Add(this.GreenButton);
			this.Controls.Add(this.TealButton);
			this.Controls.Add(this.LightBlueButton);
			this.Controls.Add(this.BlueButton);
			this.Controls.Add(this.IndigoButton);
			this.Controls.Add(this.DeepPurpleButton);
			this.Controls.Add(this.PurpleButton);
			this.Controls.Add(this.PinkButton);
			this.Controls.Add(this.RedButton);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ColorForm";
			this.Sizable = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Choose a color";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private Controls.RoundButton RedButton;
        private Controls.RoundButton PinkButton;
        private Controls.RoundButton DeepPurpleButton;
        private Controls.RoundButton PurpleButton;
        private Controls.RoundButton LightBlueButton;
        private Controls.RoundButton BlueButton;
        private Controls.RoundButton IndigoButton;
        private Controls.RoundButton OrangeButton;
        private Controls.RoundButton AmberButton;
        private Controls.RoundButton YellowButton;
        private Controls.RoundButton LimeButton;
        private Controls.RoundButton LightGreenButton;
        private Controls.RoundButton GreenButton;
        private Controls.RoundButton TealButton;
        private Controls.RoundButton ButtonGray;
        private Controls.RoundButton ButtonBrown;
        private Controls.RoundButton DeepOrangeButton;
        private Controls.RoundButton ButtonWhite;
        private Controls.RoundButton ButtonBlueGray;
        private Controls.RoundButton BlackButton;
        private Controls.RoundButton CustomColorButton;
        private MaterialSkin.Controls.MaterialFlatButton btn_ok;
        private MaterialSkin.Controls.MaterialFlatButton btn_cancel;
    }
}