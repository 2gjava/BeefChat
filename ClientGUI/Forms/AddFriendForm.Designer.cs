﻿namespace BeefChat.ClientGUI.Forms
{
    partial class AddFriendForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UsernameTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.AddButton = new MaterialSkin.Controls.MaterialFlatButton();
            this.CancelButton = new MaterialSkin.Controls.MaterialFlatButton();
            this.userPanel = new BeefChat.ClientGUI.Controls.UserPanel();
            this.SuspendLayout();
            // 
            // UsernameTextBox
            // 
            this.UsernameTextBox.Depth = 0;
            this.UsernameTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.UsernameTextBox.Hint = "Username";
            this.UsernameTextBox.Location = new System.Drawing.Point(15, 80);
            this.UsernameTextBox.MaxLength = 32767;
            this.UsernameTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.UsernameTextBox.Name = "UsernameTextBox";
            this.UsernameTextBox.PasswordChar = '\0';
            this.UsernameTextBox.SelectedText = "";
            this.UsernameTextBox.SelectionLength = 0;
            this.UsernameTextBox.SelectionStart = 0;
            this.UsernameTextBox.Size = new System.Drawing.Size(433, 23);
            this.UsernameTextBox.TabIndex = 0;
            this.UsernameTextBox.TabStop = false;
            this.UsernameTextBox.UseSystemPasswordChar = false;
            this.UsernameTextBox.TextChanged += new System.EventHandler(this.UsernameTextBox_TextChanged);
            // 
            // AddButton
            // 
            this.AddButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.AddButton.AutoSize = true;
            this.AddButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.AddButton.Depth = 0;
            this.AddButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.AddButton.Icon = null;
            this.AddButton.Location = new System.Drawing.Point(350, 172);
            this.AddButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.AddButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.AddButton.Name = "AddButton";
            this.AddButton.Primary = true;
            this.AddButton.Size = new System.Drawing.Size(98, 36);
            this.AddButton.TabIndex = 1;
            this.AddButton.Text = "Add Friend";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.AutoSize = true;
            this.CancelButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CancelButton.Depth = 0;
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Icon = null;
            this.CancelButton.Location = new System.Drawing.Point(269, 172);
            this.CancelButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.CancelButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Primary = false;
            this.CancelButton.Size = new System.Drawing.Size(73, 36);
            this.CancelButton.TabIndex = 2;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // userPanel
            // 
            this.userPanel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userPanel.Location = new System.Drawing.Point(15, 109);
            this.userPanel.Name = "userPanel";
            this.userPanel.Size = new System.Drawing.Size(433, 48);
            this.userPanel.TabIndex = 7;
            this.userPanel.Text = "userPanel1";
            this.userPanel.User = null;
            // 
            // FrmAddFriend
            // 
            this.AcceptButton = this.AddButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 225);
            this.Controls.Add(this.userPanel);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.UsernameTextBox);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAddFriend";
            this.Padding = new System.Windows.Forms.Padding(15, 80, 15, 15);
            this.Sizable = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add a friend";
            this.Load += new System.EventHandler(this.frm_addFriend_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialSingleLineTextField UsernameTextBox;
        private MaterialSkin.Controls.MaterialFlatButton AddButton;
        private MaterialSkin.Controls.MaterialFlatButton CancelButton;
        private Controls.UserPanel userPanel;
    }
}