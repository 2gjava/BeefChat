using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using BeefChat.ClientGUI.Controls;
using BeefChat.Shared.Objects;
using MaterialSkin.Controls;

using Microsoft.VisualBasic;

namespace BeefChat.ClientGUI.Forms
{
	public partial class MainForm : MaterialForm
	{
		public MainForm()
		{
			InitializeComponent();
			Program.Msm.AddFormToManage(this);
			this.SetStyle(
				ControlStyles.AllPaintingInWmPaint |
				ControlStyles.UserPaint |
				ControlStyles.DoubleBuffer,
				true);
		}

		#region Events

		private void MainForm_Load(object sender, EventArgs e)
		{
			#region Setup events

			Program.Client.UserInfoReceivedEventHandler += (u) => { if (Program.Client.Username.ToLower() == u.Username.ToLower()) FormHelpers.SafeCall(this, delegate { SetUser(u); }); };
			Program.Client.ChannelsReceivedEventHandler += Client_ChannelsReceivedEvent;
			Program.Client.FriendsReceivedEventHandler += Client_FriendsReceivedEvent;
			Program.Client.DisconnectedEventHandler += Client_DisconnectedEventHandler;
			Program.Client.MessageReceivedEventHandler += Client_MessageReceivedEventHandler;
			SideBar.ItemClickedEventHandler += Item_Click;

			SettingsPropertyGrid.PropertyValueChanged += (s, e2) => { Program.ApplySettings(); };

			#endregion Setup events

			#region Set server related info

			SetUser(Program.Client.GetProfile(Program.Client.Username));
			if (Program.Client.Friends != null) SetFriends(Program.Client.Friends);
			if (Program.Client.Channels != null) SetChannels(Program.Client.Channels);

			#endregion Set server related info

			#region Other UI Setups

			SetText();

			#region Set Property Grid Colors

			SettingsPropertyGrid.BackColor = Program.Msm.GetApplicationBackgroundColor();
			SettingsPropertyGrid.CommandsBackColor = Program.Msm.GetApplicationBackgroundColor();
			SettingsPropertyGrid.HelpBackColor = Program.Msm.GetApplicationBackgroundColor();
			SettingsPropertyGrid.ViewBackColor = Program.Msm.GetApplicationBackgroundColor();

			SettingsPropertyGrid.ViewBorderColor = Program.Msm.GetDividersColor();
			SettingsPropertyGrid.CommandsBorderColor = Program.Msm.GetDividersColor();
			SettingsPropertyGrid.CategorySplitterColor = Program.Msm.GetDividersColor();
			SettingsPropertyGrid.HelpBorderColor = Program.Msm.GetDividersColor();
			SettingsPropertyGrid.LineColor = Program.Msm.GetDividersColor();

			SettingsPropertyGrid.CategoryForeColor = Program.Msm.GetPrimaryTextColor();
			SettingsPropertyGrid.CommandsForeColor = Program.Msm.GetPrimaryTextColor();
			SettingsPropertyGrid.DisabledItemForeColor = Program.Msm.GetDisabledOrHintColor();
			SettingsPropertyGrid.HelpForeColor = Program.Msm.GetPrimaryTextColor();
			SettingsPropertyGrid.ViewForeColor = Program.Msm.GetPrimaryTextColor();

			#endregion Set Property Grid Colors

			AddFriendButton.ForeColor = Program.Msm.ColorScheme.AccentColor;
			AddFriendButton.Icon = Program.GetIcon("account-plus", 24, Drawing.GetReadableColor(Program.Msm.ColorScheme.AccentColor));
			SettingsPropertyGrid.SelectedObject = Program.CurrentSettings;
			SideBar.DeveloperDrawerItem.Visible = Program.DeveloperMode;
			MDCD.DPI = this.CreateGraphics().DpiX;

			#endregion Other UI Setups
		}

		private void MainForm_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Control && e.KeyCode == Keys.F12)
			{
				if (Program.DeveloperMode)
				{
					MessageBox.Show("You have already Developer Mode active.", Program.ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					if (MessageBox.Show("Are you sure you want to activate Developer Mode?", Program.ApplicationName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						Program.DeveloperMode = true;
				}
				SideBar.DeveloperDrawerItem.Visible = Program.DeveloperMode;
			}
		}

		private void tabControl_SelectedIndexChanged(object sender, EventArgs e) => UpdateSelectedItems();

		private void FriendsPanel_Paint(object sender, PaintEventArgs e)
		{
			if (Program.Client.Friends.Count == 0)
			{
				e.Graphics.DrawImage(Program.GetIcon("account-multiple-plus", 48, Color.FromArgb(128, Color.White)), FriendsPanel.Width / 2 - 24, FriendsPanel.Height / 2 - 30);
				e.Graphics.DrawString("You have nobody to share the beef with", Program.Msm.ROBOTO_MEDIUM_10, Brushes.White, new Rectangle(0, 50, FriendsPanel.Width, FriendsPanel.Height - 50), Drawing.Centered);
			}
		}

		private void Item_Click(string item)
		{
			switch (item)
			{
				case "friends": TabControl.SelectTab(nameof(FriendsTabPage)); break;
				case "channels": TabControl.SelectTab(nameof(ChannelsTabPage)); break;
				case "user": TabControl.SelectTab(nameof(UserTabPage)); break;
				case "developer": TabControl.SelectTab(nameof(DeveloperTabPage)); break;
				case "settings": TabControl.SelectTab(nameof(SettingsTabPage)); break;
			}
		}

		#region Click Events

		#region Developer Tab

		private void TestCrashDialogButton_Click(object sender, EventArgs e) => new CrashForm(new Exception("Hello world!")).ShowDialog();

		private void ClearUserCacheButton_Click(object sender, EventArgs e) => Program.Client.ClearUserCache();

		private void NotificationTestButton_Click(object sender, EventArgs e) => Notifications.Show("Hello", "World");

		#endregion Developer Tab

		#region User Profile Tab

		private void ColorListItem_Click(object sender, EventArgs e) => Program.Client.ChangeColorGUI();

		private void PictureListItem_Click(object sender, EventArgs e) => Program.Client.ChangePictureGUI();

		private void StatusListItem_Click(object sender, EventArgs e)
		{
		}

		private void NicknameListItem_Click(object sender, EventArgs e)
		{
			var nick = Interaction.InputBox("Enter your new nickname", Program.ApplicationName, Program.Client.GetProfile(Program.Client.Username).Nickname);
			if (nick.Length != 0)
			{
				Program.Client.ChangeNickname(nick);
			}
		}

		private void DeleteAccountButton_Click(object sender, EventArgs e) => Program.Client.DeleteAccountGUI();

		#endregion User Profile Tab

		private void AddFriendButton_Click(object sender, EventArgs e) => Program.Client.AddFriendGUI();

		#endregion Click Events

		#region Client Events

		private void Client_DisconnectedEventHandler(string reason)
		{
			MessageBox.Show(reason, "Disconnected", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			FormHelpers.SafeCall(this, delegate { this.Dispose(); });
		}

		private void Client_ChannelsReceivedEvent() => FormHelpers.SafeCall(this, delegate { SetChannels(Program.Client.Channels); });

		private void Client_FriendsReceivedEvent() => FormHelpers.SafeCall(this, delegate { SetFriends(Program.Client.Friends); });

		private void Client_MessageReceivedEventHandler(Shared.Objects.Message eventArgs)
		{
			if (eventArgs.Author != Program.Client.Username && this.WindowState == FormWindowState.Minimized)
			{
				var test = Notifications.Show(eventArgs.Author + " - " + eventArgs.Channel.ToString(), eventArgs.Content, null, true);
			}
		}

		#endregion Client Events

		#endregion Events

		#region Functions

		/// <summary>
		/// Sets localized texts for all controls.
		/// </summary>
		public void SetText()
		{
			ToolTip.SetToolTip(SideBar.UserDrawerItem, Localization.GetString("Tabs_UserProfile"));
			SideBar.UserDrawerItem.Text = Localization.GetString("Tabs_UserProfile");
			UserTabPage.Text = Localization.GetString("Tabs_UserProfile");

			ToolTip.SetToolTip(SideBar.FriendsDrawerItem, Localization.GetString("Tabs_Friends"));
			SideBar.FriendsDrawerItem.Text = Localization.GetString("Tabs_Friends");
			FriendsTabPage.Text = Localization.GetString("Tabs_Friends");

			ToolTip.SetToolTip(SideBar.ChannelsDrawerItem, Localization.GetString("Tabs_Channels"));
			SideBar.ChannelsDrawerItem.Text = Localization.GetString("Tabs_Channels");
			ChannelsTabPage.Text = Localization.GetString("Tabs_Channels");

			ToolTip.SetToolTip(SideBar.DeveloperDrawerItem, Localization.GetString("Tabs_Developer"));
			SideBar.DeveloperDrawerItem.Text = Localization.GetString("Tabs_Developer");
			DeveloperTabPage.Text = Localization.GetString("Tabs_Developer");

			ToolTip.SetToolTip(SideBar.SettingsDrawerItem, Localization.GetString("Tabs_Settings"));
			SideBar.SettingsDrawerItem.Text = Localization.GetString("Tabs_Settings");
			SettingsTabPage.Text = Localization.GetString("Tabs_Settings");

			DeleteAccountButton.Text = Localization.GetString("Button_DeleteAccount");
			ColorListItem.PrimaryText = Localization.GetString("ProfileColor");
			PictureListItem.PrimaryText = Localization.GetString("ProfilePicture");

			ManageAccountDivider.Text = Localization.GetString("ManageAccount");
			ClearUserCacheButton.Text = Localization.GetString("Button_ClearUserCache");
		}

		private void UpdateSelectedItems()
		{
			Text = "BeefChat - " + TabControl.SelectedTab.Text;

			SideBar.FriendsDrawerItem.Selected = TabControl.SelectedTab.Name == nameof(FriendsTabPage) ||
								   TabControl.SelectedTab.Text.StartsWith("@");

			SideBar.ChannelsDrawerItem.Selected = TabControl.SelectedTab.Name == nameof(ChannelsTabPage) ||
									TabControl.SelectedTab.Text.StartsWith("#");

			SideBar.SettingsDrawerItem.Selected = TabControl.SelectedTab.Name == nameof(SettingsTabPage);

			SideBar.UserDrawerItem.Selected = TabControl.SelectedTab.Name == nameof(UserTabPage);

			SideBar.DeveloperDrawerItem.Selected = TabControl.SelectedTab.Name == nameof(DeveloperTabPage);

			Refresh();
		}

		public void OpenChat(Channel channel, bool switchTab)
		{
			TabPage tp = null;
			if (TabControl.TabPages.ContainsKey(channel.ToString()))
			{
				foreach (TabPage tabpage in TabControl.TabPages)
				{
					if (tabpage.Text != channel.ToString()) continue;
					tp = tabpage;
					break;
				}
				if (tp == null) throw new ArgumentOutOfRangeException(nameof(channel), "Couldn't find chat.");
			}
			else
			{
				tp = new TabPage(channel.ToString()) { Name = channel.ToString() };
				tp.Controls.Add(new ChatControl(channel) { Dock = DockStyle.Fill });
				TabControl.TabPages.Add(tp);
			}
			if (switchTab) TabControl.SelectTab(tp);
		}

		public void SetFriends(IReadOnlyList<string> friends)
		{
			//Remove all controls
			FriendsPanel.Controls.Clear();

			//Add friend userpanels
			foreach (string friend in friends)
			{
				var up = new UserPanel(friend)
				{
					Dock = DockStyle.Top,
					InnerPadding = 5,
					Font = Program.Msm.ROBOTO_REGULAR_11,
					Size = new Size(100, 53),
					Padding = new Padding(0, 0, 0, 5),
				};
				up.Click += (s, e) => { OpenChat(new Channel(friend, Channel.ChannelType.User), true); };
				FriendsPanel.Controls.Add(up);
			}
			SetOnlineFriendCount();
		}

		public void SetOnlineFriendCount()
		{
			int o = 0;
			foreach (string friend in Program.Client.Friends)
			{
				var p = Program.Client.GetProfile(friend);
				if (p != null && p.Friend && p.Status == User.UserStatus.Online)
				{
					o++;
				}
			}
			SideBar.FriendsDrawerItem.Badge = o;
		}

		public void SetChannels(IReadOnlyList<Channel> channels)
		{
			ChannelsTabPage.Controls.Clear();
			foreach (Channel channel in channels)
			{
				NavigationDrawerItem item = new NavigationDrawerItem
				{
					IconText = "hashtag",
					Text = channel.Name,
					Dock = DockStyle.Top,
				};
				item.Click += (s, e) => OpenChat(channel, true);
				ChannelsTabPage.Controls.Add(item);
			}
		}

		public void SetUser(User user)
		{
			if (user != null)
			{
				NicknameListItem.SecondaryText = user.Nickname;
				ColorListItem.SecondaryText = user.Color;
				StatusListItem.SecondaryText = string.IsNullOrWhiteSpace(user.StatusText) ? user.Status.ToString() : user.StatusText;

				PictureListItem.Icon = user.Picture == null ? Program.GetIcon("account-circle", 64, Color.Gray) : user.Picture.ToImage();

				#region Make Color Circle

				var b = new Bitmap(40, 40);
				var g = Graphics.FromImage(b);
				var c = ColorTranslator.FromHtml(user.Color);
				var i = Program.GetIcon("palette", 24, Color.FromArgb(Drawing.GetByte(0.75), Drawing.GetReadableColor(c)));
				Drawing.SetGraphicsHighQuality(g);
				g.FillEllipse(new SolidBrush(c), 0, 0, 40, 40);
				g.DrawImage(i, 8, 8);

				#endregion Make Color Circle

				ColorListItem.Icon = b;
				g.Dispose();
				UserTabPage.Refresh();
			}
		}

		#endregion Functions
	}
}