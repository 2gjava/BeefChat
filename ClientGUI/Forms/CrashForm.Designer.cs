﻿namespace BeefChat.ClientGUI.Forms
{
	partial class CrashForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.RestartButton = new System.Windows.Forms.Button();
			this.CloseButton = new System.Windows.Forms.Button();
			this.CopyButton = new System.Windows.Forms.Button();
			this.ExceptionGroupBox = new System.Windows.Forms.GroupBox();
			this.ExceptionTextBox = new System.Windows.Forms.TextBox();
			this.HelpButton = new System.Windows.Forms.Button();
			this.ExceptionGroupBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(624, 39);
			this.label1.TabIndex = 4;
			this.label1.Text = "BeefChat had a crash and needed to be shutdown, here are some details. If you wan" +
    "t you can restart  the application without relaunching it again.";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// RestartButton
			// 
			this.RestartButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.RestartButton.DialogResult = System.Windows.Forms.DialogResult.Ignore;
			this.RestartButton.Location = new System.Drawing.Point(554, 271);
			this.RestartButton.Name = "RestartButton";
			this.RestartButton.Size = new System.Drawing.Size(75, 23);
			this.RestartButton.TabIndex = 0;
			this.RestartButton.Text = "&Restart";
			this.RestartButton.UseVisualStyleBackColor = true;
			this.RestartButton.Click += new System.EventHandler(this.RestartButton_Click);
			// 
			// CloseButton
			// 
			this.CloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.CloseButton.DialogResult = System.Windows.Forms.DialogResult.Abort;
			this.CloseButton.Location = new System.Drawing.Point(473, 271);
			this.CloseButton.Name = "CloseButton";
			this.CloseButton.Size = new System.Drawing.Size(75, 23);
			this.CloseButton.TabIndex = 1;
			this.CloseButton.Text = "&Close";
			this.CloseButton.UseVisualStyleBackColor = true;
			// 
			// CopyButton
			// 
			this.CopyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.CopyButton.Location = new System.Drawing.Point(392, 271);
			this.CopyButton.Name = "CopyButton";
			this.CopyButton.Size = new System.Drawing.Size(75, 23);
			this.CopyButton.TabIndex = 2;
			this.CopyButton.Text = "Co&py Details";
			this.CopyButton.UseVisualStyleBackColor = true;
			this.CopyButton.Click += new System.EventHandler(this.CopyButton_Click);
			// 
			// ExceptionGroupBox
			// 
			this.ExceptionGroupBox.Controls.Add(this.ExceptionTextBox);
			this.ExceptionGroupBox.Location = new System.Drawing.Point(11, 50);
			this.ExceptionGroupBox.Name = "ExceptionGroupBox";
			this.ExceptionGroupBox.Size = new System.Drawing.Size(618, 215);
			this.ExceptionGroupBox.TabIndex = 5;
			this.ExceptionGroupBox.TabStop = false;
			this.ExceptionGroupBox.Text = "Exception";
			// 
			// ExceptionTextBox
			// 
			this.ExceptionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.ExceptionTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ExceptionTextBox.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ExceptionTextBox.Location = new System.Drawing.Point(3, 16);
			this.ExceptionTextBox.Multiline = true;
			this.ExceptionTextBox.Name = "ExceptionTextBox";
			this.ExceptionTextBox.ReadOnly = true;
			this.ExceptionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.ExceptionTextBox.Size = new System.Drawing.Size(612, 196);
			this.ExceptionTextBox.TabIndex = 7;
			this.ExceptionTextBox.WordWrap = false;
			// 
			// HelpButton
			// 
			this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.HelpButton.Enabled = false;
			this.HelpButton.Location = new System.Drawing.Point(311, 271);
			this.HelpButton.Name = "HelpButton";
			this.HelpButton.Size = new System.Drawing.Size(75, 23);
			this.HelpButton.TabIndex = 6;
			this.HelpButton.Text = "Get &Help";
			this.HelpButton.UseVisualStyleBackColor = true;
			this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
			// 
			// CrashForm
			// 
			this.AcceptButton = this.RestartButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.CloseButton;
			this.ClientSize = new System.Drawing.Size(640, 305);
			this.Controls.Add(this.HelpButton);
			this.Controls.Add(this.ExceptionGroupBox);
			this.Controls.Add(this.CopyButton);
			this.Controls.Add(this.CloseButton);
			this.Controls.Add(this.RestartButton);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "CrashForm";
			this.Padding = new System.Windows.Forms.Padding(8);
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Oh no!";
			this.ExceptionGroupBox.ResumeLayout(false);
			this.ExceptionGroupBox.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button RestartButton;
		private System.Windows.Forms.Button CloseButton;
		private System.Windows.Forms.Button CopyButton;
		private System.Windows.Forms.GroupBox ExceptionGroupBox;
		private System.Windows.Forms.TextBox ExceptionTextBox;
		private System.Windows.Forms.Button HelpButton;
	}
}