﻿namespace BeefChat.ClientGUI.Forms
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.BannerPanel = new System.Windows.Forms.Panel();
			this.ContentPanel = new System.Windows.Forms.Panel();
			this.RememberCheckBox = new MaterialSkin.Controls.MaterialCheckBox();
			this.PasswordTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
			this.UsernameTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
			this.IpAddressTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
			this.CloseButton = new MaterialSkin.Controls.MaterialFlatButton();
			this.RegisterButton = new MaterialSkin.Controls.MaterialFlatButton();
			this.LoginButton = new MaterialSkin.Controls.MaterialFlatButton();
			this.ButtonPanel = new System.Windows.Forms.Panel();
			this.ContentPanel.SuspendLayout();
			this.ButtonPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// BannerPanel
			// 
			this.BannerPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.BannerPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.BannerPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.BannerPanel.Location = new System.Drawing.Point(0, 64);
			this.BannerPanel.Name = "BannerPanel";
			this.BannerPanel.Size = new System.Drawing.Size(370, 150);
			this.BannerPanel.TabIndex = 7;
			// 
			// ContentPanel
			// 
			this.ContentPanel.BackColor = System.Drawing.SystemColors.Control;
			this.ContentPanel.Controls.Add(this.RememberCheckBox);
			this.ContentPanel.Controls.Add(this.PasswordTextBox);
			this.ContentPanel.Controls.Add(this.UsernameTextBox);
			this.ContentPanel.Controls.Add(this.IpAddressTextBox);
			this.ContentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ContentPanel.Location = new System.Drawing.Point(0, 214);
			this.ContentPanel.Name = "ContentPanel";
			this.ContentPanel.Padding = new System.Windows.Forms.Padding(15);
			this.ContentPanel.Size = new System.Drawing.Size(370, 210);
			this.ContentPanel.TabIndex = 8;
			// 
			// RememberCheckBox
			// 
			this.RememberCheckBox.AutoSize = true;
			this.RememberCheckBox.BackColor = System.Drawing.SystemColors.Control;
			this.RememberCheckBox.Depth = 0;
			this.RememberCheckBox.Dock = System.Windows.Forms.DockStyle.Top;
			this.RememberCheckBox.Font = new System.Drawing.Font("Roboto", 10F);
			this.RememberCheckBox.Location = new System.Drawing.Point(15, 84);
			this.RememberCheckBox.Margin = new System.Windows.Forms.Padding(0);
			this.RememberCheckBox.MouseLocation = new System.Drawing.Point(-1, -1);
			this.RememberCheckBox.MouseState = MaterialSkin.MouseState.HOVER;
			this.RememberCheckBox.Name = "RememberCheckBox";
			this.RememberCheckBox.Ripple = true;
			this.RememberCheckBox.Size = new System.Drawing.Size(340, 30);
			this.RememberCheckBox.TabIndex = 13;
			this.RememberCheckBox.Text = "Remember Me";
			this.RememberCheckBox.UseVisualStyleBackColor = false;
			// 
			// PasswordTextBox
			// 
			this.PasswordTextBox.BackColor = System.Drawing.SystemColors.Control;
			this.PasswordTextBox.Depth = 0;
			this.PasswordTextBox.Dock = System.Windows.Forms.DockStyle.Top;
			this.PasswordTextBox.Hint = "Password";
			this.PasswordTextBox.Location = new System.Drawing.Point(15, 61);
			this.PasswordTextBox.MaxLength = 32767;
			this.PasswordTextBox.MouseState = MaterialSkin.MouseState.HOVER;
			this.PasswordTextBox.Name = "PasswordTextBox";
			this.PasswordTextBox.PasswordChar = '\0';
			this.PasswordTextBox.SelectedText = "";
			this.PasswordTextBox.SelectionLength = 0;
			this.PasswordTextBox.SelectionStart = 0;
			this.PasswordTextBox.Size = new System.Drawing.Size(340, 23);
			this.PasswordTextBox.TabIndex = 8;
			this.PasswordTextBox.TabStop = false;
			this.PasswordTextBox.UseSystemPasswordChar = true;
			// 
			// UsernameTextBox
			// 
			this.UsernameTextBox.BackColor = System.Drawing.SystemColors.Control;
			this.UsernameTextBox.Depth = 0;
			this.UsernameTextBox.Dock = System.Windows.Forms.DockStyle.Top;
			this.UsernameTextBox.Hint = "Username";
			this.UsernameTextBox.Location = new System.Drawing.Point(15, 38);
			this.UsernameTextBox.MaxLength = 32767;
			this.UsernameTextBox.MouseState = MaterialSkin.MouseState.HOVER;
			this.UsernameTextBox.Name = "UsernameTextBox";
			this.UsernameTextBox.PasswordChar = '\0';
			this.UsernameTextBox.SelectedText = "";
			this.UsernameTextBox.SelectionLength = 0;
			this.UsernameTextBox.SelectionStart = 0;
			this.UsernameTextBox.Size = new System.Drawing.Size(340, 23);
			this.UsernameTextBox.TabIndex = 7;
			this.UsernameTextBox.TabStop = false;
			this.UsernameTextBox.UseSystemPasswordChar = false;
			// 
			// IpAddressTextBox
			// 
			this.IpAddressTextBox.BackColor = System.Drawing.SystemColors.Control;
			this.IpAddressTextBox.Depth = 0;
			this.IpAddressTextBox.Dock = System.Windows.Forms.DockStyle.Top;
			this.IpAddressTextBox.Hint = "IP Address";
			this.IpAddressTextBox.Location = new System.Drawing.Point(15, 15);
			this.IpAddressTextBox.MaxLength = 32767;
			this.IpAddressTextBox.MouseState = MaterialSkin.MouseState.HOVER;
			this.IpAddressTextBox.Name = "IpAddressTextBox";
			this.IpAddressTextBox.PasswordChar = '\0';
			this.IpAddressTextBox.SelectedText = "";
			this.IpAddressTextBox.SelectionLength = 0;
			this.IpAddressTextBox.SelectionStart = 0;
			this.IpAddressTextBox.Size = new System.Drawing.Size(340, 23);
			this.IpAddressTextBox.TabIndex = 12;
			this.IpAddressTextBox.TabStop = false;
			this.IpAddressTextBox.Text = "localhost";
			this.IpAddressTextBox.UseSystemPasswordChar = false;
			// 
			// CloseButton
			// 
			this.CloseButton.AutoSize = true;
			this.CloseButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.CloseButton.BackColor = System.Drawing.SystemColors.Control;
			this.CloseButton.Depth = 0;
			this.CloseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.CloseButton.Dock = System.Windows.Forms.DockStyle.Right;
			this.CloseButton.Icon = null;
			this.CloseButton.Location = new System.Drawing.Point(153, 10);
			this.CloseButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
			this.CloseButton.MouseState = MaterialSkin.MouseState.HOVER;
			this.CloseButton.Name = "CloseButton";
			this.CloseButton.Primary = false;
			this.CloseButton.Size = new System.Drawing.Size(63, 40);
			this.CloseButton.TabIndex = 17;
			this.CloseButton.Text = "Close";
			this.CloseButton.UseVisualStyleBackColor = false;
			this.CloseButton.Click += new System.EventHandler(this.ButtonClick);
			// 
			// RegisterButton
			// 
			this.RegisterButton.AutoSize = true;
			this.RegisterButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.RegisterButton.BackColor = System.Drawing.SystemColors.Control;
			this.RegisterButton.Depth = 0;
			this.RegisterButton.DialogResult = System.Windows.Forms.DialogResult.Yes;
			this.RegisterButton.Dock = System.Windows.Forms.DockStyle.Right;
			this.RegisterButton.Icon = null;
			this.RegisterButton.Location = new System.Drawing.Point(216, 10);
			this.RegisterButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
			this.RegisterButton.MouseState = MaterialSkin.MouseState.HOVER;
			this.RegisterButton.Name = "RegisterButton";
			this.RegisterButton.Primary = false;
			this.RegisterButton.Size = new System.Drawing.Size(83, 40);
			this.RegisterButton.TabIndex = 16;
			this.RegisterButton.Text = "Register";
			this.RegisterButton.UseVisualStyleBackColor = false;
			this.RegisterButton.Click += new System.EventHandler(this.ButtonClick);
			// 
			// LoginButton
			// 
			this.LoginButton.AutoSize = true;
			this.LoginButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.LoginButton.BackColor = System.Drawing.SystemColors.Control;
			this.LoginButton.Depth = 0;
			this.LoginButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.LoginButton.Dock = System.Windows.Forms.DockStyle.Right;
			this.LoginButton.Icon = null;
			this.LoginButton.Location = new System.Drawing.Point(299, 10);
			this.LoginButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
			this.LoginButton.MouseState = MaterialSkin.MouseState.HOVER;
			this.LoginButton.Name = "LoginButton";
			this.LoginButton.Primary = true;
			this.LoginButton.Size = new System.Drawing.Size(61, 40);
			this.LoginButton.TabIndex = 15;
			this.LoginButton.Text = "Login";
			this.LoginButton.UseVisualStyleBackColor = false;
			this.LoginButton.Click += new System.EventHandler(this.ButtonClick);
			// 
			// ButtonPanel
			// 
			this.ButtonPanel.Controls.Add(this.CloseButton);
			this.ButtonPanel.Controls.Add(this.RegisterButton);
			this.ButtonPanel.Controls.Add(this.LoginButton);
			this.ButtonPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.ButtonPanel.Location = new System.Drawing.Point(0, 424);
			this.ButtonPanel.Name = "ButtonPanel";
			this.ButtonPanel.Padding = new System.Windows.Forms.Padding(10);
			this.ButtonPanel.Size = new System.Drawing.Size(370, 60);
			this.ButtonPanel.TabIndex = 19;
			// 
			// LoginForm
			// 
			this.AcceptButton = this.LoginButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.CloseButton;
			this.ClientSize = new System.Drawing.Size(370, 484);
			this.Controls.Add(this.ContentPanel);
			this.Controls.Add(this.ButtonPanel);
			this.Controls.Add(this.BannerPanel);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "LoginForm";
			this.Padding = new System.Windows.Forms.Padding(0, 64, 0, 0);
			this.Sizable = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Login in to a BeefChat server";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LoginForm_FormClosing);
			this.Load += new System.EventHandler(this.LoginForm_Load);
			this.ContentPanel.ResumeLayout(false);
			this.ContentPanel.PerformLayout();
			this.ButtonPanel.ResumeLayout(false);
			this.ButtonPanel.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel BannerPanel;
        private System.Windows.Forms.Panel ContentPanel;
        private MaterialSkin.Controls.MaterialCheckBox RememberCheckBox;
        private MaterialSkin.Controls.MaterialSingleLineTextField PasswordTextBox;
        private MaterialSkin.Controls.MaterialSingleLineTextField UsernameTextBox;
        private MaterialSkin.Controls.MaterialSingleLineTextField IpAddressTextBox;
		private MaterialSkin.Controls.MaterialFlatButton CloseButton;
		private MaterialSkin.Controls.MaterialFlatButton RegisterButton;
		private MaterialSkin.Controls.MaterialFlatButton LoginButton;
		private System.Windows.Forms.Panel ButtonPanel;
	}
}

