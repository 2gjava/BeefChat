﻿using MaterialSkin.Controls;

namespace BeefChat.ClientGUI.Forms
{
	public partial class LoadingForm : MaterialForm
	{
		public string LoadingText
		{
			set => FormHelpers.SafeCall(this, delegate { LoadingLabel.Text = value; Refresh(); });
			get
			{
				string result = null;
				FormHelpers.SafeCall(this, delegate
				{
					result = LoadingLabel.Text;
				});
				return result;
			}
		}

		public LoadingForm(string text)
		{
			InitializeComponent();
			Program.Msm.AddFormToManage(this);
			LoadingLabel.Text = text;
		}
	}
}