﻿using System;

using MaterialSkin.Controls;

namespace BeefChat.ClientGUI.Forms
{
	public partial class AddFriendForm : MaterialForm
	{
		public string Username => UsernameTextBox.Text;

		public AddFriendForm()
		{
			InitializeComponent();
			Program.Msm.AddFormToManage(this);
			userPanel.Font = Program.Msm.ROBOTO_REGULAR_11;
		}

		private void frm_addFriend_Load(object sender, EventArgs e)
		{
		}

		private void CancelButton_Click(object sender, EventArgs e) => Close();

		private void AddButton_Click(object sender, EventArgs e) => Close();

		private void UsernameTextBox_TextChanged(object sender, EventArgs e)
		{
			userPanel.User = UsernameTextBox.Text;
			userPanel.Update();
		}
	}
}