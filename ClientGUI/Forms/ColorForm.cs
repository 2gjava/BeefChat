﻿using System;
using System.Drawing;
using System.Windows.Forms;

using BeefChat.ClientGUI.Controls;

using MaterialSkin.Controls;

namespace BeefChat.ClientGUI.Forms
{
	public partial class ColorForm : MaterialForm
	{
		public Color Color { get; private set; }

		public ColorForm()
		{
			InitializeComponent();
			Program.Msm.AddFormToManage(this);
			CustomColorButton.Icon = Program.GetIcon("palette", 24, Color.Black);
		}

		private void btn_color_Click(object sender, EventArgs e)
		{
			if (sender.GetType() != typeof(RoundButton)) return;
			var btn = (RoundButton)sender;
			if (btn.Name == "btn_custom")
			{
				var dialog = new ColorDialog();
				if (dialog.ShowDialog() == DialogResult.OK)
				{
					btn.ForeColor = dialog.Color;
					btn.Refresh();
				}
			}
			Color = btn.ForeColor;
			SetChecks(btn.Name);
		}

		private void SetChecks(string name)
		{
			foreach (Control ctl in Controls)
			{
				if (ctl.GetType() != typeof(RoundButton)) continue;
				((RoundButton)ctl).Icon = ctl.Name == name ? Program.GetIcon("check", 24, ((RoundButton)ctl).ForeColor.GetReadableColor()) : (ctl.Name == "btn_custom" ? Program.GetIcon("palette", 24, Color.White) : null);
				ctl.Refresh();
			}
		}

		private void btn_close_Click(object sender, EventArgs e) => Close();
	}
}