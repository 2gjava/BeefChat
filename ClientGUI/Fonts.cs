﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Runtime.InteropServices;

namespace BeefChat.ClientGUI
{
	[Obsolete("This feature won't be used anymore because the custom MaterialSkin already comes preloaded with Roboto fonts.")]
	public class Fonts : IDisposable
	{
		//https://stackoverflow.com/questions/33115880/how-to-render-a-font-from-privatefontcollection-memory-to-editable-controls
		[DllImport("gdi32.dll")]
		private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont, IntPtr pdv, [In] ref uint pcFonts);

		private static PrivateFontCollection pfc { get; } = new PrivateFontCollection();

		private List<byte[]> FontsToAdd = new List<byte[]>()
		{
		};

		public Fonts()
		{
			foreach (byte[] font in FontsToAdd)
				AddMemoryFont(font);
		}

		public void AddMemoryFont(byte[] fontResource)
		{
			IntPtr p;
			uint c = 0;
			p = Marshal.AllocCoTaskMem(fontResource.Length);
			Marshal.Copy(fontResource, 0, p, fontResource.Length);
			AddFontMemResourceEx(p, (uint)fontResource.Length, IntPtr.Zero, ref c);
			pfc.AddMemoryFont(p, fontResource.Length);
			Marshal.FreeCoTaskMem(p);
			p = IntPtr.Zero;
		}

		/// <summary>
		/// Returns a font from memory loaded fonts.
		/// </summary>
		/// <returns>The requested font</returns>
		public static Font GetFont(int fontIndex, float fontSize = 20, FontStyle fontStyle = FontStyle.Regular)
		{
			return new Font(pfc.Families[fontIndex], fontSize, fontStyle);
		}

		/// <summary>
		/// Returns a font from memory loaded fonts.
		/// </summary>
		/// <returns>The requested font</returns>
		public static Font GetFont(string name, float fontSize = 20, FontStyle fontStyle = FontStyle.Regular)
		{
			var index = Array.FindIndex(pfc.Families, f => f.Name == name);
			if (index == -1) throw new ArgumentOutOfRangeException(nameof(name), "Font wasn't found.");
			return GetFont(index, fontSize, fontStyle);
		}

		public void Dispose()
		{
			pfc.Dispose();
		}
	}
}