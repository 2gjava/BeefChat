﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Media;
using System.Xml;

using MaterialSkin;

using Newtonsoft.Json;

using Svg;

namespace BeefChat.ClientGUI
{
	public class ThemeManager
	{
		public List<Theme> ThemeList { get; } = new List<Theme>();
		public Theme CurrentTheme { get; set; }
		public Theme DefaultTheme { get; private set; }

		public ThemeManager(string defaultTheme)
		{
			LoadAllThemes();
			CreateDefaultTheme();
			DefaultTheme = LoadTheme(defaultTheme);
			CurrentTheme = DefaultTheme;
		}

		private static void CreateDefaultTheme()
		{
			var path = Path.Combine("themes", "Default");
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
				File.WriteAllText(Path.Combine(path, "theme.json"), JsonConvert.SerializeObject(new Theme()
				{
					Accent = ColorTranslator.FromHtml("#01d6d6"),
					Author = Program.ApplicationName,
					Dark = true,
					Name = "Default",
					Primary = ColorTranslator.FromHtml("#ff3c1f"),
					PrimaryDark = ColorTranslator.FromHtml("#ee220f"),
					PrimaryLight = ColorTranslator.FromHtml("#ffa690"),
					TextShade = TextShade.WHITE
				}));
			}
		}

		private static Theme LoadTheme(string themeDirectory)
		{
			try
			{
				string content = File.ReadAllText(Path.Combine(themeDirectory, "theme.json"));
				Theme theme = JsonConvert.DeserializeObject<Theme>(content);

				//Load Icons
				string iconDirectory = Path.Combine(themeDirectory, "icons");
				if (Directory.Exists(iconDirectory))
				{
					theme.Icons.Clear();
					foreach (string iconPath in Directory.GetFiles(iconDirectory))
					{
						if (Path.GetExtension(iconPath) == ".svg")
						{
							XmlDocument xmldoc = new XmlDocument();
							xmldoc.LoadXml(File.ReadAllText(iconPath));
							theme.Icons.Add(Path.GetFileNameWithoutExtension(iconPath).ToLower(), SvgDocument.Open(xmldoc));
						}
						if (Path.GetExtension(iconPath) == ".png")
						{
							theme.Icons.Add(Path.GetFileNameWithoutExtension(iconPath).ToLower(), Image.FromFile(iconPath));
						}
					}
				}
				//Load Sounds
				string soundDirectory = Path.Combine(themeDirectory, "sounds");
				if (Directory.Exists(soundDirectory))
				{
					theme.Sounds.Clear();
					foreach (string soundPath in Directory.GetFiles(soundDirectory))
					{
						if (Path.GetExtension(soundPath) == ".wav")
						{
							SoundPlayer sound = new SoundPlayer(soundPath);
							theme.Sounds.Add(Path.GetFileNameWithoutExtension(soundPath).ToLower(), sound);
						}
					}
				}
				//Successfully loaded the theme
				return theme;
			}
			catch (Exception ex)
			{
				throw new Exception("Failed to load theme from directory " + themeDirectory, ex);
			}
		}

		public void LoadAllThemes()
		{
			ThemeList.Clear();
			Directory.CreateDirectory("themes");
			foreach (string themeDirectory in Directory.GetDirectories("themes"))
			{
				Theme theme = LoadTheme(themeDirectory);
				ThemeList.Add(theme);
			}
		}
	}

	public class Theme
	{
		public string Name { get; set; }
		public string Author { get; set; }

		[JsonIgnore]
		public Dictionary<string, object> Icons { get; } = new Dictionary<string, object>();

		[JsonIgnore]
		public Dictionary<string, SoundPlayer> Sounds { get; } = new Dictionary<string, SoundPlayer>();

		#region Material Skin Properties

		public Color PrimaryDark { get; set; }
		public Color Primary { get; set; }
		public Color PrimaryLight { get; set; }
		public Color Accent { get; set; }
		public TextShade TextShade { get; set; }
		public bool Dark { get; set; }

		#endregion Material Skin Properties
	}
}