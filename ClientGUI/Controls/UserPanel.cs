﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BeefChat.Shared.Objects;

namespace BeefChat.ClientGUI.Controls
{
	public partial class UserPanel : Control
	{
		public string User { get; set; } = "";
		public int InnerPadding { get; set; } = 5;

		public UserPanel(string user)
		{
			InitializeComponent();
			this.SetStyle(ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.SupportsTransparentBackColor, true);

			Paint += UserPanel_Paint;
			Program.Client.UserInfoReceivedEventHandler += Client_UserInfoReceivedEventHandler;
			User = user;
		}

		public UserPanel()
		{
			InitializeComponent();
			this.SetStyle(ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.SupportsTransparentBackColor, true);

			Paint += UserPanel_Paint;
		}

		private void Client_UserInfoReceivedEventHandler(Shared.Objects.User user)
		{
			if (user.Username == User)
				FormHelpers.SafeCall(this, delegate { this.Refresh(); });
		}

		private void UserPanel_Paint(object sender, PaintEventArgs e)
		{
            Drawing.SetGraphicsHighQuality(e.Graphics);

            User user;
            if (String.IsNullOrWhiteSpace(User))
            {
                user = Shared.Objects.User.ExampleUser;
            }
            else
            {
                user = Program.Client.GetProfile(User);
                if (user == null)
                    user = Shared.Objects.User.ExampleUser;
            }
            if (user != null)
			{
				//Draw Image
				var color = string.IsNullOrWhiteSpace(user.Color) ? Color.Silver : ColorTranslator.FromHtml(user.Color);
				var textPoint = new PointF(Padding.Left + InnerPadding + this.Height - InnerPadding, Padding.Top + Padding.Left + InnerPadding);
				var statusText = user.StatusText;
				var textColor = Drawing.GetReadableColor(color);
				var picRect = new Rectangle(Padding.Left + InnerPadding,
						Padding.Top + InnerPadding,
						this.Height - InnerPadding * 2 - Padding.Right,
						this.Height - InnerPadding * 2 - Padding.Bottom);

				e.Graphics.FillRoundedRectangle(new SolidBrush(color), new Rectangle(Padding.Left, Padding.Top, Width - Padding.Right, Height - Padding.Bottom), 10);
				e.Graphics.DrawString(user.Nickname, this.Font, new SolidBrush(textColor), textPoint);
				textPoint.Y += this.Font.Size + (this.Font.Size / 2);
				e.Graphics.DrawString(statusText, this.Font, new SolidBrush(Color.FromArgb(Drawing.GetByte(0.5), textColor)), textPoint);

				if (string.IsNullOrWhiteSpace(User))
                    e.Graphics.FillEllipse(Brushes.White, picRect);
				else
				{
					var image = user.Picture == null ? Program.GetIcon("account-circle", 48, Color.FromArgb(Drawing.GetByte(0.5), textColor)) : user.Picture.ToImage();
					image = Drawing.ClipToCircle(image, new Size(48, 48));
					e.Graphics.DrawImage(image, picRect);
				}
			}
			else
			{
				e.Graphics.Clear(Color.Black);
				e.Graphics.DrawString("Profile is null", this.Font, Brushes.Red, 0, 0);
			}
		}
	}
}