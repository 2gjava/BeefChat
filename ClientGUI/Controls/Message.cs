﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BeefChat.Shared;
using BeefChat.Shared.Logging;
using BeefChat.Shared.Objects;

namespace BeefChat.ClientGUI.Controls
{
	public partial class Message : Control
	{
		/// <summary>
		/// The font that will be used to draw the author's name.
		/// </summary>
		public Font AuthorFont { get; set; } = new Font("Roboto", 10);

		public int AuthorSpacing { get; set; }

		/// <summary>
		/// Size of the authors
		/// </summary>
		public int ImageSize { get; set; } = 32;

		/// <summary>
		/// Spacing between the author's profile picture and message.
		/// </summary>
		public int ImageSpacing { get; set; } = 5;

		public Padding TextPadding { get; set; } = new Padding();

		/// <summary>
		/// When true, the name will be drawn outside of the message background.
		/// </summary>
		public bool AuthorOverMessage { get; set; }

		/// <summary>
		/// Message that will get rendered
		/// </summary>
		public Shared.Objects.Message message { get; set; }

		public Message()
		{
			InitializeComponent();
			this.SetStyle(ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.SupportsTransparentBackColor, true);
		}

		public Message(Shared.Objects.Message message)
		{
			InitializeComponent();
			this.message = message;
			if (message.Attachments.Count != 0 && message.Attachments[0].GetType() == typeof(File))
			{
				this.DoubleClick += (s, e) =>
				{
					var file = (File)message.Attachments[0];
					var dialog = new SaveFileDialog();
					dialog.FileName = file.Name;
					var ext = System.IO.Path.GetExtension(file.Name);
					dialog.Filter = $"{ext} file (*{ext})|*{ext}|All files (*.*)|*.*";
					if (dialog.ShowDialog() == DialogResult.OK)
					{
						System.IO.File.WriteAllBytes(dialog.FileName, file.Content);
					}
				};
			}
			this.SetStyle(ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.SupportsTransparentBackColor, true);
		}

		private SizeF DrawMessageFileContent(Graphics g, string fileName, User u, SizeF authorSize)
		{
			return g.MeasureString(fileName, Font, Width - (TextPadding.Left + Padding.Left + TextPadding.Right + Padding.Right + ImageSize + ImageSpacing + 28));
		}

		private static Color GetMessageBackgroundColor(User u)
		{
			if (u?.Color != null) return ColorTranslator.FromHtml(u.Color);
			return Program.Msm.ColorScheme.PrimaryColor;
		}

		private Brush GetMessageBackgroundBrush(User u) => new SolidBrush(GetMessageBackgroundColor(u));

		private void DrawProfilePicture(Graphics g, Image image, SizeF authorSize)
		{
			g.DrawImage(Drawing.ClipToCircle(
							image,
							new Size(
								ImageSize,
								ImageSize)),
							new Rectangle(
								new Point(Padding.Left, Padding.Top + (int)(AuthorOverMessage ? authorSize.Height + AuthorSpacing : 0)),
								new Size(
									ImageSize,
									ImageSize
									)));
		}

		private void Message_Paint(object sender, PaintEventArgs e)
		{
			if (message == null) return;
			Drawing.SetGraphicsHighQuality(e.Graphics);
			User author = Program.Client.GetProfile(message.Author);
			SizeF authorSize = e.Graphics.MeasureString(author.Nickname, AuthorFont);

			#region Measure Message

			SizeF messageSize = SizeF.Empty;
			if (!String.IsNullOrWhiteSpace(message.Content))
			{
				messageSize = e.Graphics.MeasureString(message.Content, Font, Width - (TextPadding.Left + Padding.Left + TextPadding.Right + Padding.Right + ImageSize + ImageSpacing));
			}
			if (message.Attachments.Count != 0 && message.Attachments[0].GetType() == typeof(File))
			{
				messageSize = e.Graphics.MeasureString((message.Attachments[0] as File).Name, this.Font, new SizeF(
					Width - (TextPadding.Left + Padding.Left + TextPadding.Right + Padding.Right + ImageSize + ImageSpacing + 28),
					Height - TextPadding.Top + TextPadding.Bottom + Padding.Top + Padding.Bottom));
				//Icon
				messageSize.Width += 24;
			}

			#endregion Measure Message

			#region Draw Background

			if (!messageSize.IsEmpty)
			{
				e.Graphics.FillRoundedRectangle(
					GetMessageBackgroundBrush(author),
					new Rectangle(
						new Point(
							ImageSize + ImageSpacing + Padding.Left,
							Padding.Top + (int)(AuthorOverMessage ? AuthorSpacing + authorSize.Height : 0)),
						new Size(
							TextPadding.Right + (int)(AuthorOverMessage ? messageSize.Width : Math.Max(authorSize.Width, messageSize.Width)),
							Height - Padding.Bottom - Padding.Top - (int)(AuthorOverMessage ? authorSize.Height + AuthorSpacing : 0))),
					16);
			}

			#endregion Draw Background

			#region Draw Author

			if (author != null)
			{
				e.Graphics.DrawString(
						author.Nickname,
						AuthorFont,
						AuthorOverMessage ? GetMessageBackgroundBrush(author) : new SolidBrush(Color.FromArgb(128, GetMessageBackgroundColor(author).GetReadableColor())),
						new PointF(
							ImageSize + ImageSpacing + Padding.Left + TextPadding.Left,
							(AuthorOverMessage ? 0 : TextPadding.Top) + Padding.Top));
				var picture = author.Picture != null ? author.Picture.ToImage() : Program.GetIcon("account-circle", 64, author != null ? ColorTranslator.FromHtml(author.Color) : Drawing.GetReadableColor(Program.Msm.GetApplicationBackgroundColor()));
				e.Graphics.DrawImage(Drawing.ClipToCircle(
							picture,
							new Size(
								ImageSize,
								ImageSize)),
							new Rectangle(
								new Point(Padding.Left, Padding.Top + (int)(AuthorOverMessage ? authorSize.Height + AuthorSpacing : 0)),
								new Size(
									ImageSize,
									ImageSize
									)));
			}
			else
			{
				Logger.Add(LogEntryType.Debug, "Author is null");
			}

			#endregion Draw Author

			Height = (int)messageSize.Height + (int)authorSize.Height + ((TextPadding.Top + TextPadding.Bottom) + (Padding.Top + Padding.Bottom));

			#region Draw text content

			if (!String.IsNullOrWhiteSpace(message.Content))
			{
				e.Graphics.DrawString(
					message.Content,
					Font,
					new SolidBrush(GetMessageBackgroundColor(author).GetReadableColor()),
					new RectangleF(
						new PointF(
							ImageSize + ImageSpacing + Padding.Left + TextPadding.Left,
							TextPadding.Top + Padding.Top + AuthorSpacing + authorSize.Height),
						new SizeF(
							Width - (TextPadding.Left + Padding.Left + TextPadding.Right + Padding.Right + ImageSize + ImageSpacing),
							Height - TextPadding.Top + TextPadding.Bottom + Padding.Top + Padding.Bottom)));
			}

			#endregion Draw text content

			#region Draw attachment content

			if (message.Attachments.Count != 0)
			{
				switch (message.Attachments[0])
				{
					case File a:

						var rColor = GetMessageBackgroundColor(author).GetReadableColor();
						e.Graphics.DrawImage(Program.GetIcon("file", 24, rColor),
							ImageSize + ImageSpacing + Padding.Left + TextPadding.Left,
							TextPadding.Top + Padding.Top + AuthorSpacing + authorSize.Height - 3);
						e.Graphics.DrawString(
								a.Name,
								Font,
								new SolidBrush(rColor),
								new RectangleF(
									new PointF(
										ImageSize + ImageSpacing + Padding.Left + TextPadding.Left + 28,
										TextPadding.Top + Padding.Top + AuthorSpacing + authorSize.Height),
									new SizeF(
										Width - (TextPadding.Left + Padding.Left + TextPadding.Right + Padding.Right + ImageSize + ImageSpacing + 28),
										Height - TextPadding.Top + TextPadding.Bottom + Padding.Top + Padding.Bottom)));
						break;
				}
			}

			#endregion Draw attachment content
		}
	}
}