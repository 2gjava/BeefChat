﻿using System.Drawing;
using System.Windows.Forms;

namespace BeefChat.ClientGUI.Controls
{
	public partial class Divider : Control
	{
		public int TextSpacing { get; set; } = 5;
		public int DividerHeight { get; set; } = 1;

		public Divider()
		{
			InitializeComponent();
			SetStyle(ControlStyles.SupportsTransparentBackColor, true);
			this.Paint += Divider_Paint;
		}

		private void Divider_Paint(object sender, PaintEventArgs e)
		{
			var color = Program.Msm.GetDisabledOrHintColor();
			var dividerColor = Color.FromArgb(Drawing.GetByte(0.12), color);
			var textColor = Color.FromArgb(Drawing.GetByte(0.5), color);
			e.Graphics.FillRectangle(new SolidBrush(dividerColor), Padding.Left, Padding.Top, this.Width - Padding.Right, DividerHeight);
			e.Graphics.DrawString(Text, this.Font, new SolidBrush(textColor), Padding.Left, Padding.Top + DividerHeight + TextSpacing);
		}
	}
}