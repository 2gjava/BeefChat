﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace BeefChat.ClientGUI.Controls
{
	[DefaultEvent("Click")]
	public partial class NavigationDrawerItem : Control
	{
		public string IconText { get; set; } = "circle";
		public Image Icon { get; set; } = null;

		public int Badge { get; set; } = 0;

		private bool selected { get; set; }
		private bool IsDown { get; set; }
		private bool IsHovering { get; set; }

		public bool Selected
		{
			get => selected;
			set
			{
				selected = value;
				Refresh();
			}
		}

		public bool DrawText { get; set; } = true;
		public bool Raised { get; set; }
		private readonly Font _badgeFont = new Font("Roboto Medium", 8f, FontStyle.Regular);

		[EditorBrowsable(EditorBrowsableState.Always)]
		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[Bindable(true)]
		public override string Text { get; set; }

		public bool CustomBackgroundColor { get; set; }

		public NavigationDrawerItem()
		{
			InitializeComponent();
			SetStyle(ControlStyles.SupportsTransparentBackColor, true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
		}

		private void NavigationDrawerItem_Paint(object sender, PaintEventArgs e)
		{
			Drawing.SetGraphicsHighQuality(e.Graphics);
			e.Graphics.Clear(GetBackgroundColor());

			var alpha = Selected || IsDown ? 0.3 : IsHovering ? 0.15 : 0;
			var selectionColor = Color.FromArgb(Drawing.GetByte(alpha), Program.Msm.ColorScheme.PrimaryColor);
			if (this.Height < this.Width)
			{
				e.Graphics.FillRoundedRectangle(new SolidBrush(selectionColor), new Rectangle(new Point(0, 0), this.Size), 5);
				if (DrawText)
				{
					e.Graphics.DrawString(Text,
								  Program.Msm.ROBOTO_MEDIUM_10,
								  Selected ? Program.Msm.ColorScheme.PrimaryBrush : Program.Msm.GetPrimaryTextBrush(),
								  new RectangleF(36, 0, this.Width - 36, this.Height), new StringFormat() { LineAlignment = StringAlignment.Center });
				}
			}
			else
			{
				e.Graphics.FillEllipse(new SolidBrush(selectionColor), new Rectangle(new Point(0, 0), this.Size));
			}

			DrawIcon(e.Graphics, 20);
			DrawBadge(e.Graphics, Badge);
		}

		private void DrawIcon(Graphics g, int iconSize = 24)
		{
			Image icon = null;
			if (Icon != null) icon = Icon;
			else if (!string.IsNullOrWhiteSpace(IconText))
			{
				try
				{
					icon = Program.GetIcon(IconText, iconSize, Selected ? Program.Msm.ColorScheme.PrimaryColor : Program.Msm.GetCheckboxOffColor());
				}
				catch (Exception)
				{
					icon = null;
				}
			}
			Point point;
			if (this.Height < this.Width)
			{
				var c = this.Height / 2 - icon.Height / 2;
				point = new Point(c, c);
			}
			else
			{
				point = new Point((this.Width / 2) - iconSize / 2, (this.Height / 2) - iconSize / 2);
			}

			if (icon != null)
			{
				g.DrawImage(icon, point.X, point.Y, iconSize, iconSize);
			}
			else
			{
				g.FillEllipse(new SolidBrush(Color.FromArgb(Drawing.GetByte(0.75), Program.Msm.Theme == MaterialSkin.MaterialSkinManager.Themes.DARK ? Color.White : Color.Black)), point.X, point.Y, iconSize, iconSize);
			}
		}

		private void DrawBadge(Graphics g, int badge = 0)
		{
			if (badge > 0)
			{
				var size = 14;
				var rect = new RectangleF(this.Width - size, 0, size, size);
				g.FillEllipse(Selected ? Program.Msm.ACTION_BAR_TEXT_BRUSH : Program.Msm.ColorScheme.PrimaryBrush,
					rect);
				g.DrawString(badge.ToString(),
					_badgeFont,
					Selected ? Program.Msm.ColorScheme.PrimaryBrush : Program.Msm.ACTION_BAR_TEXT_BRUSH, this.Width - size / 2.1f, size / 1.65f, Drawing.Centered);
			}
		}

		private Color GetBackgroundColor()
		{
			if (Raised) return Program.Msm.ColorScheme.PrimaryColor;
			if (CustomBackgroundColor) return this.BackColor;
			return Program.Msm.GetApplicationBackgroundColor();
		}

		#region Events

		private void NavigationDrawerItem_Resize(object sender, EventArgs e) => Size = new Size(Size.Width, 32);

		private void NavigationDrawerItem_MouseDown(object sender, MouseEventArgs e)
		{
			IsDown = true;
			Refresh();
		}

		private void NavigationDrawerItem_MouseUp(object sender, MouseEventArgs e)
		{
			IsDown = false;
			Refresh();
		}

		private void NavigationDrawerItem_MouseEnter(object sender, EventArgs e)
		{
			IsHovering = true;
			Refresh();
		}

		private void NavigationDrawerItem_MouseLeave(object sender, EventArgs e)
		{
			IsHovering = false;
			Refresh();
		}

		#endregion Events
	}
}