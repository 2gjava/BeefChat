﻿namespace BeefChat.ClientGUI.Controls
{
    partial class SideBar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DeveloperDrawerItem = new BeefChat.ClientGUI.Controls.NavigationDrawerItem();
            this.SettingsDrawerItem = new BeefChat.ClientGUI.Controls.NavigationDrawerItem();
            this.ChannelsDrawerItem = new BeefChat.ClientGUI.Controls.NavigationDrawerItem();
            this.FriendsDrawerItem = new BeefChat.ClientGUI.Controls.NavigationDrawerItem();
            this.UserDrawerItem = new BeefChat.ClientGUI.Controls.NavigationDrawerItem();
            this.SuspendLayout();
            // 
            // DeveloperDrawerItem
            // 
            this.DeveloperDrawerItem.Badge = 0;
            this.DeveloperDrawerItem.CustomBackgroundColor = false;
            this.DeveloperDrawerItem.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DeveloperDrawerItem.DrawText = true;
            this.DeveloperDrawerItem.Icon = null;
            this.DeveloperDrawerItem.IconText = "command-line";
            this.DeveloperDrawerItem.Location = new System.Drawing.Point(2, 318);
            this.DeveloperDrawerItem.MaximumSize = new System.Drawing.Size(32, 32);
            this.DeveloperDrawerItem.MinimumSize = new System.Drawing.Size(32, 32);
            this.DeveloperDrawerItem.Name = "DeveloperDrawerItem";
            this.DeveloperDrawerItem.Raised = false;
            this.DeveloperDrawerItem.Selected = false;
            this.DeveloperDrawerItem.Size = new System.Drawing.Size(32, 32);
            this.DeveloperDrawerItem.TabIndex = 14;
            this.DeveloperDrawerItem.Tag = "developer";
            this.DeveloperDrawerItem.Text = null;
            this.DeveloperDrawerItem.Visible = false;
            this.DeveloperDrawerItem.Click += new System.EventHandler(this.DrawerItem_Click);
            // 
            // SettingsDrawerItem
            // 
            this.SettingsDrawerItem.Badge = 0;
            this.SettingsDrawerItem.CustomBackgroundColor = false;
            this.SettingsDrawerItem.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.SettingsDrawerItem.DrawText = true;
            this.SettingsDrawerItem.Icon = null;
            this.SettingsDrawerItem.IconText = "settings";
            this.SettingsDrawerItem.Location = new System.Drawing.Point(2, 350);
            this.SettingsDrawerItem.MaximumSize = new System.Drawing.Size(32, 32);
            this.SettingsDrawerItem.MinimumSize = new System.Drawing.Size(32, 32);
            this.SettingsDrawerItem.Name = "SettingsDrawerItem";
            this.SettingsDrawerItem.Raised = false;
            this.SettingsDrawerItem.Selected = false;
            this.SettingsDrawerItem.Size = new System.Drawing.Size(32, 32);
            this.SettingsDrawerItem.TabIndex = 12;
            this.SettingsDrawerItem.Tag = "settings";
            this.SettingsDrawerItem.Text = null;
            this.SettingsDrawerItem.Click += new System.EventHandler(this.DrawerItem_Click);
            // 
            // ChannelsDrawerItem
            // 
            this.ChannelsDrawerItem.Badge = 0;
            this.ChannelsDrawerItem.CustomBackgroundColor = false;
            this.ChannelsDrawerItem.Dock = System.Windows.Forms.DockStyle.Top;
            this.ChannelsDrawerItem.DrawText = true;
            this.ChannelsDrawerItem.Icon = null;
            this.ChannelsDrawerItem.IconText = "hashtag";
            this.ChannelsDrawerItem.Location = new System.Drawing.Point(2, 66);
            this.ChannelsDrawerItem.MaximumSize = new System.Drawing.Size(32, 32);
            this.ChannelsDrawerItem.MinimumSize = new System.Drawing.Size(32, 32);
            this.ChannelsDrawerItem.Name = "ChannelsDrawerItem";
            this.ChannelsDrawerItem.Raised = false;
            this.ChannelsDrawerItem.Selected = false;
            this.ChannelsDrawerItem.Size = new System.Drawing.Size(32, 32);
            this.ChannelsDrawerItem.TabIndex = 11;
            this.ChannelsDrawerItem.Tag = "channels";
            this.ChannelsDrawerItem.Text = null;
            this.ChannelsDrawerItem.Click += new System.EventHandler(this.DrawerItem_Click);
            // 
            // FriendsDrawerItem
            // 
            this.FriendsDrawerItem.Badge = 99;
            this.FriendsDrawerItem.CustomBackgroundColor = false;
            this.FriendsDrawerItem.Dock = System.Windows.Forms.DockStyle.Top;
            this.FriendsDrawerItem.DrawText = true;
            this.FriendsDrawerItem.Icon = null;
            this.FriendsDrawerItem.IconText = "account-multiple";
            this.FriendsDrawerItem.Location = new System.Drawing.Point(2, 34);
            this.FriendsDrawerItem.MaximumSize = new System.Drawing.Size(32, 32);
            this.FriendsDrawerItem.MinimumSize = new System.Drawing.Size(32, 32);
            this.FriendsDrawerItem.Name = "FriendsDrawerItem";
            this.FriendsDrawerItem.Raised = false;
            this.FriendsDrawerItem.Selected = false;
            this.FriendsDrawerItem.Size = new System.Drawing.Size(32, 32);
            this.FriendsDrawerItem.TabIndex = 10;
            this.FriendsDrawerItem.Tag = "friends";
            this.FriendsDrawerItem.Text = null;
            this.FriendsDrawerItem.Click += new System.EventHandler(this.DrawerItem_Click);
            // 
            // UserDrawerItem
            // 
            this.UserDrawerItem.Badge = 0;
            this.UserDrawerItem.CustomBackgroundColor = false;
            this.UserDrawerItem.Dock = System.Windows.Forms.DockStyle.Top;
            this.UserDrawerItem.DrawText = true;
            this.UserDrawerItem.Icon = null;
            this.UserDrawerItem.IconText = "account-circle";
            this.UserDrawerItem.Location = new System.Drawing.Point(2, 2);
            this.UserDrawerItem.MaximumSize = new System.Drawing.Size(32, 32);
            this.UserDrawerItem.MinimumSize = new System.Drawing.Size(32, 32);
            this.UserDrawerItem.Name = "UserDrawerItem";
            this.UserDrawerItem.Raised = false;
            this.UserDrawerItem.Selected = true;
            this.UserDrawerItem.Size = new System.Drawing.Size(32, 32);
            this.UserDrawerItem.TabIndex = 13;
            this.UserDrawerItem.Tag = "user";
            this.UserDrawerItem.Text = null;
            this.UserDrawerItem.Click += new System.EventHandler(this.DrawerItem_Click);
            // 
            // SideBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.DeveloperDrawerItem);
            this.Controls.Add(this.SettingsDrawerItem);
            this.Controls.Add(this.ChannelsDrawerItem);
            this.Controls.Add(this.FriendsDrawerItem);
            this.Controls.Add(this.UserDrawerItem);
            this.MinimumSize = new System.Drawing.Size(36, 164);
            this.Name = "SideBar";
            this.Padding = new System.Windows.Forms.Padding(2);
            this.Size = new System.Drawing.Size(36, 384);
            this.ResumeLayout(false);

        }

        #endregion

        public NavigationDrawerItem DeveloperDrawerItem;
        public NavigationDrawerItem SettingsDrawerItem;
        public NavigationDrawerItem ChannelsDrawerItem;
        public NavigationDrawerItem FriendsDrawerItem;
        public NavigationDrawerItem UserDrawerItem;
    }
}
