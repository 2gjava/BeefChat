﻿using System.Drawing;
using System.Windows.Forms;

namespace BeefChat.ClientGUI.Controls
{
	public partial class RoundPanel : Panel
	{
		public int Round { get; set; }
		public Padding BackgroundPadding { get; set; }
		public Color PanelColor { get; set; }

		public RoundPanel()
		{
			InitializeComponent();
			Paint += RoundPanel_Paint;
		}

		private void RoundPanel_Paint(object sender, PaintEventArgs e)
		{
			Drawing.SetGraphicsHighQuality(e.Graphics);
			e.Graphics.FillRoundedRectangle(new SolidBrush(PanelColor),
				new Rectangle(
					new Point(
						BackgroundPadding.Left,
						BackgroundPadding.Top),
					new Size(
						Size.Width - BackgroundPadding.Right,
						Size.Height - BackgroundPadding.Bottom)
						), Round);
		}
	}
}