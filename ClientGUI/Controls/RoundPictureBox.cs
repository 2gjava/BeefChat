﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace BeefChat.ClientGUI.Controls
{
	public partial class RoundPictureBox : UserControl
	{
		public Image Image { get; set; }

		public RoundPictureBox()
		{
			InitializeComponent();
		}

		private void RoundPictureBox_Paint(object sender, PaintEventArgs e)
		{
			if (Image != null)
			{
				e.Graphics.DrawImage(Drawing.ClipToCircle(Image, Size), new Rectangle(Point.Empty, Size));
			}
		}

		private void RoundPictureBox_Resize(object sender, EventArgs e)
		{
			//Square size because bugs.
			//if (this.Width > this.Height) this.Height = this.Width;
			//if (this.Height > this.Width) this.Width = this.Height;
		}
	}
}