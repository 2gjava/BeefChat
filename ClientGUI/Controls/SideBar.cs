﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BeefChat.ClientGUI.Controls
{
    public partial class SideBar : UserControl
    {
        public SideBar()
        {
            InitializeComponent();
        }
        public delegate void ItemClicked(string item);

        public event ItemClicked ItemClickedEventHandler;

        private void DrawerItem_Click(object sender, EventArgs e)
        {
            if (sender.GetType() == typeof(NavigationDrawerItem))
            {
                var item = (NavigationDrawerItem)sender;
                if (item.Tag != null && item.Tag.GetType() == typeof(string))
                    ItemClickedEventHandler?.Invoke((string)item.Tag);
            }
        }
    }
}
