﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using BeefChat.Shared;
using BeefChat.Shared.Logging;

namespace BeefChat.ClientGUI.Controls
{
	public partial class LogListBox : ListBox
	{
		private const int appPadding = 2;

		public LogListBox()
		{
			InitializeComponent();
			DrawMode = DrawMode.OwnerDrawFixed;
			ItemHeight = 18;
			this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
			Logger.EntryAdded += (e) =>
			{
				FormHelpers.SafeCall(this, () => this.Items.Add(e));
			};
			this.Items.Clear();
			this.Items.AddRange(Logger.LogEntries.ToArray());
		}

		protected override bool AllowSelection => false;

		protected override bool CanEnableIme => false;

		protected override void OnMeasureItem(MeasureItemEventArgs e)
		{
			if (e.Index == -1) return;
			var item = Logger.LogEntries[e.Index];
			var textSize = e.Graphics.MeasureString(item.Application, this.Font);
			var textSize2 = e.Graphics.MeasureString(item.Message, this.Font);
			e.ItemWidth = (int)textSize.Width + appPadding * 2 + (int)textSize2.Width;
			e.ItemHeight = (int)textSize.Height + appPadding * 2;
		}

		protected override void OnDrawItem(DrawItemEventArgs e)
		{
			if (e.Index >= 0 && e.Index < Logger.LogEntries.Count)

			{
				e.DrawBackground();
				Drawing.SetGraphicsHighQuality(e.Graphics);
				var item = Logger.LogEntries[e.Index];
				var textSize = e.Graphics.MeasureString(item.Application, this.Font);
				var bgBrush = new SolidBrush(e.State == DrawItemState.Selected ? Color.White : GetColor(item.Type));
				var rect = new Rectangle(appPadding + e.Bounds.X, appPadding + e.Bounds.Y, (int)textSize.Width + appPadding, (int)textSize.Height + appPadding);
				e.Graphics.FillRoundedRectangle(bgBrush, rect, 2);
				e.Graphics.DrawString(item.Application, this.Font, new SolidBrush(e.State == DrawItemState.Selected ? SystemColors.Highlight : Drawing.GetReadableColor(GetColor(item.Type))), appPadding, appPadding + e.Bounds.Top);
				e.Graphics.DrawString(item.Message, this.Font, e.State == DrawItemState.Selected ? Brushes.White : Brushes.Black, (int)textSize.Width + appPadding * 3, appPadding + e.Bounds.Top);
				e.DrawFocusRectangle();
			}
		}

		private static Color GetColor(LogEntryType type)
		{
			switch (type)
			{
				case LogEntryType.Normal: return Color.LightGray;
				case LogEntryType.Warning: return Color.Orange;
				case LogEntryType.Error: return Color.Tomato;
				case LogEntryType.Debug: return Color.MediumSeaGreen;
				case LogEntryType.Verbose:
				default: return Color.Gray;
			}
		}
	}
}