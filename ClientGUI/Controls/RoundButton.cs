﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace BeefChat.ClientGUI.Controls
{
	[DefaultEvent("Click")]
	public partial class RoundButton : Control
	{
		private bool IsHovering { get; set; }
		private bool IsDown { get; set; }
		public Image Icon { get; set; }
		public bool CustomColor { get; set; }
		public bool Raised { get; set; }

		public RoundButton()
		{
			InitializeComponent();
			this.SetStyle(
				ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint |
				ControlStyles.OptimizedDoubleBuffer | ControlStyles.SupportsTransparentBackColor,
				true);
		}

		private void RoundButton_Paint(object sender, PaintEventArgs e)
		{
			Drawing.SetGraphicsHighQuality(e.Graphics);
			if (Parent != null)
			{
				Bitmap behind = new Bitmap(Parent.Width, Parent.Height);
				foreach (Control c in Parent.Controls)
					if (c != this && c.Bounds.IntersectsWith(this.Bounds))
						c.DrawToBitmap(behind, c.Bounds);
				e.Graphics.DrawImage(behind, -Left, -Top);
				behind.Dispose();
			}
			e.Graphics.FillEllipse(new SolidBrush(GetBackgroundColor()), new Rectangle(Point.Empty, Size));
			if (Icon == null) return;
			var imgpos = Width / 2 - Icon.Width / 2;
			e.Graphics.DrawImage(Icon, new Point(imgpos, imgpos));
		}

		private Color GetBackgroundColor()
		{
			if (IsDown)
			{
				if (Raised) return Program.Msm.ColorScheme.DarkPrimaryColor;
				if (CustomColor) return ControlPaint.Dark(ForeColor, -0.5f);
				return Program.Msm.GetFlatButtonPressedBackgroundColor();
			}
			if (IsHovering)
			{
				if (Raised) return Program.Msm.ColorScheme.LightPrimaryColor;
				return CustomColor ? ControlPaint.Light(ForeColor, 0.5f) : Program.Msm.GetFlatButtonHoverBackgroundColor();
			}

			return Raised
				? Program.Msm.ColorScheme.PrimaryColor
				: (CustomColor ? ForeColor : Program.Msm.GetApplicationBackgroundColor());
		}

		private void RoundButton_MouseEnter(object sender, EventArgs e)
		{
			IsHovering = true; Refresh();
		}

		private void RoundButton_MouseLeave(object sender, EventArgs e)
		{
			IsHovering = false; Refresh();
		}

		private void RoundButton_MouseDown(object sender, MouseEventArgs e)
		{
			IsDown = true; Refresh();
		}

		private void RoundButton_MouseUp(object sender, MouseEventArgs e)
		{
			IsDown = false; Refresh();
		}
	}
}