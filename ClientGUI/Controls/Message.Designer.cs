﻿namespace BeefChat.ClientGUI.Controls
{
	partial class Message
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
            AuthorFont.Dispose();
			Font.Dispose();
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.SuspendLayout();
			// 
			// Message
			// 
			this.Name = "Message";
			this.Size = new System.Drawing.Size(10, 10);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.Message_Paint);
			this.ResumeLayout(false);

		}

		#endregion
	}
}
