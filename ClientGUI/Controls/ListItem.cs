﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

using static BeefChat.ClientGUI.MDCD;

namespace BeefChat.ClientGUI.Controls
{
	public partial class ListItem : Control
	{
		public string PrimaryText { get; set; }
		public Font PrimaryFont { get; set; }
		public string SecondaryText { get; set; }
		public Font SecondaryFont { get; set; }

		public Image Icon { get; set; } = null;

		[Obsolete]
		[Browsable(false)]
		private new string Text => null;

		public ListItem()
		{
			InitializeComponent();
			SetStyle(ControlStyles.SupportsTransparentBackColor, true);
			this.SizeChanged += (s, e) => FixHeight();
		}

		private void FixHeight()
		{
			this.Height = (int)MDCD.ListItem.Height((Icon != null), !string.IsNullOrWhiteSpace(SecondaryText));
			this.PerformLayout();
		}

		private float GetLeftPadding()
		{
			return MDCD.ListItem.LeftRightPadding + (Icon != null ? MDCD.ListItem.LeftRightPadding + MDCD.GetPX(40) : 0);
		}

		protected override void OnPaint(PaintEventArgs pe)
		{
			MDCD.DPI = pe.Graphics.DpiX;
			var withIcon = (Icon != null);
			var twoLine = !string.IsNullOrWhiteSpace(SecondaryText);
			FixHeight();
			var bottomLeftFormat = new StringFormat() { Alignment = StringAlignment.Near, LineAlignment = StringAlignment.Far };
			var centerLeftFormat = new StringFormat() { Alignment = StringAlignment.Near, LineAlignment = StringAlignment.Center };

			pe.Graphics.DrawString(PrimaryText, PrimaryFont, Program.Msm.GetPrimaryTextBrush(), new RectangleF(GetLeftPadding(),
				twoLine ? MDCD.GetPX(5) : 0,
				this.Width - GetLeftPadding(),
				twoLine ? MDCD.ListItem.PrimaryTextBaseline(withIcon, twoLine) : this.Height), twoLine ? bottomLeftFormat : centerLeftFormat);

			pe.Graphics.DrawString(SecondaryText, SecondaryFont, Program.Msm.GetSecondaryTextBrush(), new RectangleF(GetLeftPadding(),
				MDCD.GetPX(5) + MDCD.ListItem.PrimaryTextBaseline(withIcon, twoLine),
				this.Width - GetLeftPadding(),
				MDCD.ListItem.SecondaryTextBaseline), bottomLeftFormat);
			if (Icon != null)
			{
				//https://stackoverflow.com/questions/32422797/resize-an-image-to-fill-a-picture-box-without-stretching
				Size sourceSize = Icon.Size;
				SizeF targetSize = new SizeF(MDCD.GetPX(40), MDCD.GetPX(40));
				float scale = Math.Max((float)targetSize.Width / sourceSize.Width, (float)targetSize.Height / sourceSize.Height);
				var rect = new RectangleF();
				rect.Width = scale * sourceSize.Width;
				rect.Height = scale * sourceSize.Height;
				rect.X = ((targetSize.Width - rect.Width) / 2) + 16;
				rect.Y = ((targetSize.Height - rect.Height) / 2) + (twoLine ? 16 : 8);
				pe.Graphics.DrawImage(Icon, rect);
			}
		}
	}
}