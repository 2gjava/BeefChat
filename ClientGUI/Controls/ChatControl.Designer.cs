﻿namespace BeefChat.ClientGUI.Controls
{
	partial class ChatControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ChatPanel = new System.Windows.Forms.Panel();
			this.TextBoxPaddingPanel = new System.Windows.Forms.Panel();
			this.MessageTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
			this.SendButton = new MaterialSkin.Controls.MaterialFloatingButton();
			this.MessagesPanel = new System.Windows.Forms.Panel();
			this.ChatPanel.SuspendLayout();
			this.TextBoxPaddingPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// ChatPanel
			// 
			this.ChatPanel.Controls.Add(this.TextBoxPaddingPanel);
			this.ChatPanel.Controls.Add(this.SendButton);
			this.ChatPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.ChatPanel.Location = new System.Drawing.Point(0, 530);
			this.ChatPanel.Name = "ChatPanel";
			this.ChatPanel.Padding = new System.Windows.Forms.Padding(4);
			this.ChatPanel.Size = new System.Drawing.Size(599, 48);
			this.ChatPanel.TabIndex = 0;
			this.ChatPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.ChatPanel_Paint);
			// 
			// TextBoxPaddingPanel
			// 
			this.TextBoxPaddingPanel.Controls.Add(this.MessageTextBox);
			this.TextBoxPaddingPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TextBoxPaddingPanel.Location = new System.Drawing.Point(4, 4);
			this.TextBoxPaddingPanel.Name = "TextBoxPaddingPanel";
			this.TextBoxPaddingPanel.Padding = new System.Windows.Forms.Padding(8);
			this.TextBoxPaddingPanel.Size = new System.Drawing.Size(551, 40);
			this.TextBoxPaddingPanel.TabIndex = 4;
			this.TextBoxPaddingPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.TextBoxPaddingPanel_Paint);
			// 
			// MessageTextBox
			// 
			this.MessageTextBox.Depth = 0;
			this.MessageTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.MessageTextBox.Hint = "Message";
			this.MessageTextBox.Location = new System.Drawing.Point(8, 8);
			this.MessageTextBox.MaxLength = 32767;
			this.MessageTextBox.MouseState = MaterialSkin.MouseState.HOVER;
			this.MessageTextBox.Name = "MessageTextBox";
			this.MessageTextBox.PasswordChar = '\0';
			this.MessageTextBox.SelectedText = "";
			this.MessageTextBox.SelectionLength = 0;
			this.MessageTextBox.SelectionStart = 0;
			this.MessageTextBox.Size = new System.Drawing.Size(535, 23);
			this.MessageTextBox.TabIndex = 3;
			this.MessageTextBox.TabStop = false;
			this.MessageTextBox.UseSystemPasswordChar = false;
			this.MessageTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_message_KeyDown);
			this.MessageTextBox.TextChanged += new System.EventHandler(this.tb_message_TextChanged);
			// 
			// SendButton
			// 
			this.SendButton.AutoSize = true;
			this.SendButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.SendButton.Depth = 0;
			this.SendButton.Dock = System.Windows.Forms.DockStyle.Right;
			this.SendButton.Icon = null;
			this.SendButton.Location = new System.Drawing.Point(555, 4);
			this.SendButton.Mini = true;
			this.SendButton.MouseState = MaterialSkin.MouseState.HOVER;
			this.SendButton.Name = "SendButton";
			this.SendButton.Size = new System.Drawing.Size(40, 40);
			this.SendButton.TabIndex = 5;
			this.SendButton.UseVisualStyleBackColor = true;
			this.SendButton.Click += new System.EventHandler(this.SendButton_Click);
			// 
			// MessagesPanel
			// 
			this.MessagesPanel.AutoScroll = true;
			this.MessagesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.MessagesPanel.Location = new System.Drawing.Point(0, 0);
			this.MessagesPanel.Name = "MessagesPanel";
			this.MessagesPanel.Padding = new System.Windows.Forms.Padding(15);
			this.MessagesPanel.Size = new System.Drawing.Size(599, 530);
			this.MessagesPanel.TabIndex = 1;
			// 
			// ChatControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.MessagesPanel);
			this.Controls.Add(this.ChatPanel);
			this.DoubleBuffered = true;
			this.Name = "ChatControl";
			this.Size = new System.Drawing.Size(599, 578);
			this.ChatPanel.ResumeLayout(false);
			this.ChatPanel.PerformLayout();
			this.TextBoxPaddingPanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel ChatPanel;
		private System.Windows.Forms.Panel MessagesPanel;
		private System.Windows.Forms.Panel TextBoxPaddingPanel;
		private MaterialSkin.Controls.MaterialSingleLineTextField MessageTextBox;
        private MaterialSkin.Controls.MaterialFloatingButton SendButton;
    }
}
