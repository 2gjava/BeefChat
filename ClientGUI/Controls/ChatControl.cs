﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

using BeefChat.Shared;
using BeefChat.Shared.Objects;

namespace BeefChat.ClientGUI.Controls
{
	public partial class ChatControl : UserControl
	{
		public Channel Channel { get; set; }

		private readonly Image _send = Program.GetIcon("send", 24, Color.White);
		private readonly Image _upload = Program.GetIcon("upload", 24, Color.White);

		public ChatControl(Channel channel)
		{
			Channel = channel;
			Program.Client.MessageReceivedEventHandler += Client_MessageReceivedEvent;
			InitializeComponent();
			BackColor = Program.Msm.GetApplicationBackgroundColor();
			ChatPanel.BackColor = Program.Msm.GetApplicationBackgroundColor();
			MessagesPanel.BackColor = Program.Msm.GetApplicationBackgroundColor();
			MessageTextBox.BackColor = Program.Msm.GetApplicationBackgroundColor();
			MessagesPanel.Controls.Clear();
			SendButton.Icon = _upload;
		}

		private void Client_MessageReceivedEvent(Shared.Objects.Message message)
		{
			if (message.Channel.ToString() != Channel.ToString()) return;
			var msgctl = new Message(message)
			{
				Dock = DockStyle.Top,
				TextPadding = new Padding(9, 7, 20, 7),
				Padding = new Padding(0, 2, 0, 2),
				Font = Program.Msm.ROBOTO_REGULAR_11,
				AuthorOverMessage = Program.CurrentSettings.AuthorOverMessage,
			};
			//msgctl.MouseClick += Message_MouseClick;
			FormHelpers.SafeCall(MessagesPanel, delegate
			{
				MessagesPanel.Controls.Add(msgctl);
				msgctl.BringToFront();
				MessagesPanel.ScrollControlIntoView(msgctl);
			});
		}

		private void SendButton_Click(object sender, EventArgs e)
		{
			if (!string.IsNullOrWhiteSpace(MessageTextBox.Text))
				SendMessage();
			else
				SendFile();
		}

		private void SendMessage()
		{
			Program.Client.SendMessage(Channel, MessageTextBox.Text);
			MessageTextBox.Text = string.Empty;
		}

		private void SendFile()
		{
			var dia = new OpenFileDialog();
			if (dia.ShowDialog() == DialogResult.OK)
				Program.Client.SendFile(Channel, System.IO.File.ReadAllBytes(dia.FileName), System.IO.Path.GetFileName(dia.FileName), true);
		}

		private void tb_message_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode != Keys.Enter) return;
			SendMessage();
			e.Handled = true;
			e.SuppressKeyPress = true;
		}

		private void tb_message_TextChanged(object sender, EventArgs e)
		{
			SendButton.Icon = string.IsNullOrWhiteSpace(MessageTextBox.Text) ? _upload : _send;
			SendButton.Refresh();
		}

		private void ChatPanel_Paint(object sender, PaintEventArgs e)
		{
		}

		private void TextBoxPaddingPanel_Paint(object sender, PaintEventArgs e)
		{
		}
	}
}