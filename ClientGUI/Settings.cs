﻿using System.ComponentModel;
using System.IO;

using MaterialSkin;

using Newtonsoft.Json;

// ReSharper disable InconsistentNaming

namespace BeefChat.ClientGUI
{
	public class Settings
	{
		#region Functions

		public static Settings Load(string filePath) => JsonConvert.DeserializeObject<Settings>(File.ReadAllText(filePath));

		public void Save(string filePath) => Save(this, filePath);

		public static void Save(Settings settings, string filePath) => File.WriteAllText(filePath, JsonConvert.SerializeObject(settings));

		#endregion Functions

		#region Properties

		[Category("Customization")]
		[DisplayName("Author over message")]
		public bool AuthorOverMessage { get; set; } = true;

		[Category("Customization")]
		[DisplayName("Current Theme")]
		public string CurrentTheme { get; set; } = "Default";

		#region Notifications

		[Category("Notifications")]
		[DisplayName("Enable Notifications")]
		public bool EnableNotifications { get; set; } = true;

		[Category("Notifications")]
		[DisplayName("Method")]
		public NotificationMethod NotificationMethod { get; set; } = NotificationMethod.WindowsBalloon;

		#endregion Notifications

		#endregion Properties
	}
}