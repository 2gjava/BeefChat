﻿using System.Globalization;
using System.Resources;
using System.Windows.Forms;

namespace BeefChat.ClientGUI
{
	/// <summary>
	/// Has localization tools such as culture/langauge specific string retrieving.
	/// </summary>
	public static class Localization
	{
		/// <summary>
		/// The culture to retrieve the strings from.
		/// </summary>
		public static CultureInfo CultureInfo = CultureInfo.CreateSpecificCulture("en");

		public static ResourceManager ResourceManager => new ResourceManager("BeefChat.ClientGUI.Localization.Language", typeof(Localization).Assembly);

		/// <summary>
		/// Returns a string in the current specified culture.
		/// See also: <seealso cref="CultureInfo"/>
		/// </summary>
		/// <param name="key">The key of the object.</param>
		/// <returns>The requested string in the current specified culture.</returns>
		public static string GetString(object key)
		{
			var result = ResourceManager.GetString(key.ToString(), CultureInfo);
			if (result == null || result == "null")
			{
				return key.ToString();
			}
			else
			{
				return result;
			}
		}
	}
}