﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

using BeefChat.ClientGUI.Forms;
using BeefChat.Shared.Exceptions;
using BeefChat.Shared.Logging;

using MaterialSkin;

using Microsoft.Win32;

using Svg;

namespace BeefChat.ClientGUI
{
	internal static class Program
	{
		public static Client.Client Client { get; } = new Client.Client();
		public static MainForm MainForm;
		public static NotifyIcon NotifyIcon;
		public static Settings CurrentSettings;
		public const string AppxApplicationId = "BlueValleyMeatCorporation.BeefChat";
		public const string ApplicationName = "BeefChat";
		public static bool DeveloperMode = Environment.GetCommandLineArgs().ToList().Contains("--supersecretdevshit");

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		private static void Main(string[] args)
		{
			try
			{
				Logger.Add(LogEntryType.Verbose, "Initializing");
				Application.EnableVisualStyles();
				Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
				Application.ThreadException += (s, e) => { ReportException(e.Exception); };
				Environment.CurrentDirectory = Path.GetDirectoryName(Application.ExecutablePath);

				RegisterUriScheme();

				Logger.Add(LogEntryType.Verbose, "Loading Notification Icon");
				NotifyIcon = new NotifyIcon()
				{
					Icon = Drawing.GetSize(Properties.Resources.BeefChat_Tray_Outlined, 16, 16),
					Text = ApplicationName,
					Visible = true,
					ContextMenu = new ContextMenu(new MenuItem[] {
						new MenuItem("Show", (s,e) =>
						{
							if (MainForm != null)
							{
								MainForm.Show();
								MainForm.WindowState = FormWindowState.Normal;
							}
						}),
						new MenuItem("Exit", (s,e) => { Shutdown(); }),
					})
				};
				NotifyIcon.DoubleClick += (s, e) => { if (MainForm != null) MainForm.WindowState = FormWindowState.Normal; };

				Logger.Add(LogEntryType.Verbose, "Loading Themes");
				ThemeManager = new ThemeManager(Path.Combine("themes", "Default"));

				Logger.Add(LogEntryType.Verbose, "Loading Settings");
				if (!File.Exists("settings.json")) new Settings().Save("settings.json");
				CurrentSettings = Settings.Load("settings.json");
				ApplySettings();

				SetMsmToTheme(ThemeManager.CurrentTheme);

				if (Shared.Constants.DebugMode)
				{
					MessageBox.Show("Debug Mode in the shared library is enabled, this will expose every packet type for debugging", ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}

				while (true)
				{
					//Retry until user has successfully logged on.
					if (GuiLogin())
					{
						using (MainForm = new MainForm())
						{
							MainForm.ShowDialog();
						}
					}
				}
			}
			catch (Exception ex)
			{
				ReportException(ex);
			}
		}

		public static void ReportException(Exception ex)
		{
			if (Debugger.IsAttached)
			{
				throw ex;
			}
			else
			{
				new CrashForm(ex).ShowDialog();
			}
		}

		public static void Shutdown()
		{
			Client?.Dispose();
			CurrentSettings?.Save("settings.json");
			if (NotifyIcon != null)
			{
				NotifyIcon.Visible = false;
			}

			Environment.Exit(-1);
		}

		public static void ApplySettings()
		{
			Notifications.Method = CurrentSettings.NotificationMethod;
			var theme = ThemeManager.ThemeList.FirstOrDefault(t => t.Name == CurrentSettings.CurrentTheme);
			if (theme != null)
			{
				ThemeManager.CurrentTheme = theme;
			}
		}

		#region Protocol Handling

		public const string UriScheme = "beefchat";
		public const string FriendUriScheme = "beefchat-friend";
		public const string FriendlyName = ApplicationName;

		public static void RegisterUriScheme()
		{
			using (var key = Registry.CurrentUser.CreateSubKey("SOFTWARE\\Classes\\" + UriScheme))
			{
				key.SetValue("", FriendlyName);
				key.SetValue("URL Protocol", "");

				using (var defaultIcon = key.CreateSubKey("DefaultIcon"))
				{
					defaultIcon.SetValue("", Application.ExecutablePath + ",1");
				}

				using (var commandKey = key.CreateSubKey(@"shell\open\command"))
				{
					commandKey.SetValue("", "\"" + Application.ExecutablePath + "\" \"%1\"");
				}
			}
			using (var key = Registry.CurrentUser.CreateSubKey("SOFTWARE\\Classes\\" + FriendUriScheme))
			{
				key.SetValue("", FriendlyName);
				key.SetValue("URL Protocol", "");

				using (var defaultIcon = key.CreateSubKey("DefaultIcon"))
				{
					defaultIcon.SetValue("", Application.ExecutablePath + ",1");
				}

				using (var commandKey = key.CreateSubKey(@"shell\open\command"))
				{
					commandKey.SetValue("", "\"" + Application.ExecutablePath + "\" \"%1\"");
				}
			}
		}

		#endregion Protocol Handling

		#region Theming

		public static MaterialSkinManager Msm = MaterialSkinManager.Instance;
		public static ThemeManager ThemeManager;

		public static void SetTheme(string name)
		{
			Theme theme = ThemeManager.ThemeList.FirstOrDefault(t => t.Name == name);
			if (theme != null)
			{
				SetMsmToTheme(theme);
			}
		}

		public static void SetMsmToTheme(Theme theme)
		{
			Msm.Theme = theme.Dark ? MaterialSkinManager.Themes.DARK : MaterialSkinManager.Themes.LIGHT;
			Msm.ColorScheme = new ColorScheme(theme.Primary,
											  theme.PrimaryDark,
											  theme.PrimaryLight,
											  theme.Accent,
											  theme.TextShade);
		}

		#region Get Icons

		public static Image GetIcon(string iconName, int size, Color color) => GetIcon(ThemeManager, iconName, size, color);

		public static Image GetIcon(ThemeManager themeManager, string iconName, int size, Color color)
		{
			try
			{
				return GetIcon(themeManager.CurrentTheme, iconName, size, color);
			}
			catch (ArgumentOutOfRangeException)
			{
				try
				{
					return GetIcon(themeManager.DefaultTheme, iconName, size, color);
				}
				catch (Exception ex)
				{
					throw new Exception("Couldn't load from default theme.", ex);
				}
			}
		}

		public static Image GetIcon(Theme theme, string iconName, int size, Color color)
		{
			if (theme == null) throw new ArgumentNullException(nameof(theme));
			var icon = theme.Icons.FirstOrDefault(i => i.Key == iconName);
			if (icon.Value == null) throw new ArgumentOutOfRangeException("Entered icon couldn't be found in the specified theme.");
			if (icon.Value.GetType() == typeof(Image)) return ((Image)icon.Value).ResizeImage(size, size);
			if (icon.Value.GetType() == typeof(SvgDocument))
			{
				SvgDocument svg = (SvgDocument)icon.Value;
				svg.Color = new SvgColourServer(color);
				svg.Fill = new SvgColourServer(color);
				svg.Height = new SvgUnit(SvgUnitType.Pixel, size);
				svg.Width = new SvgUnit(SvgUnitType.Pixel, size);
				return svg.Draw();
			}

			throw new ArgumentException(nameof(iconName), "Can't convert icon object to Image");
		}

		#endregion Get Icons

		#endregion Theming

		#region Resource File Reading

		public static string GetEmbeddedResource(string resourceName, Assembly assembly)
		{
			resourceName = FormatResourceName(assembly, resourceName);
			using (Stream resourceStream = assembly.GetManifestResourceStream(resourceName))
			{
				if (resourceStream == null) return null;
				StreamReader reader = new StreamReader(resourceStream);
				var result = reader.ReadToEnd();
				reader.Dispose();
				resourceStream.Dispose();
				return result;
			}
		}

		private static string FormatResourceName(Assembly assembly, string resourceName)
		{
			return assembly.GetName().Name + "." + resourceName.Replace(" ", "_")
															   .Replace("\\", ".")
															   .Replace("/", ".");
		}

		#endregion Resource File Reading

		public static bool GuiLogin()
		{
			string ipaddress;
			string username;
			string password;
			bool register = false;
			// Show Login Form and ask for information
			using (LoginForm loginform = new LoginForm())
			{
				DialogResult result = loginform.ShowDialog();
				switch (result)
				{
					case DialogResult.Yes:
					case DialogResult.OK:
						//Yes = Register, Ok = Login
						register = result == DialogResult.Yes;
						break;

					default:
						Environment.Exit(-1);
						break;
				}

				// Save information before disposing
				ipaddress = loginform.IpAddress;
				username = loginform.Username;
				password = loginform.Password;
			}

			#region Actual connecting & logging in process

			using (LoadingForm loadform = new LoadingForm("Connecting to server..."))
			{
				Thread mThread = new Thread(delegate ()
				{
					Application.Run(loadform);
				});
				mThread.Start();

				try
				{
					Client.Connect(ipaddress);
					loadform.LoadingText = register ? "Registering..." : "Logging in...";
					if (register)
					{
						Client.Register(username, password);
					}
					else
					{
						Client.Login(username, password);
					}

					return true;
				}
				catch (BannedException)
				{
					MessageBox.Show("Authentication has failed because you've been banned.",
								   ApplicationName,
								   MessageBoxButtons.OK,
								   MessageBoxIcon.Exclamation);
					return false;
				}
				catch (InvalidCredentialsException)
				{
					MessageBox.Show("Authentication has failed due to an invalid username. \nPlease try again.",
								   ApplicationName,
								   MessageBoxButtons.OK,
								   MessageBoxIcon.Exclamation);
					return false;
				}
				catch (WrongCredentialsException)
				{
					MessageBox.Show("Authentication has failed due to wrong username or password. \nPlease try again.",
								   ApplicationName,
								   MessageBoxButtons.OK,
								   MessageBoxIcon.Exclamation);
					return false;
				}
				catch (AccountAlreadyExistsException)
				{
					MessageBox.Show("Authentication has failed because an account with that username already exists. \nPlease try again.",
								   ApplicationName,
								   MessageBoxButtons.OK,
								   MessageBoxIcon.Exclamation);
					return false;
				}
				catch (Exception ex)
				{
					if (ex.GetType() == typeof(SocketException) && ex.HResult == -2147467259)
					{
						MessageBox.Show("Failed to connect to server.",
									ApplicationName,
									MessageBoxButtons.OK,
									MessageBoxIcon.Exclamation);
					}
					else
					{
						throw;
					}
					return false;
				}
				finally
				{
					FormHelpers.SafeCall(loadform, delegate { loadform.Dispose(); });
				}
			}

			#endregion Actual connecting & logging in process
		}
	}
}