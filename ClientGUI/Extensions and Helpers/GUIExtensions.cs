﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BeefChat.ClientGUI.Forms;
using Microsoft.VisualBasic;
using static BeefChat.ClientGUI.Drawing;

namespace BeefChat.ClientGUI
{
	public static class GUIExtensions
	{
		public static void ChangePictureGUI(this Client.Client client)
		{
			OpenFileDialog dialog = new OpenFileDialog();
			if (dialog.ShowDialog() != DialogResult.OK) return;
			Image img = Image.FromFile(dialog.FileName);
			client.ChangePicture(ToBytes(img.ResizeImage(64, 64), ImageFormat.Png));
		}

		public static void ChangeColorGUI(this Client.Client client)
		{
			ColorForm dialog = new ColorForm();
			if (dialog.ShowDialog() == DialogResult.OK)
			{
				client.ChangeColor(ColorTranslator.ToHtml(dialog.Color));
			}
		}

		public static void AddFriendGUI(this Client.Client client)
		{
			AddFriendForm dialog = new AddFriendForm();
			if (dialog.ShowDialog() == DialogResult.OK)
			{
				client.AddFriend(dialog.Username);
			}
		}

		public static void DeleteAccountGUI(this Client.Client client)
		{
			if (MessageBox.Show("Are you sure you want to continue deleting your account? This action can not be undone.", Program.ApplicationName, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
			{
				var password = Interaction.InputBox("Enter password to continue account deletion", Program.ApplicationName);
				if (password.Length != 0) client.DeleteAccount(password);
			}
		}
	}
}