﻿using BeefChat.Shared;
using CredentialManagement;

namespace BeefChat.ClientGUI
{
	//Source: https://stackoverflow.com/questions/17741424/retrieve-credentials-from-windows-credentials-store-using-c-sharp
	public static class CredentialUtil
	{
		public static UserPass GetCredential(string target)
		{
			var cm = new Credential
			{
				Target = target + ":" + Constants.PortNumber,
				Type = CredentialType.Generic
			};
			return !cm.Load() ? null : new UserPass(cm.Username, cm.Password);
		}

		public static bool SetCredentials(string target, string username, string password,
			PersistanceType persistenceType)
		{
			return new Credential
			{
				Target = target + ":" + Constants.PortNumber,
				Username = username,
				Password = password,
				PersistanceType = persistenceType,
				Description = "Credential used by BeefChat",
				Type = CredentialType.Generic
			}.Save();
		}

		public static bool RemoveCredentials(string target)
		{
			return new Credential { Target = target }.Delete();
		}
	}

	public class UserPass
	{
		public string Username { get; }
		public string Password { get; }

		public UserPass(string username, string password)
		{
			Username = username;
			Password = password;
		}
	}
}