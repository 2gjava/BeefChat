﻿namespace BeefChat.ClientGUI
{
	/// <summary>
	/// Contains values and functions to calculate and return appropiate dimensions used for Material Design Components.
	/// Full Name: Material Design Component Dimensions
	/// </summary>
	public static class MDCD
	{
		/// <summary>
		/// Dots per inch, used by the functions to calculate correct sizes.
		/// </summary>
		public static float DPI = 96;

		public static class ListItem
		{
			public static float PrimaryTextBaseline(bool WithIcon, bool TwoLine) => TwoLine ? GetPX(WithIcon ? 32 : 28) : 0;

			public static float Height(bool WithIcon, bool TwoLine) => GetPX(WithIcon ? 56 : 48) + GetPX(TwoLine ? 16 : 0);

			public static float SecondaryTextBaseline => GetPX(20);
			public static float LeftRightPadding => GetPX(16);
			public static float IconPadding => GetPX(16);
		}

		public static float GetPX(int dp) => dp * DPI / 96.0F;
	}
}