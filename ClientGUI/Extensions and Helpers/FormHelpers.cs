﻿using System;
using System.Windows.Forms;

namespace BeefChat.ClientGUI
{
	public static class FormHelpers
	{
		public static void SafeCall(Control control, Action func)
		{
			if (control == null) return;
			if (control.InvokeRequired)
			{
				control.Invoke(new MethodInvoker(delegate
				{
					func();
				}));
			}
			else
			{
				if (control.IsDisposed) return;
				// Handle will be force created and will not be used.
				// ReSharper disable once UnusedVariable
				if (!control.IsHandleCreated) { var handle = control.Handle; }
				func();
			}
		}
	}
}