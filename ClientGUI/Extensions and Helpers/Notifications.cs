﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using BeefChat.ClientGUI.Forms;
using BeefChat.Shared;
using BeefChat.Shared.Logging;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace BeefChat.ClientGUI
{
	public static class Notifications
	{
		public class NotificationResult
		{
			public NotificationMethod MethodUsed;
			public NotificationForm Form;
			public ToastNotification Toast;

			public NotificationResult(NotificationMethod method, object notificationObject)
			{
				this.MethodUsed = method;
				if (method == NotificationMethod.Custom)
				{
					if (notificationObject.GetType() == typeof(NotificationForm))
					{
						Form = (NotificationForm)notificationObject;
					}
					else
					{
						throw new ArgumentException(nameof(notificationObject), "Method does not match with the notificationObject");
					}
				}
				if (method == NotificationMethod.WindowsToast)
				{
					if (notificationObject.GetType() == typeof(ToastNotification))
					{
						Toast = (ToastNotification)notificationObject;
					}
					else
					{
						throw new ArgumentException(nameof(notificationObject), "Method does not match with the notificationObject");
					}
				}
			}
		}

		/// <summary>
		/// The method on how the notifications should be sent.
		/// </summary>
		public static NotificationMethod Method = NotificationMethod.WindowsBalloon;

		public static List<NotificationForm> notificationForms { get; } = new List<NotificationForm>();

		public static NotificationResult Show(Shared.Objects.Message message)
		{
			//var pic1 = Program.Client.GetProfile(message.Author)?.Picture;
			//Image pic2;
			//if (pic1 != null) pic2 = Drawing.ToImage(pic1);
			//else pic2 = null;
			return Show(message.Author + " - " + message.Channel.ToString(), message.Content, null, true);
		}

		/// <summary>
		/// Sends a notification with a <paramref name="title"/>, <paramref name="text"/> and an <paramref name="icon"/>.
		/// </summary>
		public static NotificationResult Show(string title, string text, Image icon = null, bool reply = false)
		{
			switch (Method)
			{
				case NotificationMethod.WindowsBalloon:
					Program.NotifyIcon.ShowBalloonTip(1000, title, text, ToolTipIcon.None);
					return new NotificationResult(Method, null);

				case NotificationMethod.WindowsToast:
					var xml = new XmlDocument();
					xml.LoadXml($@"<visual>
  <binding template='ToastGeneric'>
    <text>{title}</text>
    <text>{text}</text>
  </binding>
</visual>");
					var toast = new ToastNotification(xml);
					ToastNotificationManager.CreateToastNotifier(Program.AppxApplicationId).Show(toast);
					return new NotificationResult(Method, toast);

				case NotificationMethod.Custom:
					notificationForms.Add(new NotificationForm(title, text, icon, reply));
					notificationForms.Last().Show();
					return new NotificationResult(Method, notificationForms.Last());

				default:
					Logger.Add(LogEntryType.Warning, "Notification could not be sent because the method is unknown.");
					return new NotificationResult(Method, null);
			}
		}
	}

	/// <summary>
	/// The method on how the notification should be sent.
	/// </summary>
	public enum NotificationMethod
	{
		/// <summary>
		/// Shows the notification as classic Windows balloon.
		/// </summary>
		WindowsBalloon,

		/// <summary>
		/// Shows the notification as Windows toast.
		/// </summary>
		/// <remarks>Windows 10 only</remarks>
		WindowsToast,

		/// <summary>
		/// Shows the notification as a Material themed window.
		/// </summary>
		Custom,
	}
}